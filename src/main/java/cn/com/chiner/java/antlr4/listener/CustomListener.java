package cn.com.chiner.java.antlr4.listener;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/9/18
 * 实现功能 : 全局监听语法错误
 */
public class CustomListener extends BaseErrorListener {
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        throw new RuntimeException("error:"+"line at "+line+":"+charPositionInLine+" "+msg);
    }
}
