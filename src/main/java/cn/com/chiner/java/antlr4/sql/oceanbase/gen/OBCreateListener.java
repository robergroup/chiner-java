// Generated from C:/Users/24584/IdeaProjects/chiner-java/src/main/resources/g4/oceanbase/OBCreate.g4 by ANTLR 4.13.1
package cn.com.chiner.java.antlr4.sql.oceanbase.gen;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OBCreateParser}.
 */
public interface OBCreateListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(OBCreateParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(OBCreateParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#databaseDDL}.
	 * @param ctx the parse tree
	 */
	void enterDatabaseDDL(OBCreateParser.DatabaseDDLContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#databaseDDL}.
	 * @param ctx the parse tree
	 */
	void exitDatabaseDDL(OBCreateParser.DatabaseDDLContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#databaseUse}.
	 * @param ctx the parse tree
	 */
	void enterDatabaseUse(OBCreateParser.DatabaseUseContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#databaseUse}.
	 * @param ctx the parse tree
	 */
	void exitDatabaseUse(OBCreateParser.DatabaseUseContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#ddl_table}.
	 * @param ctx the parse tree
	 */
	void enterDdl_table(OBCreateParser.Ddl_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#ddl_table}.
	 * @param ctx the parse tree
	 */
	void exitDdl_table(OBCreateParser.Ddl_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#ddl_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterDdl_primary_key(OBCreateParser.Ddl_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#ddl_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitDdl_primary_key(OBCreateParser.Ddl_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#ddl_index}.
	 * @param ctx the parse tree
	 */
	void enterDdl_index(OBCreateParser.Ddl_indexContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#ddl_index}.
	 * @param ctx the parse tree
	 */
	void exitDdl_index(OBCreateParser.Ddl_indexContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#filter_index}.
	 * @param ctx the parse tree
	 */
	void enterFilter_index(OBCreateParser.Filter_indexContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#filter_index}.
	 * @param ctx the parse tree
	 */
	void exitFilter_index(OBCreateParser.Filter_indexContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_definition_list}.
	 * @param ctx the parse tree
	 */
	void enterTable_definition_list(OBCreateParser.Table_definition_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_definition_list}.
	 * @param ctx the parse tree
	 */
	void exitTable_definition_list(OBCreateParser.Table_definition_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_definition}.
	 * @param ctx the parse tree
	 */
	void enterTable_definition(OBCreateParser.Table_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_definition}.
	 * @param ctx the parse tree
	 */
	void exitTable_definition(OBCreateParser.Table_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#column_definition_list}.
	 * @param ctx the parse tree
	 */
	void enterColumn_definition_list(OBCreateParser.Column_definition_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#column_definition_list}.
	 * @param ctx the parse tree
	 */
	void exitColumn_definition_list(OBCreateParser.Column_definition_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#column_definition}.
	 * @param ctx the parse tree
	 */
	void enterColumn_definition(OBCreateParser.Column_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#column_definition}.
	 * @param ctx the parse tree
	 */
	void exitColumn_definition(OBCreateParser.Column_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_option_list}.
	 * @param ctx the parse tree
	 */
	void enterTable_option_list(OBCreateParser.Table_option_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_option_list}.
	 * @param ctx the parse tree
	 */
	void exitTable_option_list(OBCreateParser.Table_option_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_option}.
	 * @param ctx the parse tree
	 */
	void enterTable_option(OBCreateParser.Table_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_option}.
	 * @param ctx the parse tree
	 */
	void exitTable_option(OBCreateParser.Table_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#column_name_list}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name_list(OBCreateParser.Column_name_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#column_name_list}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name_list(OBCreateParser.Column_name_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#comments}.
	 * @param ctx the parse tree
	 */
	void enterComments(OBCreateParser.CommentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#comments}.
	 * @param ctx the parse tree
	 */
	void exitComments(OBCreateParser.CommentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#columnDesc}.
	 * @param ctx the parse tree
	 */
	void enterColumnDesc(OBCreateParser.ColumnDescContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#columnDesc}.
	 * @param ctx the parse tree
	 */
	void exitColumnDesc(OBCreateParser.ColumnDescContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#autoIncrement}.
	 * @param ctx the parse tree
	 */
	void enterAutoIncrement(OBCreateParser.AutoIncrementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#autoIncrement}.
	 * @param ctx the parse tree
	 */
	void exitAutoIncrement(OBCreateParser.AutoIncrementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#notNull}.
	 * @param ctx the parse tree
	 */
	void enterNotNull(OBCreateParser.NotNullContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#notNull}.
	 * @param ctx the parse tree
	 */
	void exitNotNull(OBCreateParser.NotNullContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#null}.
	 * @param ctx the parse tree
	 */
	void enterNull(OBCreateParser.NullContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#null}.
	 * @param ctx the parse tree
	 */
	void exitNull(OBCreateParser.NullContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#primaryKey}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryKey(OBCreateParser.PrimaryKeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#primaryKey}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryKey(OBCreateParser.PrimaryKeyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#uniqueKey}.
	 * @param ctx the parse tree
	 */
	void enterUniqueKey(OBCreateParser.UniqueKeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#uniqueKey}.
	 * @param ctx the parse tree
	 */
	void exitUniqueKey(OBCreateParser.UniqueKeyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#character}.
	 * @param ctx the parse tree
	 */
	void enterCharacter(OBCreateParser.CharacterContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#character}.
	 * @param ctx the parse tree
	 */
	void exitCharacter(OBCreateParser.CharacterContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#collate}.
	 * @param ctx the parse tree
	 */
	void enterCollate(OBCreateParser.CollateContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#collate}.
	 * @param ctx the parse tree
	 */
	void exitCollate(OBCreateParser.CollateContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#charset}.
	 * @param ctx the parse tree
	 */
	void enterCharset(OBCreateParser.CharsetContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#charset}.
	 * @param ctx the parse tree
	 */
	void exitCharset(OBCreateParser.CharsetContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#default}.
	 * @param ctx the parse tree
	 */
	void enterDefault(OBCreateParser.DefaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#default}.
	 * @param ctx the parse tree
	 */
	void exitDefault(OBCreateParser.DefaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tableDesc}.
	 * @param ctx the parse tree
	 */
	void enterTableDesc(OBCreateParser.TableDescContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tableDesc}.
	 * @param ctx the parse tree
	 */
	void exitTableDesc(OBCreateParser.TableDescContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#engine}.
	 * @param ctx the parse tree
	 */
	void enterEngine(OBCreateParser.EngineContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#engine}.
	 * @param ctx the parse tree
	 */
	void exitEngine(OBCreateParser.EngineContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#rowFormat}.
	 * @param ctx the parse tree
	 */
	void enterRowFormat(OBCreateParser.RowFormatContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#rowFormat}.
	 * @param ctx the parse tree
	 */
	void exitRowFormat(OBCreateParser.RowFormatContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tableAutoIncrement}.
	 * @param ctx the parse tree
	 */
	void enterTableAutoIncrement(OBCreateParser.TableAutoIncrementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tableAutoIncrement}.
	 * @param ctx the parse tree
	 */
	void exitTableAutoIncrement(OBCreateParser.TableAutoIncrementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tableCharset}.
	 * @param ctx the parse tree
	 */
	void enterTableCharset(OBCreateParser.TableCharsetContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tableCharset}.
	 * @param ctx the parse tree
	 */
	void exitTableCharset(OBCreateParser.TableCharsetContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tableCollate}.
	 * @param ctx the parse tree
	 */
	void enterTableCollate(OBCreateParser.TableCollateContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tableCollate}.
	 * @param ctx the parse tree
	 */
	void exitTableCollate(OBCreateParser.TableCollateContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#comment}.
	 * @param ctx the parse tree
	 */
	void enterComment(OBCreateParser.CommentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#comment}.
	 * @param ctx the parse tree
	 */
	void exitComment(OBCreateParser.CommentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#comment_value}.
	 * @param ctx the parse tree
	 */
	void enterComment_value(OBCreateParser.Comment_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#comment_value}.
	 * @param ctx the parse tree
	 */
	void exitComment_value(OBCreateParser.Comment_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#data_type}.
	 * @param ctx the parse tree
	 */
	void enterData_type(OBCreateParser.Data_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#data_type}.
	 * @param ctx the parse tree
	 */
	void exitData_type(OBCreateParser.Data_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tenant_name}.
	 * @param ctx the parse tree
	 */
	void enterTenant_name(OBCreateParser.Tenant_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tenant_name}.
	 * @param ctx the parse tree
	 */
	void exitTenant_name(OBCreateParser.Tenant_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#pool_name}.
	 * @param ctx the parse tree
	 */
	void enterPool_name(OBCreateParser.Pool_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#pool_name}.
	 * @param ctx the parse tree
	 */
	void exitPool_name(OBCreateParser.Pool_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#unit_name}.
	 * @param ctx the parse tree
	 */
	void enterUnit_name(OBCreateParser.Unit_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#unit_name}.
	 * @param ctx the parse tree
	 */
	void exitUnit_name(OBCreateParser.Unit_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#zone_name}.
	 * @param ctx the parse tree
	 */
	void enterZone_name(OBCreateParser.Zone_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#zone_name}.
	 * @param ctx the parse tree
	 */
	void exitZone_name(OBCreateParser.Zone_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#region_name}.
	 * @param ctx the parse tree
	 */
	void enterRegion_name(OBCreateParser.Region_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#region_name}.
	 * @param ctx the parse tree
	 */
	void exitRegion_name(OBCreateParser.Region_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#database_name}.
	 * @param ctx the parse tree
	 */
	void enterDatabase_name(OBCreateParser.Database_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#database_name}.
	 * @param ctx the parse tree
	 */
	void exitDatabase_name(OBCreateParser.Database_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(OBCreateParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(OBCreateParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_alias_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_alias_name(OBCreateParser.Table_alias_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_alias_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_alias_name(OBCreateParser.Table_alias_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(OBCreateParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(OBCreateParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#column_alias_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_alias_name(OBCreateParser.Column_alias_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#column_alias_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_alias_name(OBCreateParser.Column_alias_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#partition_name}.
	 * @param ctx the parse tree
	 */
	void enterPartition_name(OBCreateParser.Partition_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#partition_name}.
	 * @param ctx the parse tree
	 */
	void exitPartition_name(OBCreateParser.Partition_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#subpartition_name}.
	 * @param ctx the parse tree
	 */
	void enterSubpartition_name(OBCreateParser.Subpartition_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#subpartition_name}.
	 * @param ctx the parse tree
	 */
	void exitSubpartition_name(OBCreateParser.Subpartition_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#index_name}.
	 * @param ctx the parse tree
	 */
	void enterIndex_name(OBCreateParser.Index_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#index_name}.
	 * @param ctx the parse tree
	 */
	void exitIndex_name(OBCreateParser.Index_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#view_name}.
	 * @param ctx the parse tree
	 */
	void enterView_name(OBCreateParser.View_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#view_name}.
	 * @param ctx the parse tree
	 */
	void exitView_name(OBCreateParser.View_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#object_name}.
	 * @param ctx the parse tree
	 */
	void enterObject_name(OBCreateParser.Object_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#object_name}.
	 * @param ctx the parse tree
	 */
	void exitObject_name(OBCreateParser.Object_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#constraint_name}.
	 * @param ctx the parse tree
	 */
	void enterConstraint_name(OBCreateParser.Constraint_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#constraint_name}.
	 * @param ctx the parse tree
	 */
	void exitConstraint_name(OBCreateParser.Constraint_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tablegroup_name}.
	 * @param ctx the parse tree
	 */
	void enterTablegroup_name(OBCreateParser.Tablegroup_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tablegroup_name}.
	 * @param ctx the parse tree
	 */
	void exitTablegroup_name(OBCreateParser.Tablegroup_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#outline_name}.
	 * @param ctx the parse tree
	 */
	void enterOutline_name(OBCreateParser.Outline_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#outline_name}.
	 * @param ctx the parse tree
	 */
	void exitOutline_name(OBCreateParser.Outline_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#user_name}.
	 * @param ctx the parse tree
	 */
	void enterUser_name(OBCreateParser.User_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#user_name}.
	 * @param ctx the parse tree
	 */
	void exitUser_name(OBCreateParser.User_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_factor}.
	 * @param ctx the parse tree
	 */
	void enterTable_factor(OBCreateParser.Table_factorContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_factor}.
	 * @param ctx the parse tree
	 */
	void exitTable_factor(OBCreateParser.Table_factorContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#column_factor}.
	 * @param ctx the parse tree
	 */
	void enterColumn_factor(OBCreateParser.Column_factorContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#column_factor}.
	 * @param ctx the parse tree
	 */
	void exitColumn_factor(OBCreateParser.Column_factorContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(OBCreateParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(OBCreateParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#constant_value}.
	 * @param ctx the parse tree
	 */
	void enterConstant_value(OBCreateParser.Constant_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#constant_value}.
	 * @param ctx the parse tree
	 */
	void exitConstant_value(OBCreateParser.Constant_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#operator_expression}.
	 * @param ctx the parse tree
	 */
	void enterOperator_expression(OBCreateParser.Operator_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#operator_expression}.
	 * @param ctx the parse tree
	 */
	void exitOperator_expression(OBCreateParser.Operator_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#function_expression}.
	 * @param ctx the parse tree
	 */
	void enterFunction_expression(OBCreateParser.Function_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#function_expression}.
	 * @param ctx the parse tree
	 */
	void exitFunction_expression(OBCreateParser.Function_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#primary_zone}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_zone(OBCreateParser.Primary_zoneContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#primary_zone}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_zone(OBCreateParser.Primary_zoneContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#zone_list}.
	 * @param ctx the parse tree
	 */
	void enterZone_list(OBCreateParser.Zone_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#zone_list}.
	 * @param ctx the parse tree
	 */
	void exitZone_list(OBCreateParser.Zone_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#replica_num}.
	 * @param ctx the parse tree
	 */
	void enterReplica_num(OBCreateParser.Replica_numContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#replica_num}.
	 * @param ctx the parse tree
	 */
	void exitReplica_num(OBCreateParser.Replica_numContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#tablegroup}.
	 * @param ctx the parse tree
	 */
	void enterTablegroup(OBCreateParser.TablegroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#tablegroup}.
	 * @param ctx the parse tree
	 */
	void exitTablegroup(OBCreateParser.TablegroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#default_tablegroup}.
	 * @param ctx the parse tree
	 */
	void enterDefault_tablegroup(OBCreateParser.Default_tablegroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#default_tablegroup}.
	 * @param ctx the parse tree
	 */
	void exitDefault_tablegroup(OBCreateParser.Default_tablegroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#table_tablegroup}.
	 * @param ctx the parse tree
	 */
	void enterTable_tablegroup(OBCreateParser.Table_tablegroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#table_tablegroup}.
	 * @param ctx the parse tree
	 */
	void exitTable_tablegroup(OBCreateParser.Table_tablegroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#index_desc}.
	 * @param ctx the parse tree
	 */
	void enterIndex_desc(OBCreateParser.Index_descContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#index_desc}.
	 * @param ctx the parse tree
	 */
	void exitIndex_desc(OBCreateParser.Index_descContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#index_type}.
	 * @param ctx the parse tree
	 */
	void enterIndex_type(OBCreateParser.Index_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#index_type}.
	 * @param ctx the parse tree
	 */
	void exitIndex_type(OBCreateParser.Index_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#charset_name}.
	 * @param ctx the parse tree
	 */
	void enterCharset_name(OBCreateParser.Charset_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#charset_name}.
	 * @param ctx the parse tree
	 */
	void exitCharset_name(OBCreateParser.Charset_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#character_name}.
	 * @param ctx the parse tree
	 */
	void enterCharacter_name(OBCreateParser.Character_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#character_name}.
	 * @param ctx the parse tree
	 */
	void exitCharacter_name(OBCreateParser.Character_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void enterCollation_name(OBCreateParser.Collation_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void exitCollation_name(OBCreateParser.Collation_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#logging_name}.
	 * @param ctx the parse tree
	 */
	void enterLogging_name(OBCreateParser.Logging_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#logging_name}.
	 * @param ctx the parse tree
	 */
	void exitLogging_name(OBCreateParser.Logging_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#sqlServerCommonts}.
	 * @param ctx the parse tree
	 */
	void enterSqlServerCommonts(OBCreateParser.SqlServerCommontsContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#sqlServerCommonts}.
	 * @param ctx the parse tree
	 */
	void exitSqlServerCommonts(OBCreateParser.SqlServerCommontsContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#sqlServerCommentDesc}.
	 * @param ctx the parse tree
	 */
	void enterSqlServerCommentDesc(OBCreateParser.SqlServerCommentDescContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#sqlServerCommentDesc}.
	 * @param ctx the parse tree
	 */
	void exitSqlServerCommentDesc(OBCreateParser.SqlServerCommentDescContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#sqlServerSchemaDesc}.
	 * @param ctx the parse tree
	 */
	void enterSqlServerSchemaDesc(OBCreateParser.SqlServerSchemaDescContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#sqlServerSchemaDesc}.
	 * @param ctx the parse tree
	 */
	void exitSqlServerSchemaDesc(OBCreateParser.SqlServerSchemaDescContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#sqlServerTableDesc}.
	 * @param ctx the parse tree
	 */
	void enterSqlServerTableDesc(OBCreateParser.SqlServerTableDescContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#sqlServerTableDesc}.
	 * @param ctx the parse tree
	 */
	void exitSqlServerTableDesc(OBCreateParser.SqlServerTableDescContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#sqlServerColumnDesc}.
	 * @param ctx the parse tree
	 */
	void enterSqlServerColumnDesc(OBCreateParser.SqlServerColumnDescContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#sqlServerColumnDesc}.
	 * @param ctx the parse tree
	 */
	void exitSqlServerColumnDesc(OBCreateParser.SqlServerColumnDescContext ctx);
	/**
	 * Enter a parse tree produced by {@link OBCreateParser#sqlServer_comment}.
	 * @param ctx the parse tree
	 */
	void enterSqlServer_comment(OBCreateParser.SqlServer_commentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OBCreateParser#sqlServer_comment}.
	 * @param ctx the parse tree
	 */
	void exitSqlServer_comment(OBCreateParser.SqlServer_commentContext ctx);
}