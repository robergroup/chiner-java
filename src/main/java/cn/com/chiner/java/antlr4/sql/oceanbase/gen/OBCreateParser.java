// Generated from C:/Users/24584/IdeaProjects/chiner-java/src/main/resources/g4/oceanbase/OBCreate.g4 by ANTLR 4.13.1
package cn.com.chiner.java.antlr4.sql.oceanbase.gen;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class OBCreateParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, T__62=63, T__63=64, T__64=65, T__65=66, 
		T__66=67, T__67=68, T__68=69, T__69=70, T__70=71, T__71=72, T__72=73, 
		T__73=74, T__74=75, T__75=76, T__76=77, T__77=78, T__78=79, T__79=80, 
		T__80=81, T__81=82, T__82=83, T__83=84, T__84=85, T__85=86, T__86=87, 
		T__87=88, T__88=89, T__89=90, T__90=91, T__91=92, T__92=93, T__93=94, 
		T__94=95, T__95=96, T__96=97, T__97=98, T__98=99, T__99=100, T__100=101, 
		T__101=102, T__102=103, T__103=104, T__104=105, T__105=106, T__106=107, 
		T__107=108, T__108=109, T__109=110, T__110=111, T__111=112, T__112=113, 
		T__113=114, T__114=115, T__115=116, T__116=117, T__117=118, T__118=119, 
		T__119=120, T__120=121, T__121=122, T__122=123, T__123=124, T__124=125, 
		T__125=126, T__126=127, T__127=128, T__128=129, T__129=130, T__130=131, 
		T__131=132, T__132=133, T__133=134, T__134=135, T__135=136, T__136=137, 
		T__137=138, T__138=139, T__139=140, T__140=141, T__141=142, T__142=143, 
		T__143=144, T__144=145, T__145=146, T__146=147, T__147=148, T__148=149, 
		T__149=150, T__150=151, GENERATE_ALWAYS=152, TEMPORARY=153, UNSIGNED=154, 
		ZEROFILL=155, NATIONAL=156, CHARACTER_SET=157, COLLATE_SET=158, TINY=159, 
		BOOL=160, BOOLEAN=161, SMALLINT=162, MEDIUMINT=163, INTEGER=164, BIGINT=165, 
		DECIMAL=166, NUMERIC=167, FLOAT=168, DOUBLE=169, DOUBLE_PRICISION=170, 
		BIT=171, NUMBER=172, DATE=173, DATETIME=174, TIMESTAMP=175, TIME=176, 
		YEAR=177, CHAR=178, VARCHAR=179, VARCHAR2=180, CHARACTER_VARYING=181, 
		BINARY=182, VARBINARY=183, STRING=184, TINYBLOB=185, BLOB=186, MEDIUMBLOB=187, 
		LONGBBLOB=188, TINYTEXT=189, TEXT=190, MEDIUMTEXT=191, LONGTEXT=192, ENUM=193, 
		SET=194, JSON=195, GEOMETRY=196, POINT=197, LINESTRING=198, POLYGON=199, 
		MULTIPOINT=200, MULTILINESTRING=201, MULTIPOLYGON=202, GEOMETRYCOLLECTION=203, 
		INT=204, NAME=205, SE=206, BLANK=207, NEWLINE=208, ANNOTATITION=209;
	public static final int
		RULE_prog = 0, RULE_databaseDDL = 1, RULE_databaseUse = 2, RULE_ddl_table = 3, 
		RULE_ddl_primary_key = 4, RULE_ddl_index = 5, RULE_filter_index = 6, RULE_table_definition_list = 7, 
		RULE_table_definition = 8, RULE_column_definition_list = 9, RULE_column_definition = 10, 
		RULE_table_option_list = 11, RULE_table_option = 12, RULE_column_name_list = 13, 
		RULE_comments = 14, RULE_columnDesc = 15, RULE_autoIncrement = 16, RULE_notNull = 17, 
		RULE_null = 18, RULE_primaryKey = 19, RULE_uniqueKey = 20, RULE_character = 21, 
		RULE_collate = 22, RULE_charset = 23, RULE_default = 24, RULE_tableDesc = 25, 
		RULE_engine = 26, RULE_rowFormat = 27, RULE_tableAutoIncrement = 28, RULE_tableCharset = 29, 
		RULE_tableCollate = 30, RULE_comment = 31, RULE_comment_value = 32, RULE_data_type = 33, 
		RULE_tenant_name = 34, RULE_pool_name = 35, RULE_unit_name = 36, RULE_zone_name = 37, 
		RULE_region_name = 38, RULE_database_name = 39, RULE_table_name = 40, 
		RULE_table_alias_name = 41, RULE_column_name = 42, RULE_column_alias_name = 43, 
		RULE_partition_name = 44, RULE_subpartition_name = 45, RULE_index_name = 46, 
		RULE_view_name = 47, RULE_object_name = 48, RULE_constraint_name = 49, 
		RULE_tablegroup_name = 50, RULE_outline_name = 51, RULE_user_name = 52, 
		RULE_table_factor = 53, RULE_column_factor = 54, RULE_expression = 55, 
		RULE_constant_value = 56, RULE_operator_expression = 57, RULE_function_expression = 58, 
		RULE_primary_zone = 59, RULE_zone_list = 60, RULE_replica_num = 61, RULE_tablegroup = 62, 
		RULE_default_tablegroup = 63, RULE_table_tablegroup = 64, RULE_index_desc = 65, 
		RULE_index_type = 66, RULE_charset_name = 67, RULE_character_name = 68, 
		RULE_collation_name = 69, RULE_logging_name = 70, RULE_sqlServerCommonts = 71, 
		RULE_sqlServerCommentDesc = 72, RULE_sqlServerSchemaDesc = 73, RULE_sqlServerTableDesc = 74, 
		RULE_sqlServerColumnDesc = 75, RULE_sqlServer_comment = 76;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "databaseDDL", "databaseUse", "ddl_table", "ddl_primary_key", 
			"ddl_index", "filter_index", "table_definition_list", "table_definition", 
			"column_definition_list", "column_definition", "table_option_list", "table_option", 
			"column_name_list", "comments", "columnDesc", "autoIncrement", "notNull", 
			"null", "primaryKey", "uniqueKey", "character", "collate", "charset", 
			"default", "tableDesc", "engine", "rowFormat", "tableAutoIncrement", 
			"tableCharset", "tableCollate", "comment", "comment_value", "data_type", 
			"tenant_name", "pool_name", "unit_name", "zone_name", "region_name", 
			"database_name", "table_name", "table_alias_name", "column_name", "column_alias_name", 
			"partition_name", "subpartition_name", "index_name", "view_name", "object_name", 
			"constraint_name", "tablegroup_name", "outline_name", "user_name", "table_factor", 
			"column_factor", "expression", "constant_value", "operator_expression", 
			"function_expression", "primary_zone", "zone_list", "replica_num", "tablegroup", 
			"default_tablegroup", "table_tablegroup", "index_desc", "index_type", 
			"charset_name", "character_name", "collation_name", "logging_name", "sqlServerCommonts", 
			"sqlServerCommentDesc", "sqlServerSchemaDesc", "sqlServerTableDesc", 
			"sqlServerColumnDesc", "sqlServer_comment"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'CREATE '", "'create '", "'DATABASE '", "'database '", "'IF '", 
			"'if '", "'NOT '", "'not '", "'EXIST '", "'exist '", "';'", "'USE'", 
			"'use'", "'TABLE '", "'table '", "'['", "'].'", "']'", "'('", "')'", 
			"'DROP '", "'drop '", "'TABLES '", "'tables '", "'EXISTS '", "'exists '", 
			"'ALTER '", "'alter '", "'ADD '", "'add '", "','", "'UNIQUE INDEX '", 
			"'INDEX '", "'index '", "'ON '", "'on '", "'RENAME '", "'rename '", "'TO '", 
			"'to '", "'CONTRAINT '", "'contraint '", "'AS '", "'as '", "'VIRTUAL '", 
			"'virtual '", "'STORED '", "'stored '", "'DEFAULT '", "'default '", "'CHARSET '", 
			"'charset '", "'CHARACTER '", "'SET '", "'character '", "'set '", "'='", 
			"'COLLATE '", "'collate '", "'DUPLICATE_SCOPE '", "'duplicate_scope '", 
			"'none '", "'NONE '", "'CLUSTER '", "'cluster '", "'COMMENT'", "'comment'", 
			"'IS '", "'is '", "'COLUMN '", "'column '", "'.'", "'autoIncrement'", 
			"'auto_increment'", "'AUTO_INCREMENT'", "'IDENTITY'", "'identity'", "'IDENTITY('", 
			"'identity('", "'NOT NULL'", "'not null'", "'NULL'", "'null'", "'PRIMARY '", 
			"'primary '", "'KEY'", "'key'", "'UNIQUE '", "'unique '", "'CHARACTER SET'", 
			"'character set'", "'ENGINE'", "'engine'", "'ROW_FORMAT'", "'row_format'", 
			"'CHARSET'", "'charset'", "'COLLATE'", "'collate'", "'\\uFF1B'", "'\\uFF0C'", 
			"'/'", "'\\u3002'", "'\\uFF08'", "'\\uFF09'", "'-'", "'\\u2014\\u2014'", 
			"'\"'", "'\\u201C'", "'\\u201D'", "'%'", "'*'", "'+'", "'//'", "'PRIMARY_ZONE'", 
			"'primary_zone'", "'ZONE_LIST'", "'zone_list'", "'REPLICA_NUM'", "'replica_num'", 
			"'DEFAULT'", "'default'", "'TABLEGROUP'", "'tablegroup'", "'USING'", 
			"'BTREE'", "'using'", "'btree'", "'EXEC'", "'exec'", "'sp_addextendedproperty'", 
			"''MS_Description''", "'@name'", "'N'MS_Description''", "'@value'", "''SCHEMA''", 
			"''schema''", "'@level0type'", "'N'Schema''", "'@level0name'", "''TABLE''", 
			"''table''", "'@level1type'", "'N'Table''", "'@level1name'", "''COLUMN''", 
			"''column''", "'NULL_'", "'@level2type'", "'N'Column''", "'@level2name'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, "GENERATE_ALWAYS", "TEMPORARY", 
			"UNSIGNED", "ZEROFILL", "NATIONAL", "CHARACTER_SET", "COLLATE_SET", "TINY", 
			"BOOL", "BOOLEAN", "SMALLINT", "MEDIUMINT", "INTEGER", "BIGINT", "DECIMAL", 
			"NUMERIC", "FLOAT", "DOUBLE", "DOUBLE_PRICISION", "BIT", "NUMBER", "DATE", 
			"DATETIME", "TIMESTAMP", "TIME", "YEAR", "CHAR", "VARCHAR", "VARCHAR2", 
			"CHARACTER_VARYING", "BINARY", "VARBINARY", "STRING", "TINYBLOB", "BLOB", 
			"MEDIUMBLOB", "LONGBBLOB", "TINYTEXT", "TEXT", "MEDIUMTEXT", "LONGTEXT", 
			"ENUM", "SET", "JSON", "GEOMETRY", "POINT", "LINESTRING", "POLYGON", 
			"MULTIPOINT", "MULTILINESTRING", "MULTIPOLYGON", "GEOMETRYCOLLECTION", 
			"INT", "NAME", "SE", "BLANK", "NEWLINE", "ANNOTATITION"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "OBCreate.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public OBCreateParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProgContext extends ParserRuleContext {
		public List<DatabaseDDLContext> databaseDDL() {
			return getRuleContexts(DatabaseDDLContext.class);
		}
		public DatabaseDDLContext databaseDDL(int i) {
			return getRuleContext(DatabaseDDLContext.class,i);
		}
		public List<DatabaseUseContext> databaseUse() {
			return getRuleContexts(DatabaseUseContext.class);
		}
		public DatabaseUseContext databaseUse(int i) {
			return getRuleContext(DatabaseUseContext.class,i);
		}
		public List<Ddl_tableContext> ddl_table() {
			return getRuleContexts(Ddl_tableContext.class);
		}
		public Ddl_tableContext ddl_table(int i) {
			return getRuleContext(Ddl_tableContext.class,i);
		}
		public List<Ddl_primary_keyContext> ddl_primary_key() {
			return getRuleContexts(Ddl_primary_keyContext.class);
		}
		public Ddl_primary_keyContext ddl_primary_key(int i) {
			return getRuleContext(Ddl_primary_keyContext.class,i);
		}
		public List<Ddl_indexContext> ddl_index() {
			return getRuleContexts(Ddl_indexContext.class);
		}
		public Ddl_indexContext ddl_index(int i) {
			return getRuleContext(Ddl_indexContext.class,i);
		}
		public List<Filter_indexContext> filter_index() {
			return getRuleContexts(Filter_indexContext.class);
		}
		public Filter_indexContext filter_index(int i) {
			return getRuleContext(Filter_indexContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitProg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitProg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(160);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(154);
					databaseDDL();
					}
					break;
				case 2:
					{
					setState(155);
					databaseUse();
					}
					break;
				case 3:
					{
					setState(156);
					ddl_table();
					}
					break;
				case 4:
					{
					setState(157);
					ddl_primary_key();
					}
					break;
				case 5:
					{
					setState(158);
					ddl_index();
					}
					break;
				case 6:
					{
					setState(159);
					filter_index();
					}
					break;
				}
				}
				setState(162); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & 408956934L) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DatabaseDDLContext extends ParserRuleContext {
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode TEMPORARY() { return getToken(OBCreateParser.TEMPORARY, 0); }
		public DatabaseDDLContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_databaseDDL; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDatabaseDDL(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDatabaseDDL(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDatabaseDDL(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DatabaseDDLContext databaseDDL() throws RecognitionException {
		DatabaseDDLContext _localctx = new DatabaseDDLContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_databaseDDL);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			_la = _input.LA(1);
			if ( !(_la==T__0 || _la==T__1) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(166);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TEMPORARY) {
				{
				setState(165);
				match(TEMPORARY);
				}
			}

			setState(168);
			_la = _input.LA(1);
			if ( !(_la==T__2 || _la==T__3) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(172);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__4 || _la==T__5) {
				{
				setState(169);
				_la = _input.LA(1);
				if ( !(_la==T__4 || _la==T__5) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(170);
				_la = _input.LA(1);
				if ( !(_la==T__6 || _la==T__7) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(171);
				_la = _input.LA(1);
				if ( !(_la==T__8 || _la==T__9) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(174);
			database_name();
			setState(175);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DatabaseUseContext extends ParserRuleContext {
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public DatabaseUseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_databaseUse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDatabaseUse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDatabaseUse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDatabaseUse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DatabaseUseContext databaseUse() throws RecognitionException {
		DatabaseUseContext _localctx = new DatabaseUseContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_databaseUse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(177);
			_la = _input.LA(1);
			if ( !(_la==T__11 || _la==T__12) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(178);
			database_name();
			setState(179);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Ddl_tableContext extends ParserRuleContext {
		public List<Table_nameContext> table_name() {
			return getRuleContexts(Table_nameContext.class);
		}
		public Table_nameContext table_name(int i) {
			return getRuleContext(Table_nameContext.class,i);
		}
		public Table_definition_listContext table_definition_list() {
			return getRuleContext(Table_definition_listContext.class,0);
		}
		public TerminalNode TEMPORARY() { return getToken(OBCreateParser.TEMPORARY, 0); }
		public Tablegroup_nameContext tablegroup_name() {
			return getRuleContext(Tablegroup_nameContext.class,0);
		}
		public Table_option_listContext table_option_list() {
			return getRuleContext(Table_option_listContext.class,0);
		}
		public List<TableDescContext> tableDesc() {
			return getRuleContexts(TableDescContext.class);
		}
		public TableDescContext tableDesc(int i) {
			return getRuleContext(TableDescContext.class,i);
		}
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public Ddl_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ddl_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDdl_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDdl_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDdl_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ddl_tableContext ddl_table() throws RecognitionException {
		Ddl_tableContext _localctx = new Ddl_tableContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_ddl_table);
		int _la;
		try {
			setState(238);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case T__1:
				enterOuterAlt(_localctx, 1);
				{
				setState(181);
				_la = _input.LA(1);
				if ( !(_la==T__0 || _la==T__1) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(183);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==TEMPORARY) {
					{
					setState(182);
					match(TEMPORARY);
					}
				}

				setState(185);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(189);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__4 || _la==T__5) {
					{
					setState(186);
					_la = _input.LA(1);
					if ( !(_la==T__4 || _la==T__5) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(187);
					_la = _input.LA(1);
					if ( !(_la==T__6 || _la==T__7) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(188);
					_la = _input.LA(1);
					if ( !(_la==T__8 || _la==T__9) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(195);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(191);
					match(T__15);
					setState(192);
					tablegroup_name();
					setState(193);
					match(T__16);
					}
					break;
				}
				setState(198);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__15) {
					{
					setState(197);
					match(T__15);
					}
				}

				setState(200);
				table_name();
				setState(202);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__17) {
					{
					setState(201);
					match(T__17);
					}
				}

				setState(204);
				match(T__18);
				setState(205);
				table_definition_list();
				setState(206);
				match(T__19);
				setState(208);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
				case 1:
					{
					setState(207);
					table_option_list();
					}
					break;
				}
				setState(213);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & 2243005851369475L) != 0)) {
					{
					{
					setState(210);
					tableDesc();
					}
					}
					setState(215);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(217);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__65 || _la==T__66) {
					{
					setState(216);
					comment();
					}
				}

				setState(219);
				match(T__10);
				}
				break;
			case T__20:
			case T__21:
				enterOuterAlt(_localctx, 2);
				{
				setState(221);
				_la = _input.LA(1);
				if ( !(_la==T__20 || _la==T__21) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(225);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__22:
					{
					setState(222);
					match(T__22);
					}
					break;
				case T__23:
					{
					setState(223);
					match(T__23);
					}
					break;
				case T__13:
				case T__14:
					{
					setState(224);
					_la = _input.LA(1);
					if ( !(_la==T__13 || _la==T__14) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(229);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__4 || _la==T__5) {
					{
					setState(227);
					_la = _input.LA(1);
					if ( !(_la==T__4 || _la==T__5) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(228);
					_la = _input.LA(1);
					if ( !(_la==T__24 || _la==T__25) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(232); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(231);
					table_name();
					}
					}
					setState(234); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==NAME || _la==SE );
				setState(236);
				match(T__10);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Ddl_primary_keyContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public List<ColumnDescContext> columnDesc() {
			return getRuleContexts(ColumnDescContext.class);
		}
		public ColumnDescContext columnDesc(int i) {
			return getRuleContext(ColumnDescContext.class,i);
		}
		public Column_name_listContext column_name_list() {
			return getRuleContext(Column_name_listContext.class,0);
		}
		public Ddl_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ddl_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDdl_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDdl_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDdl_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ddl_primary_keyContext ddl_primary_key() throws RecognitionException {
		Ddl_primary_keyContext _localctx = new Ddl_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ddl_primary_key);
		int _la;
		try {
			setState(269);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(240);
				_la = _input.LA(1);
				if ( !(_la==T__26 || _la==T__27) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(241);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(242);
				table_name();
				setState(243);
				_la = _input.LA(1);
				if ( !(_la==T__28 || _la==T__29) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(244);
				columnDesc();
				setState(245);
				match(T__18);
				setState(246);
				column_name_list();
				setState(247);
				match(T__19);
				setState(248);
				match(T__10);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(250);
				_la = _input.LA(1);
				if ( !(_la==T__26 || _la==T__27) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(251);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(252);
				table_name();
				setState(253);
				_la = _input.LA(1);
				if ( !(_la==T__20 || _la==T__21) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(254);
				columnDesc();
				setState(255);
				match(T__30);
				setState(256);
				_la = _input.LA(1);
				if ( !(_la==T__28 || _la==T__29) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(257);
				columnDesc();
				setState(258);
				match(T__18);
				setState(259);
				column_name_list();
				setState(260);
				match(T__19);
				setState(261);
				match(T__10);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(263);
				_la = _input.LA(1);
				if ( !(_la==T__26 || _la==T__27) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(264);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(265);
				table_name();
				setState(266);
				_la = _input.LA(1);
				if ( !(_la==T__20 || _la==T__21) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(267);
				columnDesc();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Ddl_indexContext extends ParserRuleContext {
		public List<Index_nameContext> index_name() {
			return getRuleContexts(Index_nameContext.class);
		}
		public Index_nameContext index_name(int i) {
			return getRuleContext(Index_nameContext.class,i);
		}
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Column_name_listContext column_name_list() {
			return getRuleContext(Column_name_listContext.class,0);
		}
		public Ddl_indexContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ddl_index; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDdl_index(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDdl_index(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDdl_index(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ddl_indexContext ddl_index() throws RecognitionException {
		Ddl_indexContext _localctx = new Ddl_indexContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_ddl_index);
		int _la;
		try {
			setState(320);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(271);
				_la = _input.LA(1);
				if ( !(_la==T__0 || _la==T__1) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(274);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__31:
					{
					setState(272);
					match(T__31);
					}
					break;
				case T__32:
				case T__33:
					{
					setState(273);
					_la = _input.LA(1);
					if ( !(_la==T__32 || _la==T__33) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(276);
				index_name();
				setState(277);
				_la = _input.LA(1);
				if ( !(_la==T__34 || _la==T__35) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(278);
				table_name();
				setState(279);
				match(T__18);
				setState(280);
				column_name_list();
				setState(281);
				match(T__19);
				setState(282);
				match(T__10);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(284);
				_la = _input.LA(1);
				if ( !(_la==T__26 || _la==T__27) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(285);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(286);
				table_name();
				setState(287);
				_la = _input.LA(1);
				if ( !(_la==T__28 || _la==T__29) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(288);
				_la = _input.LA(1);
				if ( !(_la==T__32 || _la==T__33) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(289);
				index_name();
				setState(290);
				match(T__18);
				setState(291);
				column_name_list();
				setState(292);
				match(T__19);
				setState(293);
				match(T__10);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(295);
				_la = _input.LA(1);
				if ( !(_la==T__20 || _la==T__21) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(296);
				_la = _input.LA(1);
				if ( !(_la==T__32 || _la==T__33) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(297);
				index_name();
				setState(298);
				_la = _input.LA(1);
				if ( !(_la==T__34 || _la==T__35) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(299);
				table_name();
				setState(300);
				match(T__10);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(302);
				_la = _input.LA(1);
				if ( !(_la==T__26 || _la==T__27) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(303);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(304);
				table_name();
				setState(305);
				_la = _input.LA(1);
				if ( !(_la==T__20 || _la==T__21) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(306);
				_la = _input.LA(1);
				if ( !(_la==T__32 || _la==T__33) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(307);
				index_name();
				setState(308);
				match(T__10);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(310);
				_la = _input.LA(1);
				if ( !(_la==T__26 || _la==T__27) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(311);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(312);
				table_name();
				setState(313);
				_la = _input.LA(1);
				if ( !(_la==T__36 || _la==T__37) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(314);
				_la = _input.LA(1);
				if ( !(_la==T__32 || _la==T__33) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(315);
				index_name();
				setState(316);
				_la = _input.LA(1);
				if ( !(_la==T__38 || _la==T__39) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(317);
				index_name();
				setState(318);
				match(T__10);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Filter_indexContext extends ParserRuleContext {
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public Logging_nameContext logging_name() {
			return getRuleContext(Logging_nameContext.class,0);
		}
		public Filter_indexContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filter_index; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterFilter_index(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitFilter_index(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitFilter_index(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Filter_indexContext filter_index() throws RecognitionException {
		Filter_indexContext _localctx = new Filter_indexContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_filter_index);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(322);
			_la = _input.LA(1);
			if ( !(_la==T__26 || _la==T__27) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(323);
			_la = _input.LA(1);
			if ( !(_la==T__32 || _la==T__33) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(324);
			index_name();
			setState(325);
			logging_name();
			setState(326);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_definition_listContext extends ParserRuleContext {
		public List<Table_definitionContext> table_definition() {
			return getRuleContexts(Table_definitionContext.class);
		}
		public Table_definitionContext table_definition(int i) {
			return getRuleContext(Table_definitionContext.class,i);
		}
		public Table_definition_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_definition_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_definition_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_definition_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_definition_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_definition_listContext table_definition_list() throws RecognitionException {
		Table_definition_listContext _localctx = new Table_definition_listContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_table_definition_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(328);
			table_definition();
			setState(333);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__30) {
				{
				{
				setState(329);
				match(T__30);
				setState(330);
				table_definition();
				}
				}
				setState(335);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_definitionContext extends ParserRuleContext {
		public Column_definitionContext column_definition() {
			return getRuleContext(Column_definitionContext.class,0);
		}
		public ColumnDescContext columnDesc() {
			return getRuleContext(ColumnDescContext.class,0);
		}
		public Index_descContext index_desc() {
			return getRuleContext(Index_descContext.class,0);
		}
		public Constraint_nameContext constraint_name() {
			return getRuleContext(Constraint_nameContext.class,0);
		}
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public Table_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_definition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_definition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_definitionContext table_definition() throws RecognitionException {
		Table_definitionContext _localctx = new Table_definitionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_table_definition);
		int _la;
		try {
			setState(352);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(336);
				column_definition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(339);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__40 || _la==T__41) {
					{
					setState(337);
					_la = _input.LA(1);
					if ( !(_la==T__40 || _la==T__41) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(338);
					constraint_name();
					}
				}

				setState(341);
				columnDesc();
				setState(342);
				index_desc();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__40 || _la==T__41) {
					{
					setState(344);
					_la = _input.LA(1);
					if ( !(_la==T__40 || _la==T__41) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(345);
					constraint_name();
					}
				}

				setState(348);
				columnDesc();
				setState(349);
				index_name();
				setState(350);
				index_desc();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Column_definition_listContext extends ParserRuleContext {
		public List<Column_definitionContext> column_definition() {
			return getRuleContexts(Column_definitionContext.class);
		}
		public Column_definitionContext column_definition(int i) {
			return getRuleContext(Column_definitionContext.class,i);
		}
		public Column_definition_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_definition_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumn_definition_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumn_definition_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumn_definition_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_definition_listContext column_definition_list() throws RecognitionException {
		Column_definition_listContext _localctx = new Column_definition_listContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_column_definition_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(354);
			column_definition();
			setState(359);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__30) {
				{
				{
				setState(355);
				match(T__30);
				setState(356);
				column_definition();
				}
				}
				setState(361);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Column_definitionContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public List<ColumnDescContext> columnDesc() {
			return getRuleContexts(ColumnDescContext.class);
		}
		public ColumnDescContext columnDesc(int i) {
			return getRuleContext(ColumnDescContext.class,i);
		}
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public CharsetContext charset() {
			return getRuleContext(CharsetContext.class,0);
		}
		public CollateContext collate() {
			return getRuleContext(CollateContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode GENERATE_ALWAYS() { return getToken(OBCreateParser.GENERATE_ALWAYS, 0); }
		public UniqueKeyContext uniqueKey() {
			return getRuleContext(UniqueKeyContext.class,0);
		}
		public Column_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumn_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumn_definition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumn_definition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_definitionContext column_definition() throws RecognitionException {
		Column_definitionContext _localctx = new Column_definitionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_column_definition);
		int _la;
		try {
			int _alt;
			setState(409);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(362);
				column_name();
				setState(363);
				data_type();
				setState(367);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(364);
						columnDesc();
						}
						} 
					}
					setState(369);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				}
				setState(371);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__65 || _la==T__66) {
					{
					setState(370);
					comment();
					}
				}

				setState(374);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__50 || _la==T__51) {
					{
					setState(373);
					charset();
					}
				}

				setState(377);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__57 || _la==T__58) {
					{
					setState(376);
					collate();
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(379);
				column_name();
				setState(380);
				data_type();
				setState(382);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==GENERATE_ALWAYS) {
					{
					setState(381);
					match(GENERATE_ALWAYS);
					}
				}

				setState(384);
				_la = _input.LA(1);
				if ( !(_la==T__42 || _la==T__43) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(385);
				expression();
				setState(387);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 527765581332480L) != 0)) {
					{
					setState(386);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 527765581332480L) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(390);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__87 || _la==T__88) {
					{
					setState(389);
					uniqueKey();
					}
				}

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(392);
				column_name();
				setState(394);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 159)) & ~0x3f) == 0 && ((1L << (_la - 159)) & 35184372088831L) != 0)) {
					{
					setState(393);
					data_type();
					}
				}

				setState(397);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==GENERATE_ALWAYS) {
					{
					setState(396);
					match(GENERATE_ALWAYS);
					}
				}

				setState(399);
				_la = _input.LA(1);
				if ( !(_la==T__42 || _la==T__43) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(400);
				match(T__18);
				setState(401);
				expression();
				setState(402);
				match(T__19);
				setState(404);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 527765581332480L) != 0)) {
					{
					setState(403);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 527765581332480L) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(407);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__87 || _la==T__88) {
					{
					setState(406);
					uniqueKey();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_option_listContext extends ParserRuleContext {
		public List<Table_optionContext> table_option() {
			return getRuleContexts(Table_optionContext.class);
		}
		public Table_optionContext table_option(int i) {
			return getRuleContext(Table_optionContext.class,i);
		}
		public Table_option_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_option_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_option_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_option_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_option_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_option_listContext table_option_list() throws RecognitionException {
		Table_option_listContext _localctx = new Table_option_listContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_table_option_list);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(412); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(411);
					table_option();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(414); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_optionContext extends ParserRuleContext {
		public Charset_nameContext charset_name() {
			return getRuleContext(Charset_nameContext.class,0);
		}
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public Table_tablegroupContext table_tablegroup() {
			return getRuleContext(Table_tablegroupContext.class,0);
		}
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public Table_optionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_option; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_option(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_option(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_option(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_optionContext table_option() throws RecognitionException {
		Table_optionContext _localctx = new Table_optionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_table_option);
		int _la;
		try {
			setState(439);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(416);
				_la = _input.LA(1);
				if ( !(_la==T__48 || _la==T__49) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(423);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__50:
					{
					setState(417);
					match(T__50);
					}
					break;
				case T__51:
					{
					setState(418);
					match(T__51);
					}
					break;
				case T__52:
					{
					setState(419);
					match(T__52);
					setState(420);
					match(T__53);
					}
					break;
				case T__54:
					{
					setState(421);
					match(T__54);
					setState(422);
					match(T__55);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(425);
				match(T__56);
				setState(426);
				charset_name();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(427);
				_la = _input.LA(1);
				if ( !(_la==T__48 || _la==T__49) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(428);
				_la = _input.LA(1);
				if ( !(_la==T__57 || _la==T__58) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(429);
				match(T__56);
				setState(430);
				collation_name();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(431);
				table_tablegroup();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(432);
				comment();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(433);
				_la = _input.LA(1);
				if ( !(_la==T__59 || _la==T__60) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(434);
				match(T__56);
				setState(435);
				match(T__61);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(436);
				match(T__62);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(437);
				match(T__63);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(438);
				match(T__64);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Column_name_listContext extends ParserRuleContext {
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public Column_name_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumn_name_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumn_name_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumn_name_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_name_listContext column_name_list() throws RecognitionException {
		Column_name_listContext _localctx = new Column_name_listContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_column_name_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(441);
			column_name();
			setState(446);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__30) {
				{
				{
				setState(442);
				match(T__30);
				setState(443);
				column_name();
				}
				}
				setState(448);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CommentsContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Comment_valueContext comment_value() {
			return getRuleContext(Comment_valueContext.class,0);
		}
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public SqlServerCommontsContext sqlServerCommonts() {
			return getRuleContext(SqlServerCommontsContext.class,0);
		}
		public CommentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterComments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitComments(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitComments(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommentsContext comments() throws RecognitionException {
		CommentsContext _localctx = new CommentsContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_comments);
		int _la;
		try {
			setState(468);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(449);
				_la = _input.LA(1);
				if ( !(_la==T__65 || _la==T__66) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(450);
				_la = _input.LA(1);
				if ( !(_la==T__34 || _la==T__35) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(451);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__14) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(452);
				table_name();
				setState(453);
				_la = _input.LA(1);
				if ( !(_la==T__67 || _la==T__68) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(454);
				comment_value();
				setState(455);
				match(T__10);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(457);
				_la = _input.LA(1);
				if ( !(_la==T__65 || _la==T__66) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(458);
				_la = _input.LA(1);
				if ( !(_la==T__34 || _la==T__35) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(459);
				_la = _input.LA(1);
				if ( !(_la==T__69 || _la==T__70) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(460);
				table_name();
				setState(461);
				match(T__71);
				setState(462);
				column_name();
				setState(463);
				_la = _input.LA(1);
				if ( !(_la==T__67 || _la==T__68) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(464);
				comment_value();
				setState(465);
				match(T__10);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(467);
				sqlServerCommonts();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ColumnDescContext extends ParserRuleContext {
		public TerminalNode UNSIGNED() { return getToken(OBCreateParser.UNSIGNED, 0); }
		public AutoIncrementContext autoIncrement() {
			return getRuleContext(AutoIncrementContext.class,0);
		}
		public PrimaryKeyContext primaryKey() {
			return getRuleContext(PrimaryKeyContext.class,0);
		}
		public UniqueKeyContext uniqueKey() {
			return getRuleContext(UniqueKeyContext.class,0);
		}
		public CharacterContext character() {
			return getRuleContext(CharacterContext.class,0);
		}
		public CollateContext collate() {
			return getRuleContext(CollateContext.class,0);
		}
		public CharsetContext charset() {
			return getRuleContext(CharsetContext.class,0);
		}
		public DefaultContext default_() {
			return getRuleContext(DefaultContext.class,0);
		}
		public ColumnDescContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnDesc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumnDesc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumnDesc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumnDesc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnDescContext columnDesc() throws RecognitionException {
		ColumnDescContext _localctx = new ColumnDescContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_columnDesc);
		try {
			setState(478);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case UNSIGNED:
				enterOuterAlt(_localctx, 1);
				{
				setState(470);
				match(UNSIGNED);
				}
				break;
			case T__72:
			case T__73:
			case T__74:
			case T__75:
			case T__76:
			case T__77:
			case T__78:
				enterOuterAlt(_localctx, 2);
				{
				setState(471);
				autoIncrement();
				}
				break;
			case T__83:
			case T__84:
			case T__85:
			case T__86:
				enterOuterAlt(_localctx, 3);
				{
				setState(472);
				primaryKey();
				}
				break;
			case T__87:
			case T__88:
				enterOuterAlt(_localctx, 4);
				{
				setState(473);
				uniqueKey();
				}
				break;
			case T__89:
			case T__90:
				enterOuterAlt(_localctx, 5);
				{
				setState(474);
				character();
				}
				break;
			case T__57:
			case T__58:
				enterOuterAlt(_localctx, 6);
				{
				setState(475);
				collate();
				}
				break;
			case T__50:
			case T__51:
				enterOuterAlt(_localctx, 7);
				{
				setState(476);
				charset();
				}
				break;
			case T__10:
			case T__15:
			case T__17:
			case T__18:
			case T__19:
			case T__30:
			case T__48:
			case T__49:
			case T__71:
			case T__79:
			case T__80:
			case T__81:
			case T__82:
			case T__99:
			case T__100:
			case T__101:
			case T__102:
			case T__103:
			case T__104:
			case T__105:
			case T__106:
			case T__107:
			case T__108:
			case T__109:
			case T__110:
			case INT:
			case NAME:
			case SE:
				enterOuterAlt(_localctx, 8);
				{
				setState(477);
				default_();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AutoIncrementContext extends ParserRuleContext {
		public List<TerminalNode> INT() { return getTokens(OBCreateParser.INT); }
		public TerminalNode INT(int i) {
			return getToken(OBCreateParser.INT, i);
		}
		public AutoIncrementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_autoIncrement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterAutoIncrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitAutoIncrement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitAutoIncrement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AutoIncrementContext autoIncrement() throws RecognitionException {
		AutoIncrementContext _localctx = new AutoIncrementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_autoIncrement);
		int _la;
		try {
			setState(499);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(480);
				_la = _input.LA(1);
				if ( !(((((_la - 73)) & ~0x3f) == 0 && ((1L << (_la - 73)) & 7L) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(481);
				match(T__75);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(482);
				match(T__76);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(483);
				match(T__77);
				setState(484);
				match(INT);
				setState(485);
				match(T__19);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(486);
				match(T__78);
				setState(487);
				match(INT);
				setState(488);
				match(T__19);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(489);
				match(T__77);
				setState(490);
				match(INT);
				setState(491);
				match(T__30);
				setState(492);
				match(INT);
				setState(493);
				match(T__19);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(494);
				match(T__78);
				setState(495);
				match(INT);
				setState(496);
				match(T__30);
				setState(497);
				match(INT);
				setState(498);
				match(T__19);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NotNullContext extends ParserRuleContext {
		public NotNullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notNull; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterNotNull(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitNotNull(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitNotNull(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotNullContext notNull() throws RecognitionException {
		NotNullContext _localctx = new NotNullContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_notNull);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(501);
			_la = _input.LA(1);
			if ( !(_la==T__79 || _la==T__80) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NullContext extends ParserRuleContext {
		public NullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterNull(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitNull(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitNull(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullContext null_() throws RecognitionException {
		NullContext _localctx = new NullContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_null);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(503);
			_la = _input.LA(1);
			if ( !(_la==T__81 || _la==T__82) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PrimaryKeyContext extends ParserRuleContext {
		public PrimaryKeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryKey; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterPrimaryKey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitPrimaryKey(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitPrimaryKey(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryKeyContext primaryKey() throws RecognitionException {
		PrimaryKeyContext _localctx = new PrimaryKeyContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_primaryKey);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(506);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__83 || _la==T__84) {
				{
				setState(505);
				_la = _input.LA(1);
				if ( !(_la==T__83 || _la==T__84) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(508);
			_la = _input.LA(1);
			if ( !(_la==T__85 || _la==T__86) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class UniqueKeyContext extends ParserRuleContext {
		public UniqueKeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uniqueKey; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterUniqueKey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitUniqueKey(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitUniqueKey(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UniqueKeyContext uniqueKey() throws RecognitionException {
		UniqueKeyContext _localctx = new UniqueKeyContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_uniqueKey);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(510);
			_la = _input.LA(1);
			if ( !(_la==T__87 || _la==T__88) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(513);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,44,_ctx) ) {
			case 1:
				{
				setState(511);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 2:
				{
				setState(512);
				_la = _input.LA(1);
				if ( !(_la==T__32 || _la==T__33) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CharacterContext extends ParserRuleContext {
		public Character_nameContext character_name() {
			return getRuleContext(Character_nameContext.class,0);
		}
		public CharacterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_character; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterCharacter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitCharacter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitCharacter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharacterContext character() throws RecognitionException {
		CharacterContext _localctx = new CharacterContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_character);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(515);
			_la = _input.LA(1);
			if ( !(_la==T__89 || _la==T__90) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(516);
			character_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CollateContext extends ParserRuleContext {
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public CollateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterCollate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitCollate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitCollate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollateContext collate() throws RecognitionException {
		CollateContext _localctx = new CollateContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_collate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(518);
			_la = _input.LA(1);
			if ( !(_la==T__57 || _la==T__58) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(519);
			collation_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CharsetContext extends ParserRuleContext {
		public Charset_nameContext charset_name() {
			return getRuleContext(Charset_nameContext.class,0);
		}
		public CharsetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterCharset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitCharset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitCharset(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharsetContext charset() throws RecognitionException {
		CharsetContext _localctx = new CharsetContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_charset);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(521);
			_la = _input.LA(1);
			if ( !(_la==T__50 || _la==T__51) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(522);
			charset_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DefaultContext extends ParserRuleContext {
		public Comment_valueContext comment_value() {
			return getRuleContext(Comment_valueContext.class,0);
		}
		public NullContext null_() {
			return getRuleContext(NullContext.class,0);
		}
		public NotNullContext notNull() {
			return getRuleContext(NotNullContext.class,0);
		}
		public DefaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDefault(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDefault(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDefault(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefaultContext default_() throws RecognitionException {
		DefaultContext _localctx = new DefaultContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_default);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(525);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__48 || _la==T__49) {
				{
				setState(524);
				_la = _input.LA(1);
				if ( !(_la==T__48 || _la==T__49) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(530);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				{
				setState(527);
				comment_value();
				}
				break;
			case 2:
				{
				setState(528);
				null_();
				}
				break;
			case 3:
				{
				setState(529);
				notNull();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TableDescContext extends ParserRuleContext {
		public EngineContext engine() {
			return getRuleContext(EngineContext.class,0);
		}
		public TableCharsetContext tableCharset() {
			return getRuleContext(TableCharsetContext.class,0);
		}
		public TableCollateContext tableCollate() {
			return getRuleContext(TableCollateContext.class,0);
		}
		public TableAutoIncrementContext tableAutoIncrement() {
			return getRuleContext(TableAutoIncrementContext.class,0);
		}
		public RowFormatContext rowFormat() {
			return getRuleContext(RowFormatContext.class,0);
		}
		public TableDescContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableDesc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTableDesc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTableDesc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTableDesc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableDescContext tableDesc() throws RecognitionException {
		TableDescContext _localctx = new TableDescContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_tableDesc);
		try {
			setState(537);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(532);
				engine();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(533);
				tableCharset();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(534);
				tableCollate();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(535);
				tableAutoIncrement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(536);
				rowFormat();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EngineContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public EngineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_engine; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterEngine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitEngine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitEngine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EngineContext engine() throws RecognitionException {
		EngineContext _localctx = new EngineContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_engine);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(539);
			_la = _input.LA(1);
			if ( !(_la==T__91 || _la==T__92) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(540);
			match(T__56);
			setState(541);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RowFormatContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public RowFormatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rowFormat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterRowFormat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitRowFormat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitRowFormat(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RowFormatContext rowFormat() throws RecognitionException {
		RowFormatContext _localctx = new RowFormatContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_rowFormat);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(543);
			_la = _input.LA(1);
			if ( !(_la==T__93 || _la==T__94) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(544);
			match(T__56);
			setState(545);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TableAutoIncrementContext extends ParserRuleContext {
		public AutoIncrementContext autoIncrement() {
			return getRuleContext(AutoIncrementContext.class,0);
		}
		public TerminalNode INT() { return getToken(OBCreateParser.INT, 0); }
		public TableAutoIncrementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableAutoIncrement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTableAutoIncrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTableAutoIncrement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTableAutoIncrement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableAutoIncrementContext tableAutoIncrement() throws RecognitionException {
		TableAutoIncrementContext _localctx = new TableAutoIncrementContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_tableAutoIncrement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(547);
			autoIncrement();
			setState(548);
			match(T__56);
			setState(549);
			match(INT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TableCharsetContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public TableCharsetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableCharset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTableCharset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTableCharset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTableCharset(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableCharsetContext tableCharset() throws RecognitionException {
		TableCharsetContext _localctx = new TableCharsetContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_tableCharset);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(552);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__48 || _la==T__49) {
				{
				setState(551);
				_la = _input.LA(1);
				if ( !(_la==T__48 || _la==T__49) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(554);
			_la = _input.LA(1);
			if ( !(_la==T__95 || _la==T__96) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(555);
			match(T__56);
			setState(556);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TableCollateContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public TableCollateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableCollate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTableCollate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTableCollate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTableCollate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableCollateContext tableCollate() throws RecognitionException {
		TableCollateContext _localctx = new TableCollateContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_tableCollate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(559);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__48 || _la==T__49) {
				{
				setState(558);
				_la = _input.LA(1);
				if ( !(_la==T__48 || _la==T__49) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(561);
			_la = _input.LA(1);
			if ( !(_la==T__97 || _la==T__98) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(562);
			match(T__56);
			setState(563);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CommentContext extends ParserRuleContext {
		public Comment_valueContext comment_value() {
			return getRuleContext(Comment_valueContext.class,0);
		}
		public CommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterComment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitComment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitComment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommentContext comment() throws RecognitionException {
		CommentContext _localctx = new CommentContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_comment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(565);
			_la = _input.LA(1);
			if ( !(_la==T__65 || _la==T__66) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(567);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__56) {
				{
				setState(566);
				match(T__56);
				}
			}

			setState(569);
			comment_value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Comment_valueContext extends ParserRuleContext {
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public List<TerminalNode> NAME() { return getTokens(OBCreateParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(OBCreateParser.NAME, i);
		}
		public List<TerminalNode> INT() { return getTokens(OBCreateParser.INT); }
		public TerminalNode INT(int i) {
			return getToken(OBCreateParser.INT, i);
		}
		public Comment_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comment_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterComment_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitComment_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitComment_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Comment_valueContext comment_value() throws RecognitionException {
		Comment_valueContext _localctx = new Comment_valueContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_comment_value);
		int _la;
		try {
			int _alt;
			setState(583);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(572);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SE) {
					{
					setState(571);
					match(SE);
					}
				}

				setState(575); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(574);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 2149386240L) != 0) || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & 1099243195393L) != 0) || _la==INT || _la==NAME) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(577); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(580);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
				case 1:
					{
					setState(579);
					match(SE);
					}
					break;
				}
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(582);
				match(INT);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Data_typeContext extends ParserRuleContext {
		public TerminalNode TINY() { return getToken(OBCreateParser.TINY, 0); }
		public TerminalNode BOOL() { return getToken(OBCreateParser.BOOL, 0); }
		public TerminalNode BOOLEAN() { return getToken(OBCreateParser.BOOLEAN, 0); }
		public TerminalNode SMALLINT() { return getToken(OBCreateParser.SMALLINT, 0); }
		public TerminalNode MEDIUMINT() { return getToken(OBCreateParser.MEDIUMINT, 0); }
		public TerminalNode INTEGER() { return getToken(OBCreateParser.INTEGER, 0); }
		public TerminalNode BIGINT() { return getToken(OBCreateParser.BIGINT, 0); }
		public TerminalNode DECIMAL() { return getToken(OBCreateParser.DECIMAL, 0); }
		public TerminalNode NUMBER() { return getToken(OBCreateParser.NUMBER, 0); }
		public TerminalNode NUMERIC() { return getToken(OBCreateParser.NUMERIC, 0); }
		public TerminalNode FLOAT() { return getToken(OBCreateParser.FLOAT, 0); }
		public TerminalNode DOUBLE() { return getToken(OBCreateParser.DOUBLE, 0); }
		public TerminalNode DOUBLE_PRICISION() { return getToken(OBCreateParser.DOUBLE_PRICISION, 0); }
		public TerminalNode BIT() { return getToken(OBCreateParser.BIT, 0); }
		public TerminalNode DATE() { return getToken(OBCreateParser.DATE, 0); }
		public TerminalNode DATETIME() { return getToken(OBCreateParser.DATETIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(OBCreateParser.TIMESTAMP, 0); }
		public TerminalNode TIME() { return getToken(OBCreateParser.TIME, 0); }
		public TerminalNode YEAR() { return getToken(OBCreateParser.YEAR, 0); }
		public TerminalNode CHAR() { return getToken(OBCreateParser.CHAR, 0); }
		public TerminalNode VARCHAR() { return getToken(OBCreateParser.VARCHAR, 0); }
		public TerminalNode VARCHAR2() { return getToken(OBCreateParser.VARCHAR2, 0); }
		public TerminalNode CHARACTER_VARYING() { return getToken(OBCreateParser.CHARACTER_VARYING, 0); }
		public TerminalNode BINARY() { return getToken(OBCreateParser.BINARY, 0); }
		public TerminalNode VARBINARY() { return getToken(OBCreateParser.VARBINARY, 0); }
		public TerminalNode STRING() { return getToken(OBCreateParser.STRING, 0); }
		public TerminalNode TINYBLOB() { return getToken(OBCreateParser.TINYBLOB, 0); }
		public TerminalNode BLOB() { return getToken(OBCreateParser.BLOB, 0); }
		public TerminalNode MEDIUMBLOB() { return getToken(OBCreateParser.MEDIUMBLOB, 0); }
		public TerminalNode LONGBBLOB() { return getToken(OBCreateParser.LONGBBLOB, 0); }
		public TerminalNode TINYTEXT() { return getToken(OBCreateParser.TINYTEXT, 0); }
		public TerminalNode TEXT() { return getToken(OBCreateParser.TEXT, 0); }
		public TerminalNode MEDIUMTEXT() { return getToken(OBCreateParser.MEDIUMTEXT, 0); }
		public TerminalNode LONGTEXT() { return getToken(OBCreateParser.LONGTEXT, 0); }
		public TerminalNode ENUM() { return getToken(OBCreateParser.ENUM, 0); }
		public TerminalNode SET() { return getToken(OBCreateParser.SET, 0); }
		public TerminalNode JSON() { return getToken(OBCreateParser.JSON, 0); }
		public TerminalNode GEOMETRY() { return getToken(OBCreateParser.GEOMETRY, 0); }
		public TerminalNode POINT() { return getToken(OBCreateParser.POINT, 0); }
		public TerminalNode LINESTRING() { return getToken(OBCreateParser.LINESTRING, 0); }
		public TerminalNode POLYGON() { return getToken(OBCreateParser.POLYGON, 0); }
		public TerminalNode MULTILINESTRING() { return getToken(OBCreateParser.MULTILINESTRING, 0); }
		public TerminalNode MULTIPOINT() { return getToken(OBCreateParser.MULTIPOINT, 0); }
		public TerminalNode MULTIPOLYGON() { return getToken(OBCreateParser.MULTIPOLYGON, 0); }
		public TerminalNode GEOMETRYCOLLECTION() { return getToken(OBCreateParser.GEOMETRYCOLLECTION, 0); }
		public Data_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_data_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterData_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitData_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitData_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Data_typeContext data_type() throws RecognitionException {
		Data_typeContext _localctx = new Data_typeContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_data_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(585);
			_la = _input.LA(1);
			if ( !(((((_la - 159)) & ~0x3f) == 0 && ((1L << (_la - 159)) & 35184372088831L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Tenant_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Tenant_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tenant_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTenant_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTenant_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTenant_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tenant_nameContext tenant_name() throws RecognitionException {
		Tenant_nameContext _localctx = new Tenant_nameContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_tenant_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(588);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(587);
				match(SE);
				}
			}

			setState(590);
			match(NAME);
			setState(592);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(591);
				match(SE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Pool_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Pool_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pool_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterPool_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitPool_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitPool_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Pool_nameContext pool_name() throws RecognitionException {
		Pool_nameContext _localctx = new Pool_nameContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_pool_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(594);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Unit_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Unit_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unit_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterUnit_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitUnit_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitUnit_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unit_nameContext unit_name() throws RecognitionException {
		Unit_nameContext _localctx = new Unit_nameContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_unit_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(596);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Zone_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Zone_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zone_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterZone_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitZone_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitZone_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Zone_nameContext zone_name() throws RecognitionException {
		Zone_nameContext _localctx = new Zone_nameContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_zone_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(598);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Region_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Region_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_region_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterRegion_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitRegion_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitRegion_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Region_nameContext region_name() throws RecognitionException {
		Region_nameContext _localctx = new Region_nameContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_region_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(600);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Database_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Database_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_database_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDatabase_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDatabase_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDatabase_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Database_nameContext database_name() throws RecognitionException {
		Database_nameContext _localctx = new Database_nameContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_database_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(603);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(602);
				match(SE);
				}
			}

			setState(605);
			match(NAME);
			setState(607);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(606);
				match(SE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_nameContext table_name() throws RecognitionException {
		Table_nameContext _localctx = new Table_nameContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_table_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(610);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(609);
				match(SE);
				}
			}

			setState(612);
			match(NAME);
			setState(614);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
			case 1:
				{
				setState(613);
				match(SE);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_alias_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Table_alias_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_alias_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_alias_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_alias_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_alias_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_alias_nameContext table_alias_name() throws RecognitionException {
		Table_alias_nameContext _localctx = new Table_alias_nameContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_table_alias_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(616);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Column_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumn_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumn_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumn_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_nameContext column_name() throws RecognitionException {
		Column_nameContext _localctx = new Column_nameContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_column_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(619);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(618);
				match(SE);
				}
			}

			setState(621);
			match(NAME);
			setState(623);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(622);
				match(SE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Column_alias_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Column_alias_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_alias_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumn_alias_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumn_alias_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumn_alias_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_alias_nameContext column_alias_name() throws RecognitionException {
		Column_alias_nameContext _localctx = new Column_alias_nameContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_column_alias_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(625);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Partition_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Partition_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_partition_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterPartition_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitPartition_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitPartition_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Partition_nameContext partition_name() throws RecognitionException {
		Partition_nameContext _localctx = new Partition_nameContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_partition_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(627);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Subpartition_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Subpartition_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subpartition_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSubpartition_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSubpartition_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSubpartition_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Subpartition_nameContext subpartition_name() throws RecognitionException {
		Subpartition_nameContext _localctx = new Subpartition_nameContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_subpartition_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(629);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Index_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Index_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterIndex_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitIndex_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitIndex_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_nameContext index_name() throws RecognitionException {
		Index_nameContext _localctx = new Index_nameContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_index_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(632);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(631);
				match(SE);
				}
			}

			setState(634);
			match(NAME);
			setState(636);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,64,_ctx) ) {
			case 1:
				{
				setState(635);
				match(SE);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class View_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public View_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_view_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterView_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitView_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitView_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final View_nameContext view_name() throws RecognitionException {
		View_nameContext _localctx = new View_nameContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_view_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(638);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Object_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Object_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_object_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterObject_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitObject_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitObject_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Object_nameContext object_name() throws RecognitionException {
		Object_nameContext _localctx = new Object_nameContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_object_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(640);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Constraint_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Constraint_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constraint_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterConstraint_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitConstraint_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitConstraint_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Constraint_nameContext constraint_name() throws RecognitionException {
		Constraint_nameContext _localctx = new Constraint_nameContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_constraint_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(642);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Tablegroup_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Tablegroup_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tablegroup_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTablegroup_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTablegroup_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTablegroup_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tablegroup_nameContext tablegroup_name() throws RecognitionException {
		Tablegroup_nameContext _localctx = new Tablegroup_nameContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_tablegroup_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(644);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Outline_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Outline_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_outline_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterOutline_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitOutline_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitOutline_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Outline_nameContext outline_name() throws RecognitionException {
		Outline_nameContext _localctx = new Outline_nameContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_outline_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(646);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class User_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public User_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_user_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterUser_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitUser_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitUser_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final User_nameContext user_name() throws RecognitionException {
		User_nameContext _localctx = new User_nameContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_user_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(648);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_factorContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public Table_factorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_factor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_factor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_factor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_factorContext table_factor() throws RecognitionException {
		Table_factorContext _localctx = new Table_factorContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_table_factor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(653);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				{
				setState(650);
				database_name();
				setState(651);
				match(T__71);
				}
				break;
			}
			setState(655);
			table_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Column_factorContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Table_factorContext table_factor() {
			return getRuleContext(Table_factorContext.class,0);
		}
		public Column_factorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterColumn_factor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitColumn_factor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitColumn_factor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_factorContext column_factor() throws RecognitionException {
		Column_factorContext _localctx = new Column_factorContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_column_factor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(660);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,66,_ctx) ) {
			case 1:
				{
				setState(657);
				table_factor();
				setState(658);
				match(T__71);
				}
				break;
			}
			setState(662);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExpressionContext extends ParserRuleContext {
		public Constant_valueContext constant_value() {
			return getRuleContext(Constant_valueContext.class,0);
		}
		public Column_factorContext column_factor() {
			return getRuleContext(Column_factorContext.class,0);
		}
		public Operator_expressionContext operator_expression() {
			return getRuleContext(Operator_expressionContext.class,0);
		}
		public Function_expressionContext function_expression() {
			return getRuleContext(Function_expressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_expression);
		try {
			setState(668);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(664);
				constant_value();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(665);
				column_factor();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(666);
				operator_expression();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(667);
				function_expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Constant_valueContext extends ParserRuleContext {
		public List<TerminalNode> INT() { return getTokens(OBCreateParser.INT); }
		public TerminalNode INT(int i) {
			return getToken(OBCreateParser.INT, i);
		}
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public Constant_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterConstant_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitConstant_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitConstant_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Constant_valueContext constant_value() throws RecognitionException {
		Constant_valueContext _localctx = new Constant_valueContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_constant_value);
		try {
			setState(675);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,68,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(670);
				match(INT);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(671);
				match(INT);
				setState(672);
				match(T__71);
				setState(673);
				match(INT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(674);
				match(NAME);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Operator_expressionContext extends ParserRuleContext {
		public List<TerminalNode> INT() { return getTokens(OBCreateParser.INT); }
		public TerminalNode INT(int i) {
			return getToken(OBCreateParser.INT, i);
		}
		public Operator_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterOperator_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitOperator_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitOperator_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Operator_expressionContext operator_expression() throws RecognitionException {
		Operator_expressionContext _localctx = new Operator_expressionContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_operator_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(677);
			match(INT);
			setState(682);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 102)) & ~0x3f) == 0 && ((1L << (_la - 102)) & 7697L) != 0)) {
				{
				{
				setState(678);
				_la = _input.LA(1);
				if ( !(((((_la - 102)) & ~0x3f) == 0 && ((1L << (_la - 102)) & 7697L) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(679);
				match(INT);
				}
				}
				setState(684);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Function_expressionContext extends ParserRuleContext {
		public List<TerminalNode> NAME() { return getTokens(OBCreateParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(OBCreateParser.NAME, i);
		}
		public Function_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterFunction_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitFunction_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitFunction_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_expressionContext function_expression() throws RecognitionException {
		Function_expressionContext _localctx = new Function_expressionContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_function_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(685);
			match(NAME);
			setState(686);
			match(T__18);
			setState(687);
			match(NAME);
			setState(692);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__30) {
				{
				{
				setState(688);
				match(T__30);
				setState(689);
				match(NAME);
				}
				}
				setState(694);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(695);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Primary_zoneContext extends ParserRuleContext {
		public Zone_nameContext zone_name() {
			return getRuleContext(Zone_nameContext.class,0);
		}
		public Primary_zoneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary_zone; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterPrimary_zone(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitPrimary_zone(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitPrimary_zone(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Primary_zoneContext primary_zone() throws RecognitionException {
		Primary_zoneContext _localctx = new Primary_zoneContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_primary_zone);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(697);
			_la = _input.LA(1);
			if ( !(_la==T__114 || _la==T__115) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(699);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__56) {
				{
				setState(698);
				match(T__56);
				}
			}

			setState(701);
			zone_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Zone_listContext extends ParserRuleContext {
		public List<Zone_nameContext> zone_name() {
			return getRuleContexts(Zone_nameContext.class);
		}
		public Zone_nameContext zone_name(int i) {
			return getRuleContext(Zone_nameContext.class,i);
		}
		public Zone_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zone_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterZone_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitZone_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitZone_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Zone_listContext zone_list() throws RecognitionException {
		Zone_listContext _localctx = new Zone_listContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_zone_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(703);
			_la = _input.LA(1);
			if ( !(_la==T__116 || _la==T__117) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(705);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__56) {
				{
				setState(704);
				match(T__56);
				}
			}

			setState(707);
			match(T__18);
			setState(711);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NAME) {
				{
				{
				setState(708);
				zone_name();
				}
				}
				setState(713);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(714);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Replica_numContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(OBCreateParser.INT, 0); }
		public Replica_numContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_replica_num; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterReplica_num(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitReplica_num(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitReplica_num(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Replica_numContext replica_num() throws RecognitionException {
		Replica_numContext _localctx = new Replica_numContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_replica_num);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(716);
			_la = _input.LA(1);
			if ( !(_la==T__118 || _la==T__119) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(718);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__56) {
				{
				setState(717);
				match(T__56);
				}
			}

			setState(720);
			match(INT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TablegroupContext extends ParserRuleContext {
		public Default_tablegroupContext default_tablegroup() {
			return getRuleContext(Default_tablegroupContext.class,0);
		}
		public Table_tablegroupContext table_tablegroup() {
			return getRuleContext(Table_tablegroupContext.class,0);
		}
		public TablegroupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tablegroup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTablegroup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTablegroup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTablegroup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TablegroupContext tablegroup() throws RecognitionException {
		TablegroupContext _localctx = new TablegroupContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_tablegroup);
		try {
			setState(724);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__120:
			case T__121:
				enterOuterAlt(_localctx, 1);
				{
				setState(722);
				default_tablegroup();
				}
				break;
			case T__122:
			case T__123:
				enterOuterAlt(_localctx, 2);
				{
				setState(723);
				table_tablegroup();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Default_tablegroupContext extends ParserRuleContext {
		public Tablegroup_nameContext tablegroup_name() {
			return getRuleContext(Tablegroup_nameContext.class,0);
		}
		public Default_tablegroupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_tablegroup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterDefault_tablegroup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitDefault_tablegroup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitDefault_tablegroup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_tablegroupContext default_tablegroup() throws RecognitionException {
		Default_tablegroupContext _localctx = new Default_tablegroupContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_default_tablegroup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(726);
			_la = _input.LA(1);
			if ( !(_la==T__120 || _la==T__121) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(727);
			_la = _input.LA(1);
			if ( !(_la==T__122 || _la==T__123) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			{
			setState(728);
			match(T__56);
			}
			setState(729);
			tablegroup_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_tablegroupContext extends ParserRuleContext {
		public Tablegroup_nameContext tablegroup_name() {
			return getRuleContext(Tablegroup_nameContext.class,0);
		}
		public Table_tablegroupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_tablegroup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterTable_tablegroup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitTable_tablegroup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitTable_tablegroup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_tablegroupContext table_tablegroup() throws RecognitionException {
		Table_tablegroupContext _localctx = new Table_tablegroupContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_table_tablegroup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(731);
			_la = _input.LA(1);
			if ( !(_la==T__122 || _la==T__123) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(733);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__56) {
				{
				setState(732);
				match(T__56);
				}
			}

			setState(735);
			tablegroup_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Index_descContext extends ParserRuleContext {
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public Index_typeContext index_type() {
			return getRuleContext(Index_typeContext.class,0);
		}
		public Index_descContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_desc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterIndex_desc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitIndex_desc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitIndex_desc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_descContext index_desc() throws RecognitionException {
		Index_descContext _localctx = new Index_descContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_index_desc);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(737);
			match(T__18);
			setState(738);
			column_name();
			setState(743);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__30) {
				{
				{
				setState(739);
				match(T__30);
				setState(740);
				column_name();
				}
				}
				setState(745);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(746);
			match(T__19);
			setState(748);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__124 || _la==T__126) {
				{
				setState(747);
				index_type();
				}
			}

			setState(751);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
			case 1:
				{
				setState(750);
				match(T__30);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Index_typeContext extends ParserRuleContext {
		public Index_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterIndex_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitIndex_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitIndex_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_typeContext index_type() throws RecognitionException {
		Index_typeContext _localctx = new Index_typeContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_index_type);
		try {
			setState(757);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__124:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(753);
				match(T__124);
				setState(754);
				match(T__125);
				}
				}
				break;
			case T__126:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(755);
				match(T__126);
				setState(756);
				match(T__127);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Charset_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Charset_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charset_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterCharset_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitCharset_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitCharset_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Charset_nameContext charset_name() throws RecognitionException {
		Charset_nameContext _localctx = new Charset_nameContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_charset_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(760);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(759);
				match(SE);
				}
			}

			setState(762);
			match(NAME);
			setState(764);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				{
				setState(763);
				match(SE);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Character_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Character_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_character_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterCharacter_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitCharacter_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitCharacter_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Character_nameContext character_name() throws RecognitionException {
		Character_nameContext _localctx = new Character_nameContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_character_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(767);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(766);
				match(SE);
				}
			}

			setState(769);
			match(NAME);
			setState(771);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,84,_ctx) ) {
			case 1:
				{
				setState(770);
				match(SE);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Collation_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Collation_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collation_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterCollation_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitCollation_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitCollation_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Collation_nameContext collation_name() throws RecognitionException {
		Collation_nameContext _localctx = new Collation_nameContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_collation_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(774);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(773);
				match(SE);
				}
			}

			setState(776);
			match(NAME);
			setState(778);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,86,_ctx) ) {
			case 1:
				{
				setState(777);
				match(SE);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Logging_nameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(OBCreateParser.NAME, 0); }
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public Logging_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logging_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterLogging_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitLogging_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitLogging_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logging_nameContext logging_name() throws RecognitionException {
		Logging_nameContext _localctx = new Logging_nameContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_logging_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(781);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(780);
				match(SE);
				}
			}

			setState(783);
			match(NAME);
			setState(785);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SE) {
				{
				setState(784);
				match(SE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SqlServerCommontsContext extends ParserRuleContext {
		public SqlServerCommentDescContext sqlServerCommentDesc() {
			return getRuleContext(SqlServerCommentDescContext.class,0);
		}
		public SqlServerSchemaDescContext sqlServerSchemaDesc() {
			return getRuleContext(SqlServerSchemaDescContext.class,0);
		}
		public SqlServerTableDescContext sqlServerTableDesc() {
			return getRuleContext(SqlServerTableDescContext.class,0);
		}
		public SqlServerColumnDescContext sqlServerColumnDesc() {
			return getRuleContext(SqlServerColumnDescContext.class,0);
		}
		public SqlServerCommontsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlServerCommonts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSqlServerCommonts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSqlServerCommonts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSqlServerCommonts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlServerCommontsContext sqlServerCommonts() throws RecognitionException {
		SqlServerCommontsContext _localctx = new SqlServerCommontsContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_sqlServerCommonts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(787);
			_la = _input.LA(1);
			if ( !(_la==T__128 || _la==T__129) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(788);
			match(T__130);
			setState(789);
			sqlServerCommentDesc();
			setState(790);
			sqlServerSchemaDesc();
			setState(791);
			sqlServerTableDesc();
			setState(792);
			sqlServerColumnDesc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SqlServerCommentDescContext extends ParserRuleContext {
		public SqlServer_commentContext sqlServer_comment() {
			return getRuleContext(SqlServer_commentContext.class,0);
		}
		public SqlServerCommentDescContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlServerCommentDesc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSqlServerCommentDesc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSqlServerCommentDesc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSqlServerCommentDesc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlServerCommentDescContext sqlServerCommentDesc() throws RecognitionException {
		SqlServerCommentDescContext _localctx = new SqlServerCommentDescContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_sqlServerCommentDesc);
		try {
			setState(808);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__131:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(794);
				match(T__131);
				setState(795);
				match(T__30);
				setState(796);
				sqlServer_comment();
				setState(797);
				match(T__30);
				}
				}
				break;
			case T__132:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(799);
				match(T__132);
				setState(800);
				match(T__56);
				setState(801);
				match(T__133);
				setState(802);
				match(T__30);
				setState(803);
				match(T__134);
				setState(804);
				match(T__56);
				setState(805);
				sqlServer_comment();
				setState(806);
				match(T__30);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SqlServerSchemaDescContext extends ParserRuleContext {
		public Tablegroup_nameContext tablegroup_name() {
			return getRuleContext(Tablegroup_nameContext.class,0);
		}
		public SqlServerSchemaDescContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlServerSchemaDesc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSqlServerSchemaDesc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSqlServerSchemaDesc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSqlServerSchemaDesc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlServerSchemaDescContext sqlServerSchemaDesc() throws RecognitionException {
		SqlServerSchemaDescContext _localctx = new SqlServerSchemaDescContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_sqlServerSchemaDesc);
		int _la;
		try {
			setState(824);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__135:
			case T__136:
				enterOuterAlt(_localctx, 1);
				{
				setState(810);
				_la = _input.LA(1);
				if ( !(_la==T__135 || _la==T__136) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(811);
				match(T__30);
				setState(812);
				tablegroup_name();
				setState(813);
				match(T__30);
				}
				break;
			case T__137:
				enterOuterAlt(_localctx, 2);
				{
				setState(815);
				match(T__137);
				setState(816);
				match(T__56);
				setState(817);
				match(T__138);
				setState(818);
				match(T__30);
				setState(819);
				match(T__139);
				setState(820);
				match(T__56);
				setState(821);
				tablegroup_name();
				setState(822);
				match(T__30);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SqlServerTableDescContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public SqlServerTableDescContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlServerTableDesc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSqlServerTableDesc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSqlServerTableDesc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSqlServerTableDesc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlServerTableDescContext sqlServerTableDesc() throws RecognitionException {
		SqlServerTableDescContext _localctx = new SqlServerTableDescContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_sqlServerTableDesc);
		int _la;
		try {
			setState(840);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__140:
			case T__141:
				enterOuterAlt(_localctx, 1);
				{
				setState(826);
				_la = _input.LA(1);
				if ( !(_la==T__140 || _la==T__141) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(827);
				match(T__30);
				setState(828);
				table_name();
				setState(829);
				match(T__30);
				}
				break;
			case T__142:
				enterOuterAlt(_localctx, 2);
				{
				setState(831);
				match(T__142);
				setState(832);
				match(T__56);
				setState(833);
				match(T__143);
				setState(834);
				match(T__30);
				setState(835);
				match(T__144);
				setState(836);
				match(T__56);
				setState(837);
				table_name();
				setState(838);
				match(T__30);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SqlServerColumnDescContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public SqlServerColumnDescContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlServerColumnDesc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSqlServerColumnDesc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSqlServerColumnDesc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSqlServerColumnDesc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlServerColumnDescContext sqlServerColumnDesc() throws RecognitionException {
		SqlServerColumnDescContext _localctx = new SqlServerColumnDescContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_sqlServerColumnDesc);
		int _la;
		try {
			setState(862);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__81:
			case T__82:
			case T__145:
			case T__146:
			case T__147:
				enterOuterAlt(_localctx, 1);
				{
				setState(842);
				_la = _input.LA(1);
				if ( !(_la==T__81 || _la==T__82 || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & 7L) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(843);
				match(T__30);
				setState(847);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case NAME:
				case SE:
					{
					setState(844);
					column_name();
					}
					break;
				case T__82:
					{
					setState(845);
					match(T__82);
					}
					break;
				case T__81:
					{
					setState(846);
					match(T__81);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(849);
				match(T__10);
				}
				break;
			case T__148:
				enterOuterAlt(_localctx, 2);
				{
				setState(850);
				match(T__148);
				setState(851);
				match(T__56);
				setState(852);
				match(T__149);
				setState(853);
				match(T__30);
				setState(854);
				match(T__150);
				setState(855);
				match(T__56);
				setState(859);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case NAME:
				case SE:
					{
					setState(856);
					column_name();
					}
					break;
				case T__82:
					{
					setState(857);
					match(T__82);
					}
					break;
				case T__81:
					{
					setState(858);
					match(T__81);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(861);
				match(T__10);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SqlServer_commentContext extends ParserRuleContext {
		public List<TerminalNode> SE() { return getTokens(OBCreateParser.SE); }
		public TerminalNode SE(int i) {
			return getToken(OBCreateParser.SE, i);
		}
		public List<TerminalNode> NAME() { return getTokens(OBCreateParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(OBCreateParser.NAME, i);
		}
		public TerminalNode INT() { return getToken(OBCreateParser.INT, 0); }
		public SqlServer_commentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlServer_comment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).enterSqlServer_comment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OBCreateListener ) ((OBCreateListener)listener).exitSqlServer_comment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OBCreateVisitor ) return ((OBCreateVisitor<? extends T>)visitor).visitSqlServer_comment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlServer_commentContext sqlServer_comment() throws RecognitionException {
		SqlServer_commentContext _localctx = new SqlServer_commentContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_sqlServer_comment);
		int _la;
		try {
			setState(874);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SE:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(864);
				match(SE);
				setState(866); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(865);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 1574912L) != 0) || ((((_la - 101)) & ~0x3f) == 0 && ((1L << (_la - 101)) & 27L) != 0) || _la==NAME) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					}
					setState(868); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & 1574912L) != 0) || ((((_la - 101)) & ~0x3f) == 0 && ((1L << (_la - 101)) & 27L) != 0) || _la==NAME );
				setState(870);
				match(SE);
				}
				}
				break;
			case INT:
				enterOuterAlt(_localctx, 2);
				{
				setState(871);
				match(INT);
				}
				break;
			case T__81:
				enterOuterAlt(_localctx, 3);
				{
				setState(872);
				match(T__81);
				}
				break;
			case T__82:
				enterOuterAlt(_localctx, 4);
				{
				setState(873);
				match(T__82);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\u0004\u0001\u00d1\u036d\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001"+
		"\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004"+
		"\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007"+
		"\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b"+
		"\u0002\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007"+
		"\u000f\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007"+
		"\u0012\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007"+
		"\u0015\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007"+
		"\u0018\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007"+
		"\u001b\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007"+
		"\u001e\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0002\"\u0007"+
		"\"\u0002#\u0007#\u0002$\u0007$\u0002%\u0007%\u0002&\u0007&\u0002\'\u0007"+
		"\'\u0002(\u0007(\u0002)\u0007)\u0002*\u0007*\u0002+\u0007+\u0002,\u0007"+
		",\u0002-\u0007-\u0002.\u0007.\u0002/\u0007/\u00020\u00070\u00021\u0007"+
		"1\u00022\u00072\u00023\u00073\u00024\u00074\u00025\u00075\u00026\u0007"+
		"6\u00027\u00077\u00028\u00078\u00029\u00079\u0002:\u0007:\u0002;\u0007"+
		";\u0002<\u0007<\u0002=\u0007=\u0002>\u0007>\u0002?\u0007?\u0002@\u0007"+
		"@\u0002A\u0007A\u0002B\u0007B\u0002C\u0007C\u0002D\u0007D\u0002E\u0007"+
		"E\u0002F\u0007F\u0002G\u0007G\u0002H\u0007H\u0002I\u0007I\u0002J\u0007"+
		"J\u0002K\u0007K\u0002L\u0007L\u0001\u0000\u0001\u0000\u0001\u0000\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0004\u0000\u00a1\b\u0000\u000b\u0000\f"+
		"\u0000\u00a2\u0001\u0001\u0001\u0001\u0003\u0001\u00a7\b\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0003\u0001\u00ad\b\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0003\u0001\u0003\u0003\u0003\u00b8\b\u0003\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0003\u0003\u0003\u00be\b\u0003\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0003\u0003\u0003\u00c4\b\u0003\u0001\u0003\u0003\u0003"+
		"\u00c7\b\u0003\u0001\u0003\u0001\u0003\u0003\u0003\u00cb\b\u0003\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0003\u0003\u00d1\b\u0003\u0001"+
		"\u0003\u0005\u0003\u00d4\b\u0003\n\u0003\f\u0003\u00d7\t\u0003\u0001\u0003"+
		"\u0003\u0003\u00da\b\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0003\u0003\u0003\u00e2\b\u0003\u0001\u0003\u0001\u0003"+
		"\u0003\u0003\u00e6\b\u0003\u0001\u0003\u0004\u0003\u00e9\b\u0003\u000b"+
		"\u0003\f\u0003\u00ea\u0001\u0003\u0001\u0003\u0003\u0003\u00ef\b\u0003"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0003\u0004"+
		"\u010e\b\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0003\u0005\u0113\b"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0003\u0005\u0141\b\u0005\u0001\u0006\u0001"+
		"\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0007\u0001"+
		"\u0007\u0001\u0007\u0005\u0007\u014c\b\u0007\n\u0007\f\u0007\u014f\t\u0007"+
		"\u0001\b\u0001\b\u0001\b\u0003\b\u0154\b\b\u0001\b\u0001\b\u0001\b\u0001"+
		"\b\u0001\b\u0003\b\u015b\b\b\u0001\b\u0001\b\u0001\b\u0001\b\u0003\b\u0161"+
		"\b\b\u0001\t\u0001\t\u0001\t\u0005\t\u0166\b\t\n\t\f\t\u0169\t\t\u0001"+
		"\n\u0001\n\u0001\n\u0005\n\u016e\b\n\n\n\f\n\u0171\t\n\u0001\n\u0003\n"+
		"\u0174\b\n\u0001\n\u0003\n\u0177\b\n\u0001\n\u0003\n\u017a\b\n\u0001\n"+
		"\u0001\n\u0001\n\u0003\n\u017f\b\n\u0001\n\u0001\n\u0001\n\u0003\n\u0184"+
		"\b\n\u0001\n\u0003\n\u0187\b\n\u0001\n\u0001\n\u0003\n\u018b\b\n\u0001"+
		"\n\u0003\n\u018e\b\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0003\n\u0195"+
		"\b\n\u0001\n\u0003\n\u0198\b\n\u0003\n\u019a\b\n\u0001\u000b\u0004\u000b"+
		"\u019d\b\u000b\u000b\u000b\f\u000b\u019e\u0001\f\u0001\f\u0001\f\u0001"+
		"\f\u0001\f\u0001\f\u0001\f\u0003\f\u01a8\b\f\u0001\f\u0001\f\u0001\f\u0001"+
		"\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001"+
		"\f\u0001\f\u0003\f\u01b8\b\f\u0001\r\u0001\r\u0001\r\u0005\r\u01bd\b\r"+
		"\n\r\f\r\u01c0\t\r\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001"+
		"\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001"+
		"\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001"+
		"\u000e\u0001\u000e\u0001\u000e\u0003\u000e\u01d5\b\u000e\u0001\u000f\u0001"+
		"\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001"+
		"\u000f\u0003\u000f\u01df\b\u000f\u0001\u0010\u0001\u0010\u0001\u0010\u0001"+
		"\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001"+
		"\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001"+
		"\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0003\u0010\u01f4\b\u0010\u0001"+
		"\u0011\u0001\u0011\u0001\u0012\u0001\u0012\u0001\u0013\u0003\u0013\u01fb"+
		"\b\u0013\u0001\u0013\u0001\u0013\u0001\u0014\u0001\u0014\u0001\u0014\u0003"+
		"\u0014\u0202\b\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0016\u0001"+
		"\u0016\u0001\u0016\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0018\u0003"+
		"\u0018\u020e\b\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0003\u0018\u0213"+
		"\b\u0018\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0003"+
		"\u0019\u021a\b\u0019\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0001"+
		"\u001b\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001c\u0001\u001c\u0001"+
		"\u001c\u0001\u001c\u0001\u001d\u0003\u001d\u0229\b\u001d\u0001\u001d\u0001"+
		"\u001d\u0001\u001d\u0001\u001d\u0001\u001e\u0003\u001e\u0230\b\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001f\u0001\u001f\u0003"+
		"\u001f\u0238\b\u001f\u0001\u001f\u0001\u001f\u0001 \u0003 \u023d\b \u0001"+
		" \u0004 \u0240\b \u000b \f \u0241\u0001 \u0003 \u0245\b \u0001 \u0003"+
		" \u0248\b \u0001!\u0001!\u0001\"\u0003\"\u024d\b\"\u0001\"\u0001\"\u0003"+
		"\"\u0251\b\"\u0001#\u0001#\u0001$\u0001$\u0001%\u0001%\u0001&\u0001&\u0001"+
		"\'\u0003\'\u025c\b\'\u0001\'\u0001\'\u0003\'\u0260\b\'\u0001(\u0003(\u0263"+
		"\b(\u0001(\u0001(\u0003(\u0267\b(\u0001)\u0001)\u0001*\u0003*\u026c\b"+
		"*\u0001*\u0001*\u0003*\u0270\b*\u0001+\u0001+\u0001,\u0001,\u0001-\u0001"+
		"-\u0001.\u0003.\u0279\b.\u0001.\u0001.\u0003.\u027d\b.\u0001/\u0001/\u0001"+
		"0\u00010\u00011\u00011\u00012\u00012\u00013\u00013\u00014\u00014\u0001"+
		"5\u00015\u00015\u00035\u028e\b5\u00015\u00015\u00016\u00016\u00016\u0003"+
		"6\u0295\b6\u00016\u00016\u00017\u00017\u00017\u00017\u00037\u029d\b7\u0001"+
		"8\u00018\u00018\u00018\u00018\u00038\u02a4\b8\u00019\u00019\u00019\u0005"+
		"9\u02a9\b9\n9\f9\u02ac\t9\u0001:\u0001:\u0001:\u0001:\u0001:\u0005:\u02b3"+
		"\b:\n:\f:\u02b6\t:\u0001:\u0001:\u0001;\u0001;\u0003;\u02bc\b;\u0001;"+
		"\u0001;\u0001<\u0001<\u0003<\u02c2\b<\u0001<\u0001<\u0005<\u02c6\b<\n"+
		"<\f<\u02c9\t<\u0001<\u0001<\u0001=\u0001=\u0003=\u02cf\b=\u0001=\u0001"+
		"=\u0001>\u0001>\u0003>\u02d5\b>\u0001?\u0001?\u0001?\u0001?\u0001?\u0001"+
		"@\u0001@\u0003@\u02de\b@\u0001@\u0001@\u0001A\u0001A\u0001A\u0001A\u0005"+
		"A\u02e6\bA\nA\fA\u02e9\tA\u0001A\u0001A\u0003A\u02ed\bA\u0001A\u0003A"+
		"\u02f0\bA\u0001B\u0001B\u0001B\u0001B\u0003B\u02f6\bB\u0001C\u0003C\u02f9"+
		"\bC\u0001C\u0001C\u0003C\u02fd\bC\u0001D\u0003D\u0300\bD\u0001D\u0001"+
		"D\u0003D\u0304\bD\u0001E\u0003E\u0307\bE\u0001E\u0001E\u0003E\u030b\b"+
		"E\u0001F\u0003F\u030e\bF\u0001F\u0001F\u0003F\u0312\bF\u0001G\u0001G\u0001"+
		"G\u0001G\u0001G\u0001G\u0001G\u0001H\u0001H\u0001H\u0001H\u0001H\u0001"+
		"H\u0001H\u0001H\u0001H\u0001H\u0001H\u0001H\u0001H\u0001H\u0003H\u0329"+
		"\bH\u0001I\u0001I\u0001I\u0001I\u0001I\u0001I\u0001I\u0001I\u0001I\u0001"+
		"I\u0001I\u0001I\u0001I\u0001I\u0003I\u0339\bI\u0001J\u0001J\u0001J\u0001"+
		"J\u0001J\u0001J\u0001J\u0001J\u0001J\u0001J\u0001J\u0001J\u0001J\u0001"+
		"J\u0003J\u0349\bJ\u0001K\u0001K\u0001K\u0001K\u0001K\u0003K\u0350\bK\u0001"+
		"K\u0001K\u0001K\u0001K\u0001K\u0001K\u0001K\u0001K\u0001K\u0001K\u0003"+
		"K\u035c\bK\u0001K\u0003K\u035f\bK\u0001L\u0001L\u0004L\u0363\bL\u000b"+
		"L\fL\u0364\u0001L\u0001L\u0001L\u0001L\u0003L\u036b\bL\u0001L\u0000\u0000"+
		"M\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018\u001a"+
		"\u001c\u001e \"$&(*,.02468:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082"+
		"\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u0000"+
		"1\u0001\u0000\u0001\u0002\u0001\u0000\u0003\u0004\u0001\u0000\u0005\u0006"+
		"\u0001\u0000\u0007\b\u0001\u0000\t\n\u0001\u0000\f\r\u0001\u0000\u000e"+
		"\u000f\u0001\u0000\u0015\u0016\u0001\u0000\u0019\u001a\u0001\u0000\u001b"+
		"\u001c\u0001\u0000\u001d\u001e\u0001\u0000!\"\u0001\u0000#$\u0001\u0000"+
		"%&\u0001\u0000\'(\u0001\u0000)*\u0001\u0000+,\u0001\u0000-0\u0001\u0000"+
		"12\u0001\u0000:;\u0001\u0000<=\u0001\u0000BC\u0001\u0000DE\u0001\u0000"+
		"FG\u0001\u0000IK\u0001\u0000PQ\u0001\u0000RS\u0001\u0000TU\u0001\u0000"+
		"VW\u0001\u0000XY\u0001\u0000Z[\u0001\u000034\u0001\u0000\\]\u0001\u0000"+
		"^_\u0001\u0000`a\u0001\u0000bc\b\u0000\u000b\u000b\u0010\u0010\u0012\u0014"+
		"\u001f\u001fHHRSdo\u00cc\u00cd\u0001\u0000\u009f\u00cb\u0003\u0000ffj"+
		"jor\u0001\u0000st\u0001\u0000uv\u0001\u0000wx\u0001\u0000yz\u0001\u0000"+
		"{|\u0001\u0000\u0081\u0082\u0001\u0000\u0088\u0089\u0001\u0000\u008d\u008e"+
		"\u0002\u0000RS\u0092\u0094\u0005\u0000\u000b\u000b\u0013\u0014efhi\u00cd"+
		"\u00cd\u03ab\u0000\u00a0\u0001\u0000\u0000\u0000\u0002\u00a4\u0001\u0000"+
		"\u0000\u0000\u0004\u00b1\u0001\u0000\u0000\u0000\u0006\u00ee\u0001\u0000"+
		"\u0000\u0000\b\u010d\u0001\u0000\u0000\u0000\n\u0140\u0001\u0000\u0000"+
		"\u0000\f\u0142\u0001\u0000\u0000\u0000\u000e\u0148\u0001\u0000\u0000\u0000"+
		"\u0010\u0160\u0001\u0000\u0000\u0000\u0012\u0162\u0001\u0000\u0000\u0000"+
		"\u0014\u0199\u0001\u0000\u0000\u0000\u0016\u019c\u0001\u0000\u0000\u0000"+
		"\u0018\u01b7\u0001\u0000\u0000\u0000\u001a\u01b9\u0001\u0000\u0000\u0000"+
		"\u001c\u01d4\u0001\u0000\u0000\u0000\u001e\u01de\u0001\u0000\u0000\u0000"+
		" \u01f3\u0001\u0000\u0000\u0000\"\u01f5\u0001\u0000\u0000\u0000$\u01f7"+
		"\u0001\u0000\u0000\u0000&\u01fa\u0001\u0000\u0000\u0000(\u01fe\u0001\u0000"+
		"\u0000\u0000*\u0203\u0001\u0000\u0000\u0000,\u0206\u0001\u0000\u0000\u0000"+
		".\u0209\u0001\u0000\u0000\u00000\u020d\u0001\u0000\u0000\u00002\u0219"+
		"\u0001\u0000\u0000\u00004\u021b\u0001\u0000\u0000\u00006\u021f\u0001\u0000"+
		"\u0000\u00008\u0223\u0001\u0000\u0000\u0000:\u0228\u0001\u0000\u0000\u0000"+
		"<\u022f\u0001\u0000\u0000\u0000>\u0235\u0001\u0000\u0000\u0000@\u0247"+
		"\u0001\u0000\u0000\u0000B\u0249\u0001\u0000\u0000\u0000D\u024c\u0001\u0000"+
		"\u0000\u0000F\u0252\u0001\u0000\u0000\u0000H\u0254\u0001\u0000\u0000\u0000"+
		"J\u0256\u0001\u0000\u0000\u0000L\u0258\u0001\u0000\u0000\u0000N\u025b"+
		"\u0001\u0000\u0000\u0000P\u0262\u0001\u0000\u0000\u0000R\u0268\u0001\u0000"+
		"\u0000\u0000T\u026b\u0001\u0000\u0000\u0000V\u0271\u0001\u0000\u0000\u0000"+
		"X\u0273\u0001\u0000\u0000\u0000Z\u0275\u0001\u0000\u0000\u0000\\\u0278"+
		"\u0001\u0000\u0000\u0000^\u027e\u0001\u0000\u0000\u0000`\u0280\u0001\u0000"+
		"\u0000\u0000b\u0282\u0001\u0000\u0000\u0000d\u0284\u0001\u0000\u0000\u0000"+
		"f\u0286\u0001\u0000\u0000\u0000h\u0288\u0001\u0000\u0000\u0000j\u028d"+
		"\u0001\u0000\u0000\u0000l\u0294\u0001\u0000\u0000\u0000n\u029c\u0001\u0000"+
		"\u0000\u0000p\u02a3\u0001\u0000\u0000\u0000r\u02a5\u0001\u0000\u0000\u0000"+
		"t\u02ad\u0001\u0000\u0000\u0000v\u02b9\u0001\u0000\u0000\u0000x\u02bf"+
		"\u0001\u0000\u0000\u0000z\u02cc\u0001\u0000\u0000\u0000|\u02d4\u0001\u0000"+
		"\u0000\u0000~\u02d6\u0001\u0000\u0000\u0000\u0080\u02db\u0001\u0000\u0000"+
		"\u0000\u0082\u02e1\u0001\u0000\u0000\u0000\u0084\u02f5\u0001\u0000\u0000"+
		"\u0000\u0086\u02f8\u0001\u0000\u0000\u0000\u0088\u02ff\u0001\u0000\u0000"+
		"\u0000\u008a\u0306\u0001\u0000\u0000\u0000\u008c\u030d\u0001\u0000\u0000"+
		"\u0000\u008e\u0313\u0001\u0000\u0000\u0000\u0090\u0328\u0001\u0000\u0000"+
		"\u0000\u0092\u0338\u0001\u0000\u0000\u0000\u0094\u0348\u0001\u0000\u0000"+
		"\u0000\u0096\u035e\u0001\u0000\u0000\u0000\u0098\u036a\u0001\u0000\u0000"+
		"\u0000\u009a\u00a1\u0003\u0002\u0001\u0000\u009b\u00a1\u0003\u0004\u0002"+
		"\u0000\u009c\u00a1\u0003\u0006\u0003\u0000\u009d\u00a1\u0003\b\u0004\u0000"+
		"\u009e\u00a1\u0003\n\u0005\u0000\u009f\u00a1\u0003\f\u0006\u0000\u00a0"+
		"\u009a\u0001\u0000\u0000\u0000\u00a0\u009b\u0001\u0000\u0000\u0000\u00a0"+
		"\u009c\u0001\u0000\u0000\u0000\u00a0\u009d\u0001\u0000\u0000\u0000\u00a0"+
		"\u009e\u0001\u0000\u0000\u0000\u00a0\u009f\u0001\u0000\u0000\u0000\u00a1"+
		"\u00a2\u0001\u0000\u0000\u0000\u00a2\u00a0\u0001\u0000\u0000\u0000\u00a2"+
		"\u00a3\u0001\u0000\u0000\u0000\u00a3\u0001\u0001\u0000\u0000\u0000\u00a4"+
		"\u00a6\u0007\u0000\u0000\u0000\u00a5\u00a7\u0005\u0099\u0000\u0000\u00a6"+
		"\u00a5\u0001\u0000\u0000\u0000\u00a6\u00a7\u0001\u0000\u0000\u0000\u00a7"+
		"\u00a8\u0001\u0000\u0000\u0000\u00a8\u00ac\u0007\u0001\u0000\u0000\u00a9"+
		"\u00aa\u0007\u0002\u0000\u0000\u00aa\u00ab\u0007\u0003\u0000\u0000\u00ab"+
		"\u00ad\u0007\u0004\u0000\u0000\u00ac\u00a9\u0001\u0000\u0000\u0000\u00ac"+
		"\u00ad\u0001\u0000\u0000\u0000\u00ad\u00ae\u0001\u0000\u0000\u0000\u00ae"+
		"\u00af\u0003N\'\u0000\u00af\u00b0\u0005\u000b\u0000\u0000\u00b0\u0003"+
		"\u0001\u0000\u0000\u0000\u00b1\u00b2\u0007\u0005\u0000\u0000\u00b2\u00b3"+
		"\u0003N\'\u0000\u00b3\u00b4\u0005\u000b\u0000\u0000\u00b4\u0005\u0001"+
		"\u0000\u0000\u0000\u00b5\u00b7\u0007\u0000\u0000\u0000\u00b6\u00b8\u0005"+
		"\u0099\u0000\u0000\u00b7\u00b6\u0001\u0000\u0000\u0000\u00b7\u00b8\u0001"+
		"\u0000\u0000\u0000\u00b8\u00b9\u0001\u0000\u0000\u0000\u00b9\u00bd\u0007"+
		"\u0006\u0000\u0000\u00ba\u00bb\u0007\u0002\u0000\u0000\u00bb\u00bc\u0007"+
		"\u0003\u0000\u0000\u00bc\u00be\u0007\u0004\u0000\u0000\u00bd\u00ba\u0001"+
		"\u0000\u0000\u0000\u00bd\u00be\u0001\u0000\u0000\u0000\u00be\u00c3\u0001"+
		"\u0000\u0000\u0000\u00bf\u00c0\u0005\u0010\u0000\u0000\u00c0\u00c1\u0003"+
		"d2\u0000\u00c1\u00c2\u0005\u0011\u0000\u0000\u00c2\u00c4\u0001\u0000\u0000"+
		"\u0000\u00c3\u00bf\u0001\u0000\u0000\u0000\u00c3\u00c4\u0001\u0000\u0000"+
		"\u0000\u00c4\u00c6\u0001\u0000\u0000\u0000\u00c5\u00c7\u0005\u0010\u0000"+
		"\u0000\u00c6\u00c5\u0001\u0000\u0000\u0000\u00c6\u00c7\u0001\u0000\u0000"+
		"\u0000\u00c7\u00c8\u0001\u0000\u0000\u0000\u00c8\u00ca\u0003P(\u0000\u00c9"+
		"\u00cb\u0005\u0012\u0000\u0000\u00ca\u00c9\u0001\u0000\u0000\u0000\u00ca"+
		"\u00cb\u0001\u0000\u0000\u0000\u00cb\u00cc\u0001\u0000\u0000\u0000\u00cc"+
		"\u00cd\u0005\u0013\u0000\u0000\u00cd\u00ce\u0003\u000e\u0007\u0000\u00ce"+
		"\u00d0\u0005\u0014\u0000\u0000\u00cf\u00d1\u0003\u0016\u000b\u0000\u00d0"+
		"\u00cf\u0001\u0000\u0000\u0000\u00d0\u00d1\u0001\u0000\u0000\u0000\u00d1"+
		"\u00d5\u0001\u0000\u0000\u0000\u00d2\u00d4\u00032\u0019\u0000\u00d3\u00d2"+
		"\u0001\u0000\u0000\u0000\u00d4\u00d7\u0001\u0000\u0000\u0000\u00d5\u00d3"+
		"\u0001\u0000\u0000\u0000\u00d5\u00d6\u0001\u0000\u0000\u0000\u00d6\u00d9"+
		"\u0001\u0000\u0000\u0000\u00d7\u00d5\u0001\u0000\u0000\u0000\u00d8\u00da"+
		"\u0003>\u001f\u0000\u00d9\u00d8\u0001\u0000\u0000\u0000\u00d9\u00da\u0001"+
		"\u0000\u0000\u0000\u00da\u00db\u0001\u0000\u0000\u0000\u00db\u00dc\u0005"+
		"\u000b\u0000\u0000\u00dc\u00ef\u0001\u0000\u0000\u0000\u00dd\u00e1\u0007"+
		"\u0007\u0000\u0000\u00de\u00e2\u0005\u0017\u0000\u0000\u00df\u00e2\u0005"+
		"\u0018\u0000\u0000\u00e0\u00e2\u0007\u0006\u0000\u0000\u00e1\u00de\u0001"+
		"\u0000\u0000\u0000\u00e1\u00df\u0001\u0000\u0000\u0000\u00e1\u00e0\u0001"+
		"\u0000\u0000\u0000\u00e2\u00e5\u0001\u0000\u0000\u0000\u00e3\u00e4\u0007"+
		"\u0002\u0000\u0000\u00e4\u00e6\u0007\b\u0000\u0000\u00e5\u00e3\u0001\u0000"+
		"\u0000\u0000\u00e5\u00e6\u0001\u0000\u0000\u0000\u00e6\u00e8\u0001\u0000"+
		"\u0000\u0000\u00e7\u00e9\u0003P(\u0000\u00e8\u00e7\u0001\u0000\u0000\u0000"+
		"\u00e9\u00ea\u0001\u0000\u0000\u0000\u00ea\u00e8\u0001\u0000\u0000\u0000"+
		"\u00ea\u00eb\u0001\u0000\u0000\u0000\u00eb\u00ec\u0001\u0000\u0000\u0000"+
		"\u00ec\u00ed\u0005\u000b\u0000\u0000\u00ed\u00ef\u0001\u0000\u0000\u0000"+
		"\u00ee\u00b5\u0001\u0000\u0000\u0000\u00ee\u00dd\u0001\u0000\u0000\u0000"+
		"\u00ef\u0007\u0001\u0000\u0000\u0000\u00f0\u00f1\u0007\t\u0000\u0000\u00f1"+
		"\u00f2\u0007\u0006\u0000\u0000\u00f2\u00f3\u0003P(\u0000\u00f3\u00f4\u0007"+
		"\n\u0000\u0000\u00f4\u00f5\u0003\u001e\u000f\u0000\u00f5\u00f6\u0005\u0013"+
		"\u0000\u0000\u00f6\u00f7\u0003\u001a\r\u0000\u00f7\u00f8\u0005\u0014\u0000"+
		"\u0000\u00f8\u00f9\u0005\u000b\u0000\u0000\u00f9\u010e\u0001\u0000\u0000"+
		"\u0000\u00fa\u00fb\u0007\t\u0000\u0000\u00fb\u00fc\u0007\u0006\u0000\u0000"+
		"\u00fc\u00fd\u0003P(\u0000\u00fd\u00fe\u0007\u0007\u0000\u0000\u00fe\u00ff"+
		"\u0003\u001e\u000f\u0000\u00ff\u0100\u0005\u001f\u0000\u0000\u0100\u0101"+
		"\u0007\n\u0000\u0000\u0101\u0102\u0003\u001e\u000f\u0000\u0102\u0103\u0005"+
		"\u0013\u0000\u0000\u0103\u0104\u0003\u001a\r\u0000\u0104\u0105\u0005\u0014"+
		"\u0000\u0000\u0105\u0106\u0005\u000b\u0000\u0000\u0106\u010e\u0001\u0000"+
		"\u0000\u0000\u0107\u0108\u0007\t\u0000\u0000\u0108\u0109\u0007\u0006\u0000"+
		"\u0000\u0109\u010a\u0003P(\u0000\u010a\u010b\u0007\u0007\u0000\u0000\u010b"+
		"\u010c\u0003\u001e\u000f\u0000\u010c\u010e\u0001\u0000\u0000\u0000\u010d"+
		"\u00f0\u0001\u0000\u0000\u0000\u010d\u00fa\u0001\u0000\u0000\u0000\u010d"+
		"\u0107\u0001\u0000\u0000\u0000\u010e\t\u0001\u0000\u0000\u0000\u010f\u0112"+
		"\u0007\u0000\u0000\u0000\u0110\u0113\u0005 \u0000\u0000\u0111\u0113\u0007"+
		"\u000b\u0000\u0000\u0112\u0110\u0001\u0000\u0000\u0000\u0112\u0111\u0001"+
		"\u0000\u0000\u0000\u0113\u0114\u0001\u0000\u0000\u0000\u0114\u0115\u0003"+
		"\\.\u0000\u0115\u0116\u0007\f\u0000\u0000\u0116\u0117\u0003P(\u0000\u0117"+
		"\u0118\u0005\u0013\u0000\u0000\u0118\u0119\u0003\u001a\r\u0000\u0119\u011a"+
		"\u0005\u0014\u0000\u0000\u011a\u011b\u0005\u000b\u0000\u0000\u011b\u0141"+
		"\u0001\u0000\u0000\u0000\u011c\u011d\u0007\t\u0000\u0000\u011d\u011e\u0007"+
		"\u0006\u0000\u0000\u011e\u011f\u0003P(\u0000\u011f\u0120\u0007\n\u0000"+
		"\u0000\u0120\u0121\u0007\u000b\u0000\u0000\u0121\u0122\u0003\\.\u0000"+
		"\u0122\u0123\u0005\u0013\u0000\u0000\u0123\u0124\u0003\u001a\r\u0000\u0124"+
		"\u0125\u0005\u0014\u0000\u0000\u0125\u0126\u0005\u000b\u0000\u0000\u0126"+
		"\u0141\u0001\u0000\u0000\u0000\u0127\u0128\u0007\u0007\u0000\u0000\u0128"+
		"\u0129\u0007\u000b\u0000\u0000\u0129\u012a\u0003\\.\u0000\u012a\u012b"+
		"\u0007\f\u0000\u0000\u012b\u012c\u0003P(\u0000\u012c\u012d\u0005\u000b"+
		"\u0000\u0000\u012d\u0141\u0001\u0000\u0000\u0000\u012e\u012f\u0007\t\u0000"+
		"\u0000\u012f\u0130\u0007\u0006\u0000\u0000\u0130\u0131\u0003P(\u0000\u0131"+
		"\u0132\u0007\u0007\u0000\u0000\u0132\u0133\u0007\u000b\u0000\u0000\u0133"+
		"\u0134\u0003\\.\u0000\u0134\u0135\u0005\u000b\u0000\u0000\u0135\u0141"+
		"\u0001\u0000\u0000\u0000\u0136\u0137\u0007\t\u0000\u0000\u0137\u0138\u0007"+
		"\u0006\u0000\u0000\u0138\u0139\u0003P(\u0000\u0139\u013a\u0007\r\u0000"+
		"\u0000\u013a\u013b\u0007\u000b\u0000\u0000\u013b\u013c\u0003\\.\u0000"+
		"\u013c\u013d\u0007\u000e\u0000\u0000\u013d\u013e\u0003\\.\u0000\u013e"+
		"\u013f\u0005\u000b\u0000\u0000\u013f\u0141\u0001\u0000\u0000\u0000\u0140"+
		"\u010f\u0001\u0000\u0000\u0000\u0140\u011c\u0001\u0000\u0000\u0000\u0140"+
		"\u0127\u0001\u0000\u0000\u0000\u0140\u012e\u0001\u0000\u0000\u0000\u0140"+
		"\u0136\u0001\u0000\u0000\u0000\u0141\u000b\u0001\u0000\u0000\u0000\u0142"+
		"\u0143\u0007\t\u0000\u0000\u0143\u0144\u0007\u000b\u0000\u0000\u0144\u0145"+
		"\u0003\\.\u0000\u0145\u0146\u0003\u008cF\u0000\u0146\u0147\u0005\u000b"+
		"\u0000\u0000\u0147\r\u0001\u0000\u0000\u0000\u0148\u014d\u0003\u0010\b"+
		"\u0000\u0149\u014a\u0005\u001f\u0000\u0000\u014a\u014c\u0003\u0010\b\u0000"+
		"\u014b\u0149\u0001\u0000\u0000\u0000\u014c\u014f\u0001\u0000\u0000\u0000"+
		"\u014d\u014b\u0001\u0000\u0000\u0000\u014d\u014e\u0001\u0000\u0000\u0000"+
		"\u014e\u000f\u0001\u0000\u0000\u0000\u014f\u014d\u0001\u0000\u0000\u0000"+
		"\u0150\u0161\u0003\u0014\n\u0000\u0151\u0152\u0007\u000f\u0000\u0000\u0152"+
		"\u0154\u0003b1\u0000\u0153\u0151\u0001\u0000\u0000\u0000\u0153\u0154\u0001"+
		"\u0000\u0000\u0000\u0154\u0155\u0001\u0000\u0000\u0000\u0155\u0156\u0003"+
		"\u001e\u000f\u0000\u0156\u0157\u0003\u0082A\u0000\u0157\u0161\u0001\u0000"+
		"\u0000\u0000\u0158\u0159\u0007\u000f\u0000\u0000\u0159\u015b\u0003b1\u0000"+
		"\u015a\u0158\u0001\u0000\u0000\u0000\u015a\u015b\u0001\u0000\u0000\u0000"+
		"\u015b\u015c\u0001\u0000\u0000\u0000\u015c\u015d\u0003\u001e\u000f\u0000"+
		"\u015d\u015e\u0003\\.\u0000\u015e\u015f\u0003\u0082A\u0000\u015f\u0161"+
		"\u0001\u0000\u0000\u0000\u0160\u0150\u0001\u0000\u0000\u0000\u0160\u0153"+
		"\u0001\u0000\u0000\u0000\u0160\u015a\u0001\u0000\u0000\u0000\u0161\u0011"+
		"\u0001\u0000\u0000\u0000\u0162\u0167\u0003\u0014\n\u0000\u0163\u0164\u0005"+
		"\u001f\u0000\u0000\u0164\u0166\u0003\u0014\n\u0000\u0165\u0163\u0001\u0000"+
		"\u0000\u0000\u0166\u0169\u0001\u0000\u0000\u0000\u0167\u0165\u0001\u0000"+
		"\u0000\u0000\u0167\u0168\u0001\u0000\u0000\u0000\u0168\u0013\u0001\u0000"+
		"\u0000\u0000\u0169\u0167\u0001\u0000\u0000\u0000\u016a\u016b\u0003T*\u0000"+
		"\u016b\u016f\u0003B!\u0000\u016c\u016e\u0003\u001e\u000f\u0000\u016d\u016c"+
		"\u0001\u0000\u0000\u0000\u016e\u0171\u0001\u0000\u0000\u0000\u016f\u016d"+
		"\u0001\u0000\u0000\u0000\u016f\u0170\u0001\u0000\u0000\u0000\u0170\u0173"+
		"\u0001\u0000\u0000\u0000\u0171\u016f\u0001\u0000\u0000\u0000\u0172\u0174"+
		"\u0003>\u001f\u0000\u0173\u0172\u0001\u0000\u0000\u0000\u0173\u0174\u0001"+
		"\u0000\u0000\u0000\u0174\u0176\u0001\u0000\u0000\u0000\u0175\u0177\u0003"+
		".\u0017\u0000\u0176\u0175\u0001\u0000\u0000\u0000\u0176\u0177\u0001\u0000"+
		"\u0000\u0000\u0177\u0179\u0001\u0000\u0000\u0000\u0178\u017a\u0003,\u0016"+
		"\u0000\u0179\u0178\u0001\u0000\u0000\u0000\u0179\u017a\u0001\u0000\u0000"+
		"\u0000\u017a\u019a\u0001\u0000\u0000\u0000\u017b\u017c\u0003T*\u0000\u017c"+
		"\u017e\u0003B!\u0000\u017d\u017f\u0005\u0098\u0000\u0000\u017e\u017d\u0001"+
		"\u0000\u0000\u0000\u017e\u017f\u0001\u0000\u0000\u0000\u017f\u0180\u0001"+
		"\u0000\u0000\u0000\u0180\u0181\u0007\u0010\u0000\u0000\u0181\u0183\u0003"+
		"n7\u0000\u0182\u0184\u0007\u0011\u0000\u0000\u0183\u0182\u0001\u0000\u0000"+
		"\u0000\u0183\u0184\u0001\u0000\u0000\u0000\u0184\u0186\u0001\u0000\u0000"+
		"\u0000\u0185\u0187\u0003(\u0014\u0000\u0186\u0185\u0001\u0000\u0000\u0000"+
		"\u0186\u0187\u0001\u0000\u0000\u0000\u0187\u019a\u0001\u0000\u0000\u0000"+
		"\u0188\u018a\u0003T*\u0000\u0189\u018b\u0003B!\u0000\u018a\u0189\u0001"+
		"\u0000\u0000\u0000\u018a\u018b\u0001\u0000\u0000\u0000\u018b\u018d\u0001"+
		"\u0000\u0000\u0000\u018c\u018e\u0005\u0098\u0000\u0000\u018d\u018c\u0001"+
		"\u0000\u0000\u0000\u018d\u018e\u0001\u0000\u0000\u0000\u018e\u018f\u0001"+
		"\u0000\u0000\u0000\u018f\u0190\u0007\u0010\u0000\u0000\u0190\u0191\u0005"+
		"\u0013\u0000\u0000\u0191\u0192\u0003n7\u0000\u0192\u0194\u0005\u0014\u0000"+
		"\u0000\u0193\u0195\u0007\u0011\u0000\u0000\u0194\u0193\u0001\u0000\u0000"+
		"\u0000\u0194\u0195\u0001\u0000\u0000\u0000\u0195\u0197\u0001\u0000\u0000"+
		"\u0000\u0196\u0198\u0003(\u0014\u0000\u0197\u0196\u0001\u0000\u0000\u0000"+
		"\u0197\u0198\u0001\u0000\u0000\u0000\u0198\u019a\u0001\u0000\u0000\u0000"+
		"\u0199\u016a\u0001\u0000\u0000\u0000\u0199\u017b\u0001\u0000\u0000\u0000"+
		"\u0199\u0188\u0001\u0000\u0000\u0000\u019a\u0015\u0001\u0000\u0000\u0000"+
		"\u019b\u019d\u0003\u0018\f\u0000\u019c\u019b\u0001\u0000\u0000\u0000\u019d"+
		"\u019e\u0001\u0000\u0000\u0000\u019e\u019c\u0001\u0000\u0000\u0000\u019e"+
		"\u019f\u0001\u0000\u0000\u0000\u019f\u0017\u0001\u0000\u0000\u0000\u01a0"+
		"\u01a7\u0007\u0012\u0000\u0000\u01a1\u01a8\u00053\u0000\u0000\u01a2\u01a8"+
		"\u00054\u0000\u0000\u01a3\u01a4\u00055\u0000\u0000\u01a4\u01a8\u00056"+
		"\u0000\u0000\u01a5\u01a6\u00057\u0000\u0000\u01a6\u01a8\u00058\u0000\u0000"+
		"\u01a7\u01a1\u0001\u0000\u0000\u0000\u01a7\u01a2\u0001\u0000\u0000\u0000"+
		"\u01a7\u01a3\u0001\u0000\u0000\u0000\u01a7\u01a5\u0001\u0000\u0000\u0000"+
		"\u01a8\u01a9\u0001\u0000\u0000\u0000\u01a9\u01aa\u00059\u0000\u0000\u01aa"+
		"\u01b8\u0003\u0086C\u0000\u01ab\u01ac\u0007\u0012\u0000\u0000\u01ac\u01ad"+
		"\u0007\u0013\u0000\u0000\u01ad\u01ae\u00059\u0000\u0000\u01ae\u01b8\u0003"+
		"\u008aE\u0000\u01af\u01b8\u0003\u0080@\u0000\u01b0\u01b8\u0003>\u001f"+
		"\u0000\u01b1\u01b2\u0007\u0014\u0000\u0000\u01b2\u01b3\u00059\u0000\u0000"+
		"\u01b3\u01b8\u0005>\u0000\u0000\u01b4\u01b8\u0005?\u0000\u0000\u01b5\u01b8"+
		"\u0005@\u0000\u0000\u01b6\u01b8\u0005A\u0000\u0000\u01b7\u01a0\u0001\u0000"+
		"\u0000\u0000\u01b7\u01ab\u0001\u0000\u0000\u0000\u01b7\u01af\u0001\u0000"+
		"\u0000\u0000\u01b7\u01b0\u0001\u0000\u0000\u0000\u01b7\u01b1\u0001\u0000"+
		"\u0000\u0000\u01b7\u01b4\u0001\u0000\u0000\u0000\u01b7\u01b5\u0001\u0000"+
		"\u0000\u0000\u01b7\u01b6\u0001\u0000\u0000\u0000\u01b8\u0019\u0001\u0000"+
		"\u0000\u0000\u01b9\u01be\u0003T*\u0000\u01ba\u01bb\u0005\u001f\u0000\u0000"+
		"\u01bb\u01bd\u0003T*\u0000\u01bc\u01ba\u0001\u0000\u0000\u0000\u01bd\u01c0"+
		"\u0001\u0000\u0000\u0000\u01be\u01bc\u0001\u0000\u0000\u0000\u01be\u01bf"+
		"\u0001\u0000\u0000\u0000\u01bf\u001b\u0001\u0000\u0000\u0000\u01c0\u01be"+
		"\u0001\u0000\u0000\u0000\u01c1\u01c2\u0007\u0015\u0000\u0000\u01c2\u01c3"+
		"\u0007\f\u0000\u0000\u01c3\u01c4\u0007\u0006\u0000\u0000\u01c4\u01c5\u0003"+
		"P(\u0000\u01c5\u01c6\u0007\u0016\u0000\u0000\u01c6\u01c7\u0003@ \u0000"+
		"\u01c7\u01c8\u0005\u000b\u0000\u0000\u01c8\u01d5\u0001\u0000\u0000\u0000"+
		"\u01c9\u01ca\u0007\u0015\u0000\u0000\u01ca\u01cb\u0007\f\u0000\u0000\u01cb"+
		"\u01cc\u0007\u0017\u0000\u0000\u01cc\u01cd\u0003P(\u0000\u01cd\u01ce\u0005"+
		"H\u0000\u0000\u01ce\u01cf\u0003T*\u0000\u01cf\u01d0\u0007\u0016\u0000"+
		"\u0000\u01d0\u01d1\u0003@ \u0000\u01d1\u01d2\u0005\u000b\u0000\u0000\u01d2"+
		"\u01d5\u0001\u0000\u0000\u0000\u01d3\u01d5\u0003\u008eG\u0000\u01d4\u01c1"+
		"\u0001\u0000\u0000\u0000\u01d4\u01c9\u0001\u0000\u0000\u0000\u01d4\u01d3"+
		"\u0001\u0000\u0000\u0000\u01d5\u001d\u0001\u0000\u0000\u0000\u01d6\u01df"+
		"\u0005\u009a\u0000\u0000\u01d7\u01df\u0003 \u0010\u0000\u01d8\u01df\u0003"+
		"&\u0013\u0000\u01d9\u01df\u0003(\u0014\u0000\u01da\u01df\u0003*\u0015"+
		"\u0000\u01db\u01df\u0003,\u0016\u0000\u01dc\u01df\u0003.\u0017\u0000\u01dd"+
		"\u01df\u00030\u0018\u0000\u01de\u01d6\u0001\u0000\u0000\u0000\u01de\u01d7"+
		"\u0001\u0000\u0000\u0000\u01de\u01d8\u0001\u0000\u0000\u0000\u01de\u01d9"+
		"\u0001\u0000\u0000\u0000\u01de\u01da\u0001\u0000\u0000\u0000\u01de\u01db"+
		"\u0001\u0000\u0000\u0000\u01de\u01dc\u0001\u0000\u0000\u0000\u01de\u01dd"+
		"\u0001\u0000\u0000\u0000\u01df\u001f\u0001\u0000\u0000\u0000\u01e0\u01f4"+
		"\u0007\u0018\u0000\u0000\u01e1\u01f4\u0005L\u0000\u0000\u01e2\u01f4\u0005"+
		"M\u0000\u0000\u01e3\u01e4\u0005N\u0000\u0000\u01e4\u01e5\u0005\u00cc\u0000"+
		"\u0000\u01e5\u01f4\u0005\u0014\u0000\u0000\u01e6\u01e7\u0005O\u0000\u0000"+
		"\u01e7\u01e8\u0005\u00cc\u0000\u0000\u01e8\u01f4\u0005\u0014\u0000\u0000"+
		"\u01e9\u01ea\u0005N\u0000\u0000\u01ea\u01eb\u0005\u00cc\u0000\u0000\u01eb"+
		"\u01ec\u0005\u001f\u0000\u0000\u01ec\u01ed\u0005\u00cc\u0000\u0000\u01ed"+
		"\u01f4\u0005\u0014\u0000\u0000\u01ee\u01ef\u0005O\u0000\u0000\u01ef\u01f0"+
		"\u0005\u00cc\u0000\u0000\u01f0\u01f1\u0005\u001f\u0000\u0000\u01f1\u01f2"+
		"\u0005\u00cc\u0000\u0000\u01f2\u01f4\u0005\u0014\u0000\u0000\u01f3\u01e0"+
		"\u0001\u0000\u0000\u0000\u01f3\u01e1\u0001\u0000\u0000\u0000\u01f3\u01e2"+
		"\u0001\u0000\u0000\u0000\u01f3\u01e3\u0001\u0000\u0000\u0000\u01f3\u01e6"+
		"\u0001\u0000\u0000\u0000\u01f3\u01e9\u0001\u0000\u0000\u0000\u01f3\u01ee"+
		"\u0001\u0000\u0000\u0000\u01f4!\u0001\u0000\u0000\u0000\u01f5\u01f6\u0007"+
		"\u0019\u0000\u0000\u01f6#\u0001\u0000\u0000\u0000\u01f7\u01f8\u0007\u001a"+
		"\u0000\u0000\u01f8%\u0001\u0000\u0000\u0000\u01f9\u01fb\u0007\u001b\u0000"+
		"\u0000\u01fa\u01f9\u0001\u0000\u0000\u0000\u01fa\u01fb\u0001\u0000\u0000"+
		"\u0000\u01fb\u01fc\u0001\u0000\u0000\u0000\u01fc\u01fd\u0007\u001c\u0000"+
		"\u0000\u01fd\'\u0001\u0000\u0000\u0000\u01fe\u0201\u0007\u001d\u0000\u0000"+
		"\u01ff\u0202\u0007\u001c\u0000\u0000\u0200\u0202\u0007\u000b\u0000\u0000"+
		"\u0201\u01ff\u0001\u0000\u0000\u0000\u0201\u0200\u0001\u0000\u0000\u0000"+
		"\u0201\u0202\u0001\u0000\u0000\u0000\u0202)\u0001\u0000\u0000\u0000\u0203"+
		"\u0204\u0007\u001e\u0000\u0000\u0204\u0205\u0003\u0088D\u0000\u0205+\u0001"+
		"\u0000\u0000\u0000\u0206\u0207\u0007\u0013\u0000\u0000\u0207\u0208\u0003"+
		"\u008aE\u0000\u0208-\u0001\u0000\u0000\u0000\u0209\u020a\u0007\u001f\u0000"+
		"\u0000\u020a\u020b\u0003\u0086C\u0000\u020b/\u0001\u0000\u0000\u0000\u020c"+
		"\u020e\u0007\u0012\u0000\u0000\u020d\u020c\u0001\u0000\u0000\u0000\u020d"+
		"\u020e\u0001\u0000\u0000\u0000\u020e\u0212\u0001\u0000\u0000\u0000\u020f"+
		"\u0213\u0003@ \u0000\u0210\u0213\u0003$\u0012\u0000\u0211\u0213\u0003"+
		"\"\u0011\u0000\u0212\u020f\u0001\u0000\u0000\u0000\u0212\u0210\u0001\u0000"+
		"\u0000\u0000\u0212\u0211\u0001\u0000\u0000\u0000\u02131\u0001\u0000\u0000"+
		"\u0000\u0214\u021a\u00034\u001a\u0000\u0215\u021a\u0003:\u001d\u0000\u0216"+
		"\u021a\u0003<\u001e\u0000\u0217\u021a\u00038\u001c\u0000\u0218\u021a\u0003"+
		"6\u001b\u0000\u0219\u0214\u0001\u0000\u0000\u0000\u0219\u0215\u0001\u0000"+
		"\u0000\u0000\u0219\u0216\u0001\u0000\u0000\u0000\u0219\u0217\u0001\u0000"+
		"\u0000\u0000\u0219\u0218\u0001\u0000\u0000\u0000\u021a3\u0001\u0000\u0000"+
		"\u0000\u021b\u021c\u0007 \u0000\u0000\u021c\u021d\u00059\u0000\u0000\u021d"+
		"\u021e\u0005\u00cd\u0000\u0000\u021e5\u0001\u0000\u0000\u0000\u021f\u0220"+
		"\u0007!\u0000\u0000\u0220\u0221\u00059\u0000\u0000\u0221\u0222\u0005\u00cd"+
		"\u0000\u0000\u02227\u0001\u0000\u0000\u0000\u0223\u0224\u0003 \u0010\u0000"+
		"\u0224\u0225\u00059\u0000\u0000\u0225\u0226\u0005\u00cc\u0000\u0000\u0226"+
		"9\u0001\u0000\u0000\u0000\u0227\u0229\u0007\u0012\u0000\u0000\u0228\u0227"+
		"\u0001\u0000\u0000\u0000\u0228\u0229\u0001\u0000\u0000\u0000\u0229\u022a"+
		"\u0001\u0000\u0000\u0000\u022a\u022b\u0007\"\u0000\u0000\u022b\u022c\u0005"+
		"9\u0000\u0000\u022c\u022d\u0005\u00cd\u0000\u0000\u022d;\u0001\u0000\u0000"+
		"\u0000\u022e\u0230\u0007\u0012\u0000\u0000\u022f\u022e\u0001\u0000\u0000"+
		"\u0000\u022f\u0230\u0001\u0000\u0000\u0000\u0230\u0231\u0001\u0000\u0000"+
		"\u0000\u0231\u0232\u0007#\u0000\u0000\u0232\u0233\u00059\u0000\u0000\u0233"+
		"\u0234\u0005\u00cd\u0000\u0000\u0234=\u0001\u0000\u0000\u0000\u0235\u0237"+
		"\u0007\u0015\u0000\u0000\u0236\u0238\u00059\u0000\u0000\u0237\u0236\u0001"+
		"\u0000\u0000\u0000\u0237\u0238\u0001\u0000\u0000\u0000\u0238\u0239\u0001"+
		"\u0000\u0000\u0000\u0239\u023a\u0003@ \u0000\u023a?\u0001\u0000\u0000"+
		"\u0000\u023b\u023d\u0005\u00ce\u0000\u0000\u023c\u023b\u0001\u0000\u0000"+
		"\u0000\u023c\u023d\u0001\u0000\u0000\u0000\u023d\u023f\u0001\u0000\u0000"+
		"\u0000\u023e\u0240\u0007$\u0000\u0000\u023f\u023e\u0001\u0000\u0000\u0000"+
		"\u0240\u0241\u0001\u0000\u0000\u0000\u0241\u023f\u0001\u0000\u0000\u0000"+
		"\u0241\u0242\u0001\u0000\u0000\u0000\u0242\u0244\u0001\u0000\u0000\u0000"+
		"\u0243\u0245\u0005\u00ce\u0000\u0000\u0244\u0243\u0001\u0000\u0000\u0000"+
		"\u0244\u0245\u0001\u0000\u0000\u0000\u0245\u0248\u0001\u0000\u0000\u0000"+
		"\u0246\u0248\u0005\u00cc\u0000\u0000\u0247\u023c\u0001\u0000\u0000\u0000"+
		"\u0247\u0246\u0001\u0000\u0000\u0000\u0248A\u0001\u0000\u0000\u0000\u0249"+
		"\u024a\u0007%\u0000\u0000\u024aC\u0001\u0000\u0000\u0000\u024b\u024d\u0005"+
		"\u00ce\u0000\u0000\u024c\u024b\u0001\u0000\u0000\u0000\u024c\u024d\u0001"+
		"\u0000\u0000\u0000\u024d\u024e\u0001\u0000\u0000\u0000\u024e\u0250\u0005"+
		"\u00cd\u0000\u0000\u024f\u0251\u0005\u00ce\u0000\u0000\u0250\u024f\u0001"+
		"\u0000\u0000\u0000\u0250\u0251\u0001\u0000\u0000\u0000\u0251E\u0001\u0000"+
		"\u0000\u0000\u0252\u0253\u0005\u00cd\u0000\u0000\u0253G\u0001\u0000\u0000"+
		"\u0000\u0254\u0255\u0005\u00cd\u0000\u0000\u0255I\u0001\u0000\u0000\u0000"+
		"\u0256\u0257\u0005\u00cd\u0000\u0000\u0257K\u0001\u0000\u0000\u0000\u0258"+
		"\u0259\u0005\u00cd\u0000\u0000\u0259M\u0001\u0000\u0000\u0000\u025a\u025c"+
		"\u0005\u00ce\u0000\u0000\u025b\u025a\u0001\u0000\u0000\u0000\u025b\u025c"+
		"\u0001\u0000\u0000\u0000\u025c\u025d\u0001\u0000\u0000\u0000\u025d\u025f"+
		"\u0005\u00cd\u0000\u0000\u025e\u0260\u0005\u00ce\u0000\u0000\u025f\u025e"+
		"\u0001\u0000\u0000\u0000\u025f\u0260\u0001\u0000\u0000\u0000\u0260O\u0001"+
		"\u0000\u0000\u0000\u0261\u0263\u0005\u00ce\u0000\u0000\u0262\u0261\u0001"+
		"\u0000\u0000\u0000\u0262\u0263\u0001\u0000\u0000\u0000\u0263\u0264\u0001"+
		"\u0000\u0000\u0000\u0264\u0266\u0005\u00cd\u0000\u0000\u0265\u0267\u0005"+
		"\u00ce\u0000\u0000\u0266\u0265\u0001\u0000\u0000\u0000\u0266\u0267\u0001"+
		"\u0000\u0000\u0000\u0267Q\u0001\u0000\u0000\u0000\u0268\u0269\u0005\u00cd"+
		"\u0000\u0000\u0269S\u0001\u0000\u0000\u0000\u026a\u026c\u0005\u00ce\u0000"+
		"\u0000\u026b\u026a\u0001\u0000\u0000\u0000\u026b\u026c\u0001\u0000\u0000"+
		"\u0000\u026c\u026d\u0001\u0000\u0000\u0000\u026d\u026f\u0005\u00cd\u0000"+
		"\u0000\u026e\u0270\u0005\u00ce\u0000\u0000\u026f\u026e\u0001\u0000\u0000"+
		"\u0000\u026f\u0270\u0001\u0000\u0000\u0000\u0270U\u0001\u0000\u0000\u0000"+
		"\u0271\u0272\u0005\u00cd\u0000\u0000\u0272W\u0001\u0000\u0000\u0000\u0273"+
		"\u0274\u0005\u00cd\u0000\u0000\u0274Y\u0001\u0000\u0000\u0000\u0275\u0276"+
		"\u0005\u00cd\u0000\u0000\u0276[\u0001\u0000\u0000\u0000\u0277\u0279\u0005"+
		"\u00ce\u0000\u0000\u0278\u0277\u0001\u0000\u0000\u0000\u0278\u0279\u0001"+
		"\u0000\u0000\u0000\u0279\u027a\u0001\u0000\u0000\u0000\u027a\u027c\u0005"+
		"\u00cd\u0000\u0000\u027b\u027d\u0005\u00ce\u0000\u0000\u027c\u027b\u0001"+
		"\u0000\u0000\u0000\u027c\u027d\u0001\u0000\u0000\u0000\u027d]\u0001\u0000"+
		"\u0000\u0000\u027e\u027f\u0005\u00cd\u0000\u0000\u027f_\u0001\u0000\u0000"+
		"\u0000\u0280\u0281\u0005\u00cd\u0000\u0000\u0281a\u0001\u0000\u0000\u0000"+
		"\u0282\u0283\u0005\u00cd\u0000\u0000\u0283c\u0001\u0000\u0000\u0000\u0284"+
		"\u0285\u0005\u00cd\u0000\u0000\u0285e\u0001\u0000\u0000\u0000\u0286\u0287"+
		"\u0005\u00cd\u0000\u0000\u0287g\u0001\u0000\u0000\u0000\u0288\u0289\u0005"+
		"\u00cd\u0000\u0000\u0289i\u0001\u0000\u0000\u0000\u028a\u028b\u0003N\'"+
		"\u0000\u028b\u028c\u0005H\u0000\u0000\u028c\u028e\u0001\u0000\u0000\u0000"+
		"\u028d\u028a\u0001\u0000\u0000\u0000\u028d\u028e\u0001\u0000\u0000\u0000"+
		"\u028e\u028f\u0001\u0000\u0000\u0000\u028f\u0290\u0003P(\u0000\u0290k"+
		"\u0001\u0000\u0000\u0000\u0291\u0292\u0003j5\u0000\u0292\u0293\u0005H"+
		"\u0000\u0000\u0293\u0295\u0001\u0000\u0000\u0000\u0294\u0291\u0001\u0000"+
		"\u0000\u0000\u0294\u0295\u0001\u0000\u0000\u0000\u0295\u0296\u0001\u0000"+
		"\u0000\u0000\u0296\u0297\u0003T*\u0000\u0297m\u0001\u0000\u0000\u0000"+
		"\u0298\u029d\u0003p8\u0000\u0299\u029d\u0003l6\u0000\u029a\u029d\u0003"+
		"r9\u0000\u029b\u029d\u0003t:\u0000\u029c\u0298\u0001\u0000\u0000\u0000"+
		"\u029c\u0299\u0001\u0000\u0000\u0000\u029c\u029a\u0001\u0000\u0000\u0000"+
		"\u029c\u029b\u0001\u0000\u0000\u0000\u029do\u0001\u0000\u0000\u0000\u029e"+
		"\u02a4\u0005\u00cc\u0000\u0000\u029f\u02a0\u0005\u00cc\u0000\u0000\u02a0"+
		"\u02a1\u0005H\u0000\u0000\u02a1\u02a4\u0005\u00cc\u0000\u0000\u02a2\u02a4"+
		"\u0005\u00cd\u0000\u0000\u02a3\u029e\u0001\u0000\u0000\u0000\u02a3\u029f"+
		"\u0001\u0000\u0000\u0000\u02a3\u02a2\u0001\u0000\u0000\u0000\u02a4q\u0001"+
		"\u0000\u0000\u0000\u02a5\u02aa\u0005\u00cc\u0000\u0000\u02a6\u02a7\u0007"+
		"&\u0000\u0000\u02a7\u02a9\u0005\u00cc\u0000\u0000\u02a8\u02a6\u0001\u0000"+
		"\u0000\u0000\u02a9\u02ac\u0001\u0000\u0000\u0000\u02aa\u02a8\u0001\u0000"+
		"\u0000\u0000\u02aa\u02ab\u0001\u0000\u0000\u0000\u02abs\u0001\u0000\u0000"+
		"\u0000\u02ac\u02aa\u0001\u0000\u0000\u0000\u02ad\u02ae\u0005\u00cd\u0000"+
		"\u0000\u02ae\u02af\u0005\u0013\u0000\u0000\u02af\u02b4\u0005\u00cd\u0000"+
		"\u0000\u02b0\u02b1\u0005\u001f\u0000\u0000\u02b1\u02b3\u0005\u00cd\u0000"+
		"\u0000\u02b2\u02b0\u0001\u0000\u0000\u0000\u02b3\u02b6\u0001\u0000\u0000"+
		"\u0000\u02b4\u02b2\u0001\u0000\u0000\u0000\u02b4\u02b5\u0001\u0000\u0000"+
		"\u0000\u02b5\u02b7\u0001\u0000\u0000\u0000\u02b6\u02b4\u0001\u0000\u0000"+
		"\u0000\u02b7\u02b8\u0005\u0014\u0000\u0000\u02b8u\u0001\u0000\u0000\u0000"+
		"\u02b9\u02bb\u0007\'\u0000\u0000\u02ba\u02bc\u00059\u0000\u0000\u02bb"+
		"\u02ba\u0001\u0000\u0000\u0000\u02bb\u02bc\u0001\u0000\u0000\u0000\u02bc"+
		"\u02bd\u0001\u0000\u0000\u0000\u02bd\u02be\u0003J%\u0000\u02bew\u0001"+
		"\u0000\u0000\u0000\u02bf\u02c1\u0007(\u0000\u0000\u02c0\u02c2\u00059\u0000"+
		"\u0000\u02c1\u02c0\u0001\u0000\u0000\u0000\u02c1\u02c2\u0001\u0000\u0000"+
		"\u0000\u02c2\u02c3\u0001\u0000\u0000\u0000\u02c3\u02c7\u0005\u0013\u0000"+
		"\u0000\u02c4\u02c6\u0003J%\u0000\u02c5\u02c4\u0001\u0000\u0000\u0000\u02c6"+
		"\u02c9\u0001\u0000\u0000\u0000\u02c7\u02c5\u0001\u0000\u0000\u0000\u02c7"+
		"\u02c8\u0001\u0000\u0000\u0000\u02c8\u02ca\u0001\u0000\u0000\u0000\u02c9"+
		"\u02c7\u0001\u0000\u0000\u0000\u02ca\u02cb\u0005\u0014\u0000\u0000\u02cb"+
		"y\u0001\u0000\u0000\u0000\u02cc\u02ce\u0007)\u0000\u0000\u02cd\u02cf\u0005"+
		"9\u0000\u0000\u02ce\u02cd\u0001\u0000\u0000\u0000\u02ce\u02cf\u0001\u0000"+
		"\u0000\u0000\u02cf\u02d0\u0001\u0000\u0000\u0000\u02d0\u02d1\u0005\u00cc"+
		"\u0000\u0000\u02d1{\u0001\u0000\u0000\u0000\u02d2\u02d5\u0003~?\u0000"+
		"\u02d3\u02d5\u0003\u0080@\u0000\u02d4\u02d2\u0001\u0000\u0000\u0000\u02d4"+
		"\u02d3\u0001\u0000\u0000\u0000\u02d5}\u0001\u0000\u0000\u0000\u02d6\u02d7"+
		"\u0007*\u0000\u0000\u02d7\u02d8\u0007+\u0000\u0000\u02d8\u02d9\u00059"+
		"\u0000\u0000\u02d9\u02da\u0003d2\u0000\u02da\u007f\u0001\u0000\u0000\u0000"+
		"\u02db\u02dd\u0007+\u0000\u0000\u02dc\u02de\u00059\u0000\u0000\u02dd\u02dc"+
		"\u0001\u0000\u0000\u0000\u02dd\u02de\u0001\u0000\u0000\u0000\u02de\u02df"+
		"\u0001\u0000\u0000\u0000\u02df\u02e0\u0003d2\u0000\u02e0\u0081\u0001\u0000"+
		"\u0000\u0000\u02e1\u02e2\u0005\u0013\u0000\u0000\u02e2\u02e7\u0003T*\u0000"+
		"\u02e3\u02e4\u0005\u001f\u0000\u0000\u02e4\u02e6\u0003T*\u0000\u02e5\u02e3"+
		"\u0001\u0000\u0000\u0000\u02e6\u02e9\u0001\u0000\u0000\u0000\u02e7\u02e5"+
		"\u0001\u0000\u0000\u0000\u02e7\u02e8\u0001\u0000\u0000\u0000\u02e8\u02ea"+
		"\u0001\u0000\u0000\u0000\u02e9\u02e7\u0001\u0000\u0000\u0000\u02ea\u02ec"+
		"\u0005\u0014\u0000\u0000\u02eb\u02ed\u0003\u0084B\u0000\u02ec\u02eb\u0001"+
		"\u0000\u0000\u0000\u02ec\u02ed\u0001\u0000\u0000\u0000\u02ed\u02ef\u0001"+
		"\u0000\u0000\u0000\u02ee\u02f0\u0005\u001f\u0000\u0000\u02ef\u02ee\u0001"+
		"\u0000\u0000\u0000\u02ef\u02f0\u0001\u0000\u0000\u0000\u02f0\u0083\u0001"+
		"\u0000\u0000\u0000\u02f1\u02f2\u0005}\u0000\u0000\u02f2\u02f6\u0005~\u0000"+
		"\u0000\u02f3\u02f4\u0005\u007f\u0000\u0000\u02f4\u02f6\u0005\u0080\u0000"+
		"\u0000\u02f5\u02f1\u0001\u0000\u0000\u0000\u02f5\u02f3\u0001\u0000\u0000"+
		"\u0000\u02f6\u0085\u0001\u0000\u0000\u0000\u02f7\u02f9\u0005\u00ce\u0000"+
		"\u0000\u02f8\u02f7\u0001\u0000\u0000\u0000\u02f8\u02f9\u0001\u0000\u0000"+
		"\u0000\u02f9\u02fa\u0001\u0000\u0000\u0000\u02fa\u02fc\u0005\u00cd\u0000"+
		"\u0000\u02fb\u02fd\u0005\u00ce\u0000\u0000\u02fc\u02fb\u0001\u0000\u0000"+
		"\u0000\u02fc\u02fd\u0001\u0000\u0000\u0000\u02fd\u0087\u0001\u0000\u0000"+
		"\u0000\u02fe\u0300\u0005\u00ce\u0000\u0000\u02ff\u02fe\u0001\u0000\u0000"+
		"\u0000\u02ff\u0300\u0001\u0000\u0000\u0000\u0300\u0301\u0001\u0000\u0000"+
		"\u0000\u0301\u0303\u0005\u00cd\u0000\u0000\u0302\u0304\u0005\u00ce\u0000"+
		"\u0000\u0303\u0302\u0001\u0000\u0000\u0000\u0303\u0304\u0001\u0000\u0000"+
		"\u0000\u0304\u0089\u0001\u0000\u0000\u0000\u0305\u0307\u0005\u00ce\u0000"+
		"\u0000\u0306\u0305\u0001\u0000\u0000\u0000\u0306\u0307\u0001\u0000\u0000"+
		"\u0000\u0307\u0308\u0001\u0000\u0000\u0000\u0308\u030a\u0005\u00cd\u0000"+
		"\u0000\u0309\u030b\u0005\u00ce\u0000\u0000\u030a\u0309\u0001\u0000\u0000"+
		"\u0000\u030a\u030b\u0001\u0000\u0000\u0000\u030b\u008b\u0001\u0000\u0000"+
		"\u0000\u030c\u030e\u0005\u00ce\u0000\u0000\u030d\u030c\u0001\u0000\u0000"+
		"\u0000\u030d\u030e\u0001\u0000\u0000\u0000\u030e\u030f\u0001\u0000\u0000"+
		"\u0000\u030f\u0311\u0005\u00cd\u0000\u0000\u0310\u0312\u0005\u00ce\u0000"+
		"\u0000\u0311\u0310\u0001\u0000\u0000\u0000\u0311\u0312\u0001\u0000\u0000"+
		"\u0000\u0312\u008d\u0001\u0000\u0000\u0000\u0313\u0314\u0007,\u0000\u0000"+
		"\u0314\u0315\u0005\u0083\u0000\u0000\u0315\u0316\u0003\u0090H\u0000\u0316"+
		"\u0317\u0003\u0092I\u0000\u0317\u0318\u0003\u0094J\u0000\u0318\u0319\u0003"+
		"\u0096K\u0000\u0319\u008f\u0001\u0000\u0000\u0000\u031a\u031b\u0005\u0084"+
		"\u0000\u0000\u031b\u031c\u0005\u001f\u0000\u0000\u031c\u031d\u0003\u0098"+
		"L\u0000\u031d\u031e\u0005\u001f\u0000\u0000\u031e\u0329\u0001\u0000\u0000"+
		"\u0000\u031f\u0320\u0005\u0085\u0000\u0000\u0320\u0321\u00059\u0000\u0000"+
		"\u0321\u0322\u0005\u0086\u0000\u0000\u0322\u0323\u0005\u001f\u0000\u0000"+
		"\u0323\u0324\u0005\u0087\u0000\u0000\u0324\u0325\u00059\u0000\u0000\u0325"+
		"\u0326\u0003\u0098L\u0000\u0326\u0327\u0005\u001f\u0000\u0000\u0327\u0329"+
		"\u0001\u0000\u0000\u0000\u0328\u031a\u0001\u0000\u0000\u0000\u0328\u031f"+
		"\u0001\u0000\u0000\u0000\u0329\u0091\u0001\u0000\u0000\u0000\u032a\u032b"+
		"\u0007-\u0000\u0000\u032b\u032c\u0005\u001f\u0000\u0000\u032c\u032d\u0003"+
		"d2\u0000\u032d\u032e\u0005\u001f\u0000\u0000\u032e\u0339\u0001\u0000\u0000"+
		"\u0000\u032f\u0330\u0005\u008a\u0000\u0000\u0330\u0331\u00059\u0000\u0000"+
		"\u0331\u0332\u0005\u008b\u0000\u0000\u0332\u0333\u0005\u001f\u0000\u0000"+
		"\u0333\u0334\u0005\u008c\u0000\u0000\u0334\u0335\u00059\u0000\u0000\u0335"+
		"\u0336\u0003d2\u0000\u0336\u0337\u0005\u001f\u0000\u0000\u0337\u0339\u0001"+
		"\u0000\u0000\u0000\u0338\u032a\u0001\u0000\u0000\u0000\u0338\u032f\u0001"+
		"\u0000\u0000\u0000\u0339\u0093\u0001\u0000\u0000\u0000\u033a\u033b\u0007"+
		".\u0000\u0000\u033b\u033c\u0005\u001f\u0000\u0000\u033c\u033d\u0003P("+
		"\u0000\u033d\u033e\u0005\u001f\u0000\u0000\u033e\u0349\u0001\u0000\u0000"+
		"\u0000\u033f\u0340\u0005\u008f\u0000\u0000\u0340\u0341\u00059\u0000\u0000"+
		"\u0341\u0342\u0005\u0090\u0000\u0000\u0342\u0343\u0005\u001f\u0000\u0000"+
		"\u0343\u0344\u0005\u0091\u0000\u0000\u0344\u0345\u00059\u0000\u0000\u0345"+
		"\u0346\u0003P(\u0000\u0346\u0347\u0005\u001f\u0000\u0000\u0347\u0349\u0001"+
		"\u0000\u0000\u0000\u0348\u033a\u0001\u0000\u0000\u0000\u0348\u033f\u0001"+
		"\u0000\u0000\u0000\u0349\u0095\u0001\u0000\u0000\u0000\u034a\u034b\u0007"+
		"/\u0000\u0000\u034b\u034f\u0005\u001f\u0000\u0000\u034c\u0350\u0003T*"+
		"\u0000\u034d\u0350\u0005S\u0000\u0000\u034e\u0350\u0005R\u0000\u0000\u034f"+
		"\u034c\u0001\u0000\u0000\u0000\u034f\u034d\u0001\u0000\u0000\u0000\u034f"+
		"\u034e\u0001\u0000\u0000\u0000\u0350\u0351\u0001\u0000\u0000\u0000\u0351"+
		"\u035f\u0005\u000b\u0000\u0000\u0352\u0353\u0005\u0095\u0000\u0000\u0353"+
		"\u0354\u00059\u0000\u0000\u0354\u0355\u0005\u0096\u0000\u0000\u0355\u0356"+
		"\u0005\u001f\u0000\u0000\u0356\u0357\u0005\u0097\u0000\u0000\u0357\u035b"+
		"\u00059\u0000\u0000\u0358\u035c\u0003T*\u0000\u0359\u035c\u0005S\u0000"+
		"\u0000\u035a\u035c\u0005R\u0000\u0000\u035b\u0358\u0001\u0000\u0000\u0000"+
		"\u035b\u0359\u0001\u0000\u0000\u0000\u035b\u035a\u0001\u0000\u0000\u0000"+
		"\u035c\u035d\u0001\u0000\u0000\u0000\u035d\u035f\u0005\u000b\u0000\u0000"+
		"\u035e\u034a\u0001\u0000\u0000\u0000\u035e\u0352\u0001\u0000\u0000\u0000"+
		"\u035f\u0097\u0001\u0000\u0000\u0000\u0360\u0362\u0005\u00ce\u0000\u0000"+
		"\u0361\u0363\u00070\u0000\u0000\u0362\u0361\u0001\u0000\u0000\u0000\u0363"+
		"\u0364\u0001\u0000\u0000\u0000\u0364\u0362\u0001\u0000\u0000\u0000\u0364"+
		"\u0365\u0001\u0000\u0000\u0000\u0365\u0366\u0001\u0000\u0000\u0000\u0366"+
		"\u036b\u0005\u00ce\u0000\u0000\u0367\u036b\u0005\u00cc\u0000\u0000\u0368"+
		"\u036b\u0005R\u0000\u0000\u0369\u036b\u0005S\u0000\u0000\u036a\u0360\u0001"+
		"\u0000\u0000\u0000\u036a\u0367\u0001\u0000\u0000\u0000\u036a\u0368\u0001"+
		"\u0000\u0000\u0000\u036a\u0369\u0001\u0000\u0000\u0000\u036b\u0099\u0001"+
		"\u0000\u0000\u0000a\u00a0\u00a2\u00a6\u00ac\u00b7\u00bd\u00c3\u00c6\u00ca"+
		"\u00d0\u00d5\u00d9\u00e1\u00e5\u00ea\u00ee\u010d\u0112\u0140\u014d\u0153"+
		"\u015a\u0160\u0167\u016f\u0173\u0176\u0179\u017e\u0183\u0186\u018a\u018d"+
		"\u0194\u0197\u0199\u019e\u01a7\u01b7\u01be\u01d4\u01de\u01f3\u01fa\u0201"+
		"\u020d\u0212\u0219\u0228\u022f\u0237\u023c\u0241\u0244\u0247\u024c\u0250"+
		"\u025b\u025f\u0262\u0266\u026b\u026f\u0278\u027c\u028d\u0294\u029c\u02a3"+
		"\u02aa\u02b4\u02bb\u02c1\u02c7\u02ce\u02d4\u02dd\u02e7\u02ec\u02ef\u02f5"+
		"\u02f8\u02fc\u02ff\u0303\u0306\u030a\u030d\u0311\u0328\u0338\u0348\u034f"+
		"\u035b\u035e\u0364\u036a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}