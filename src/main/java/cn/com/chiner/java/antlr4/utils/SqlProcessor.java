package cn.com.chiner.java.antlr4.utils;

import cn.com.chiner.java.antlr4.chain.*;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 模块名 : SqlProcessor
 * 文件名 : SqlProcessor.java
 * 创建时间 : 2023/8/26
 * 实现功能 : antlr4 无法匹配空字符串的问题，针对''类型，需要预处理
 * 作者 : wantyx
 * 版本 : v0.0.1
 *
 * @see
 * @since ----------------------------------------------------------------
 * 修改记录
 * 日 期     版本     修改人  修改内容
 * 2023/8/26   v0.0.1    wantyx   创建
 * ----------------------------------------------------------------
 */
public class SqlProcessor {

    public static Logger logger = LoggerFactory.getLogger(SqlProcessor.class);
    public static final String CREATE_PATTERN = "(?i)^create\\s+table[\\s\\S]*?";

    public static SqlProcess chain;
    static {
        EmptyStringProcess emptyStringProcess = new EmptyStringProcess();
        SchemaIgnoreProcess schemaIgnoreProcess = new SchemaIgnoreProcess();
        DropDDLIgnoreProcess dropDDLIgnoreProcess = new DropDDLIgnoreProcess();
        AnnotationIgnoreProcess annotationIgnoreProcess = new AnnotationIgnoreProcess();
        PropertyIgnoreProcess propertyIgnoreProcess = new PropertyIgnoreProcess();
        chain = emptyStringProcess;
        emptyStringProcess.setNext(schemaIgnoreProcess);
        schemaIgnoreProcess.setNext(dropDDLIgnoreProcess);
        dropDDLIgnoreProcess.setNext(annotationIgnoreProcess);
        annotationIgnoreProcess.setNext(propertyIgnoreProcess);
    }

    public static InputStream processFile(String sqlPath) {
        String sql = FileUtil.readString(sqlPath, StandardCharsets.UTF_8);
        sql = chain.apply(sql);
        //去掉多余的分号
        String[] split = sql.split(";");
        StringBuilder sb = new StringBuilder();
        for (String singleSql : split) {
            if (StrUtil.isEmpty(singleSql) || nothingInSql(singleSql)){
            }else {
                sb.append(singleSql).append(";");
            }
        }
        sql = sb.toString();
        return new ByteArrayInputStream(sql.getBytes());
    }

    public static String extractDDL(String sqlPath){
        Pattern compile = Pattern.compile(CREATE_PATTERN);
        String sql = FileUtil.readString(sqlPath, StandardCharsets.UTF_8);
        Matcher matcher = compile.matcher(sql);
        while (matcher.find()){
            String matchedSql = matcher.group();
            System.out.println("Matched SQL: " + matchedSql);
        }
        return null;
    }

    private static boolean nothingInSql(String sql){
        String s = sql.replaceAll("\n", "");
        s = s.replaceAll(" ","");
        if (StrUtil.isEmpty(s)){
            return true;
        }
        return false;
    }

}
