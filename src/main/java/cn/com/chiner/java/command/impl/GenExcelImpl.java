/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.java.command.impl;

import cn.com.chiner.java.command.Command;
import cn.com.chiner.java.command.ExecResult;
import cn.com.chiner.java.command.impl.excel.ExcelCommonUtils;
import cn.com.chiner.java.command.impl.excel.ExcelDocUtils;
import cn.com.chiner.java.model.Module;
import cn.com.chiner.java.model.*;
import cn.fisok.raw.io.ByteInputStream;
import cn.fisok.raw.kit.FileKit;
import cn.fisok.raw.kit.IOKit;
import cn.fisok.raw.kit.JSONKit;
import cn.fisok.raw.lang.ValueObject;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2024/1/6
 * @desc : 生成EXCEL文件
 */
public class GenExcelImpl implements Command<ExecResult> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public ExecResult exec(Map<String, String> params) {
        String dataFile = params.get("dataFile");
        String outFile = params.get("outFile"); //输出的文档文件
        String out = params.get("out");         //输出的结果json文件

        ExecResult ret = new ExecResult();

        try {
            exec(dataFile, outFile,out);
            ret.setStatus(ExecResult.SUCCESS);
            ret.setBody(outFile);
        } catch (Exception e) {
            String message = e.getMessage();
            if(message == null){
                message = e.toString();
            }
            ret.setStatus(ExecResult.FAILED);
            ret.setBody(message);
            logger.error("", e);
        }
        return ret;
    }

    protected void exec(String dataFile, String outFile,String out) throws IOException {
        File inDataFile = new File(dataFile);

        String jsonText = parseFile(inDataFile);

        Project project = null;
        ProjectOriginal projectOriginal = JSONKit.jsonToBean(jsonText,ProjectOriginal.class);
        Object objectProfile = projectOriginal.getProfile();
        List<Object> objectEntities = projectOriginal.getEntities();
        List<Object> objectViews = projectOriginal.getViews();
        List<Object> objectDicts = projectOriginal.getDicts();
        List<Object> objectDiagrams = projectOriginal.getDiagrams();
        List<Object> viewGroups = projectOriginal.getViewGroups();

        String textEntities = JSONKit.toJsonString(objectEntities);
        String textViews = JSONKit.toJsonString(objectViews);
        String textDicts = JSONKit.toJsonString(objectDicts);
        String textDiagrams = JSONKit.toJsonString(objectDiagrams);
        String textProfile=JSONKit.toJsonString(objectProfile);

        Profile profile=JSONKit.jsonToBean(textProfile,Profile.class);
        List<ColumnHeader> columnHeaders=profile.getHeaders();
        List<TableEntity> entities = JSONKit.jsonToBeanList(textEntities,TableEntity.class);
        List<View> views = JSONKit.jsonToBeanList(textViews,View.class);
        List<Dict> dicts = JSONKit.jsonToBeanList(textDicts,Dict.class);
        List<Diagram> diagrams = JSONKit.jsonToBeanList(textDiagrams,Diagram.class);
        if(entities == null){
            entities = new ArrayList<>();
        }
        if(views == null){
            views = new ArrayList<>();
        }
        if(dicts == null){
            dicts = new ArrayList<>();
        }
        if(diagrams == null){
            diagrams = new ArrayList<>();
        }

        List<Module> modules = new ArrayList<Module>();
        project = JSONKit.jsonToBean(jsonText,Project.class);
        project.setModules(modules);

        if(viewGroups == null || viewGroups.size() == 0){
            //如果没有分组，则构建一个默认分组
            Module module = new Module();
            modules.add(module);
            module.setDefKey("PDManer元数建模");
            module.setDefName(project.getName());


            module.setEntities(entities);
            module.setViews(views);
            module.setDicts(dicts);
            module.setDiagrams(diagrams);

            for(TableEntity entity : entities){
                entity.setDicts(dicts);
                entity.fillFieldsCalcValue();
            }

            module.fillEntitiesRowNo();
            module.fillDictsRowNo();

            fillExcelDataSimple(module,outFile,columnHeaders);
        }else{
            List<TableEntity> findedEntities = new ArrayList<>();
            List<Diagram> findDiagrams = new ArrayList<>();
            List<Dict> findedDicts = new ArrayList<>();

            //处理模块中包含的数据表+数据字典
            for(Object viewGroup : viewGroups){
                Map<String,Object> mapModule = ( Map<String,Object>)viewGroup;
                String moduleDefKey = ValueObject.valueOf(mapModule.get("defKey")).strValue("");
                String moduleDefName = ValueObject.valueOf(mapModule.get("defName")).strValue("");

                Module module = new Module();
                modules.add(module);
                module.setDefKey(moduleDefKey);
                module.setDefName(moduleDefName);
                module.setEntities(new ArrayList<>());
                module.setDicts(new ArrayList<>());
                module.setDiagrams(new ArrayList<>());
                //填充好本模块
                fillModule(module,mapModule,entities,dicts,diagrams,findedEntities,findDiagrams,findedDicts);
            }

            //处理没有包含在模块中的表+关系图+数据字典
            List<TableEntity> remainTableEntities = new ArrayList<>();
            List<Diagram> remainDiagrams = new ArrayList<>();
            List<Dict> remainDicts = new ArrayList<>();
            for(TableEntity entity : entities){
                //没有被找到过，说明不在模块分组里
                if(findedEntities.indexOf(entity) < 0){
                    remainTableEntities.add(entity);
                }
            }
            for(Diagram diagram : diagrams){
                if(findDiagrams.indexOf(diagram) < 0){
                    remainDiagrams.add(diagram);
                }
            }
            for(Dict dict : dicts){
                if(findedDicts.indexOf(dict) < 0){
                    remainDicts.add(dict);
                }
            }
            //其他模块这个分组只有内容不空时，才能加到模型中去
            Module otherModule = new Module();
            otherModule.setDefKey("default");
            otherModule.setDefName("默认主题域");
            otherModule.setEntities(new ArrayList<>());
            otherModule.setDiagrams(new ArrayList<>());
            otherModule.setDicts(new ArrayList<>());
            otherModule.setEntities(remainTableEntities);
            otherModule.setDiagrams(remainDiagrams);
            otherModule.setDicts(remainDicts);
            if(!otherModule.isEmpty()){
                modules.add(otherModule);
            }

            fillExcelDataGroup(modules,outFile,columnHeaders);
        }
    }

    private void fillModule(Module module,
                            Map<String, Object> mapModule,
                            List<TableEntity> entities,
                            List<Dict> dicts,
                            List<Diagram> diagrams,
                            List<TableEntity> findedEntities,
                            List<Diagram> findDiagrams,
                            List<Dict> findedDicts
                            ) {
        List<String> refEntities = (List<String>) mapModule.get("refEntities");
        List<String> refViews = (List<String>) mapModule.get("refViews");
        List<String> refDiagrams = (List<String>) mapModule.get("refDiagrams");
        List<String> refDicts = (List<String>) mapModule.get("refDicts");

        if (refEntities != null && refEntities.size() >= 0) {
            for (String entityDefKey : refEntities) {
                TableEntity entity = lookupTableEntity(entities, entityDefKey);
                if (entity != null) {
//                    entity.fillFieldsCalcValue();
                    findedEntities.add(entity);
                    module.getEntities().add(entity);
                }
            }
        }

        if (refDicts != null && refDicts.size() > 0) {
            for (String dictDefKey : refDicts) {
                Dict dict = lookupDict(dicts, dictDefKey);
                if(dict != null){
                    findedDicts.add(dict);
                    module.getDicts().add(dict);
                }
            }
        }

        for(TableEntity entity : entities){
            entity.setDicts(dicts);
            entity.fillFieldsCalcValue();
        }
        module.fillEntitiesRowNo();
        module.fillDictsRowNo();
    }

    private TableEntity lookupTableEntity(List<TableEntity> entities,String entityDefKey){
        for(TableEntity entity : entities){
            if(entityDefKey.equalsIgnoreCase(entity.getId())){
                return entity;
            }
        }
        return null;
    }

    private Diagram lookupDiagram(List<Diagram> diagrams,String diagramId){
        for(Diagram diagram : diagrams){
            if(diagramId.equalsIgnoreCase(diagram.getId())){
                return diagram;
            }
        }
        return null;
    }


    private Dict lookupDict(List<Dict> dicts,String dictDefKey){
        for(Dict dict : dicts){
            if(dictDefKey.equalsIgnoreCase(dict.getId())){
                return dict;
            }
        }
        return null;
    }

    protected String parseFile(File sinerFile) throws IOException {
        String jsonText = null;
        try(InputStream inputStream = FileKit.openInputStream(sinerFile)) {
            jsonText = IOKit.toString(inputStream, "UTF-8");
//            mapObject = JSONKit.jsonToMap(jsonText);
        } catch (IOException e) {
            throw e;
        }
        return jsonText;
    }


    /**
     * 填充EXCEL数据，使用简单不分组的模式
     * @param module 单个的模块对象
     */
    private void fillExcelDataSimple(Module module,String outFile,List<ColumnHeader> columnHeaders){
        FileOutputStream fos = null;
        InputStream orginIn = getClass().getResourceAsStream("/xlsx/tpl-by-simple.xlsx");
        ByteInputStream byteIn = null;

        try {
            fos = new FileOutputStream(outFile);
            byteIn = IOKit.convertToByteArrayInputStream(orginIn);
            Workbook workbook = ExcelCommonUtils.getWorkbook(byteIn);

            Font defaultFont = workbook.createFont();
            defaultFont.setFontName("宋体-简"); // 设置字体为宋体
            CellStyle defaultCellStyle = workbook.createCellStyle();
            defaultCellStyle.setFont(defaultFont); // 将默认字体应用到样式中

            //填充字典
            Sheet dictSheet = workbook.getSheetAt(1);
            int startRow = 2;
            List<Dict> dicts = module.getDicts();
            if(dicts == null){
                return;
            }
            for (Dict dict : dicts) {
                List<DictItem> items = dict.getItems();
                if (items == null) {
                    continue;
                }
                boolean isFirst = true;
                for (int j=0;j<items.size();j++) {
                    DictItem item = items.get(j);
                    Row row = ExcelCommonUtils.touchRow(dictSheet, startRow++);
                    if (row == null) {
                        row = dictSheet.createRow(startRow);
                    }
                    Cell defKeyCell = ExcelCommonUtils.touchCell(row, 0,defaultCellStyle);
                    Cell defNameCell = ExcelCommonUtils.touchCell(row, 1,defaultCellStyle);
                    Cell itemNoCell = ExcelCommonUtils.touchCell(row, 2,defaultCellStyle);
                    Cell itemKeyCell = ExcelCommonUtils.touchCell(row, 3,defaultCellStyle);
                    Cell itemNameCell = ExcelCommonUtils.touchCell(row, 4,defaultCellStyle);
                    Cell itemSortCell = ExcelCommonUtils.touchCell(row, 5,defaultCellStyle);
                    Cell itemParentKeyCell = ExcelCommonUtils.touchCell(row, 6,defaultCellStyle);
                    Cell itemIntroCell = ExcelCommonUtils.touchCell(row, 7,defaultCellStyle);

                    if(isFirst){
                        defKeyCell.setCellValue(dict.getDefKey());
                        defNameCell.setCellValue(dict.getDefName());
                        isFirst = false;
                    }
                    itemNoCell.setCellValue(j+1);
                    itemKeyCell.setCellValue(item.getDefKey());
                    itemNameCell.setCellValue(item.getDefName());
                    itemSortCell.setCellValue(item.getSort());
                    itemParentKeyCell.setCellValue(item.getParentKey());
                    itemIntroCell.setCellValue(item.getIntro());
                }
            }

            //处理数据表
            //1.取目录sheet页
            Sheet catalogSheet = workbook.getSheetAt(0);
            List<TableEntity> entities = module.getEntities();
            if(entities == null || entities.size()==0){
                return;
            }
            for(int i=0;i<entities.size();i++){
                TableEntity entity = entities.get(i);
                //1.创建目录页面的目录行
                Row catalogRow = ExcelCommonUtils.touchRow(catalogSheet,2+i);

                //2.填充目录内容以及超链接
                Cell numberCell = ExcelCommonUtils.touchCell(catalogRow,0,defaultCellStyle);
                Cell defKeyCell = ExcelCommonUtils.touchCell(catalogRow,1,defaultCellStyle);
                Cell defNameCell = ExcelCommonUtils.touchCell(catalogRow,2,defaultCellStyle);
                Cell commentCell = ExcelCommonUtils.touchCell(catalogRow,3,defaultCellStyle);
                numberCell.setCellValue(i+1);
                defKeyCell.setCellValue(entity.getDefKey());
                defNameCell.setCellValue(entity.getDefName());
                commentCell.setCellValue(entity.getComment());
                String sheetName = ExcelDocUtils.fillEntityCatalogLink(entity.getDefKey(),workbook,defKeyCell);

                //2.创建内容sheet并填充内容
                ExcelDocUtils.fillFieldsIntoSheet(entity,workbook,sheetName,defaultCellStyle,columnHeaders);

            }
            workbook.setActiveSheet(0);
            workbook.removeSheetAt(2);  //删除模板sheet

            workbook.write(fos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOKit.close(byteIn);
            IOKit.close(orginIn);
            IOKit.close(fos);
        }
    }

    /**
     * 填充EXCEL数据，使用简单不分组的模式
     * @param modules 多模块的列表
     */
    private void fillExcelDataGroup(List<Module> modules,String outFile,List<ColumnHeader>columnHeaders){
        FileOutputStream fos = null;
        InputStream orginIn = getClass().getResourceAsStream("/xlsx/tpl-by-group.xlsx");
        ByteInputStream byteIn = null;

        try {
            fos = new FileOutputStream(outFile);
            byteIn = IOKit.convertToByteArrayInputStream(orginIn);
            Workbook workbook = ExcelCommonUtils.getWorkbook(byteIn);

            Font defaultFont = workbook.createFont();
            defaultFont.setFontName("宋体-简"); // 设置字体为宋体
            CellStyle defaultCellStyle = workbook.createCellStyle();
            defaultCellStyle.setFont(defaultFont); // 将默认字体应用到样式中

            // ------
            //填充字典
            // ------
            Sheet dictSheet = workbook.getSheetAt(1);
            int rowCursor = 2;
            for(int i=0;i<modules.size();i++){
                Module module = modules.get(i);
                List<Dict> dicts = module.getDicts();
                // 1.填充主题域名称部分
                Row groupRow = ExcelCommonUtils.touchRow(dictSheet, rowCursor);
                Cell groupCell = ExcelCommonUtils.touchCell(groupRow,0,defaultCellStyle);
                groupCell.setCellValue(module.getDefKey()+"-"+module.getDefName());
                if(dicts == null || dicts.isEmpty()){
                    rowCursor ++;  //如果没有值，也要把空行留出来
                    continue;
                }

                // 2.填充字典定义部分
                for(int j=0;j<dicts.size();j++){
                    Dict dict = dicts.get(j);
                    Row dictRow = ExcelCommonUtils.touchRow(dictSheet, rowCursor);

                    Cell itemKeyCell = ExcelCommonUtils.touchCell(dictRow,1,defaultCellStyle);
                    Cell itemNameCell = ExcelCommonUtils.touchCell(dictRow,2,defaultCellStyle);
                    itemKeyCell.setCellValue(dict.getDefKey());
                    itemNameCell.setCellValue(dict.getDefName());

                    // 3.填充字典项部分
                    List<DictItem> items = dict.getItems();
                    if(items == null || items.isEmpty()){
                        rowCursor ++;  //如果没有值，也要把空行留出来
                        continue;
                    }
                    for(int k=0;k<items.size();k++){
                        if(rowCursor == 32){
                            int x = 3;
                        }
                        DictItem item = items.get(k);
                        Row itemRow = ExcelCommonUtils.touchRow(dictSheet,rowCursor++);

                        ExcelCommonUtils.touchCell(itemRow,3,defaultCellStyle).setCellValue(k+1);
                        ExcelCommonUtils.touchCell(itemRow,4,defaultCellStyle).setCellValue(item.getDefKey());
                        ExcelCommonUtils.touchCell(itemRow,5,defaultCellStyle).setCellValue(item.getDefName());
                        ExcelCommonUtils.touchCell(itemRow,6,defaultCellStyle).setCellValue(item.getSort());
                        ExcelCommonUtils.touchCell(itemRow,7,defaultCellStyle).setCellValue(item.getParentKey());
                        ExcelCommonUtils.touchCell(itemRow,8,defaultCellStyle).setCellValue(item.getIntro());
                    }
                }
            }

            // ------
            //填充数据表
            // ------
            Sheet catalogSheet = workbook.getSheetAt(0);
            int entityRowCursor = 2;
            for(int i=0;i<modules.size();i++){
                Module module = modules.get(i);

                //1.取目录sheet页
                List<TableEntity> entities = module.getEntities();
                if(entities == null || entities.isEmpty()){
                    continue;
                }
                for(int j=0;j<entities.size();j++){
                    TableEntity entity = entities.get(j);
                    //1.创建目录页面的目录行
                    Row catalogRow = ExcelCommonUtils.touchRow(catalogSheet,entityRowCursor++);

                    //2.填充目录内容以及超链接
                    Cell moduleCell = ExcelCommonUtils.touchCell(catalogRow,0,defaultCellStyle);
                    Cell numberCell = ExcelCommonUtils.touchCell(catalogRow,1,defaultCellStyle);
                    Cell defKeyCell = ExcelCommonUtils.touchCell(catalogRow,2,defaultCellStyle);
                    Cell defNameCell = ExcelCommonUtils.touchCell(catalogRow,3,defaultCellStyle);
                    Cell commentCell = ExcelCommonUtils.touchCell(catalogRow,4,defaultCellStyle);
                    if(j==0){
                        moduleCell.setCellValue(module.getDefKey()+"-"+module.getDefName());
                    }
                    numberCell.setCellValue(j+1);
                    defKeyCell.setCellValue(entity.getDefKey());
                    defNameCell.setCellValue(entity.getDefName());
                    commentCell.setCellValue(entity.getComment());
                    String sheetName = ExcelDocUtils.fillEntityCatalogLink(entity.getDefKey(),workbook,defKeyCell);

                    //2.创建内容sheet并填充内容
                    ExcelDocUtils.fillFieldsIntoSheet(entity,workbook,sheetName,defaultCellStyle,columnHeaders);
                }
            }

            workbook.setActiveSheet(0);
            workbook.removeSheetAt(2);  //删除模板sheet

            workbook.write(fos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOKit.close(byteIn);
            IOKit.close(orginIn);
            IOKit.close(fos);
        }
    }
}
