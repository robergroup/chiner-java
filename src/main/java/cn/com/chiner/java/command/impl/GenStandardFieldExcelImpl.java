package cn.com.chiner.java.command.impl;

import cn.com.chiner.java.command.Command;
import cn.com.chiner.java.command.ExecResult;
import cn.com.chiner.java.command.impl.excel.ExcelCommonUtils;
import cn.com.chiner.java.model.ProjectOriginal;
import cn.com.chiner.java.model.StandardField;
import cn.com.chiner.java.model.StandardFieldModule;
import cn.fisok.raw.io.ByteInputStream;
import cn.fisok.raw.kit.FileKit;
import cn.fisok.raw.kit.IOKit;
import cn.fisok.raw.kit.JSONKit;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GenStandardFieldExcelImpl implements Command<ExecResult> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public ExecResult exec(Map<String, String> params) throws IOException {
        String dataFile = params.get("dataFile");//需转成excel的json文件
        String outFile = params.get("outFile"); //输出的文档文件
        ExecResult ret = new ExecResult();

        try {
            parserStandardFieldModuleToExcel(dataFile, outFile);
            ret.setStatus(ExecResult.SUCCESS);
            ret.setBody(outFile);
        } catch (Exception e) {
            String message = e.getMessage();
            if (message == null) {
                message = e.toString();
            }
            ret.setStatus(ExecResult.FAILED);
            ret.setBody(message);
            logger.error("", e);
        }
        return ret;

    }

    private void parserStandardFieldModuleToExcel(String dataFile, String outFile) throws IOException {

        File inDataFile = new File(dataFile);
        //将数据文件转为json
        String jsonText = parseFile(inDataFile);

        ProjectOriginal projectOriginal = JSONKit.jsonToBean(jsonText, ProjectOriginal.class);
        List<Object> objectStandardFields = projectOriginal.getStandardFields();
        String textStandardFields = JSONKit.toJsonString(objectStandardFields);
        //获取标准字段库模板对象集合
        List<StandardFieldModule> standardFieldModules = JSONKit.jsonToBeanList(textStandardFields, StandardFieldModule.class);

        if (standardFieldModules == null) {
            standardFieldModules = new ArrayList<>();
        }
        //填充Excel信息
        fillExcelByFieldModules(standardFieldModules, outFile);

    }

    protected String parseFile(File sinerFile) throws IOException {
        String jsonText = null;
        try (InputStream inputStream = FileKit.openInputStream(sinerFile)) {
            jsonText = IOKit.toString(inputStream, "UTF-8");
        } catch (IOException e) {
            throw e;
        }
        return jsonText;
    }

    /**
     * 填充EXCEL数据
     * @param standardFieldModules
     */
    private void fillExcelByFieldModules(List<StandardFieldModule> standardFieldModules, String outFile) {
        FileOutputStream fos = null;
        InputStream orginIn = getClass().getResourceAsStream("/xlsx/tpl-standard-field.xlsx");
        ByteInputStream byteIn = null;

        try {
            fos = new FileOutputStream(outFile);
            byteIn = IOKit.convertToByteArrayInputStream(orginIn);
            Workbook workbook = ExcelCommonUtils.getWorkbook(byteIn);

            Font defaultFont = workbook.createFont();
            defaultFont.setFontName("宋体-简"); // 设置字体为宋体
            CellStyle defaultCellStyle = workbook.createCellStyle();
            defaultCellStyle.setFont(defaultFont); // 将默认字体应用到样式中
            // ------
            //填充字段
            // ------
            Sheet standardFieldSheet = workbook.getSheetAt(0);
            int startRow = 2;
            for (StandardFieldModule standardFieldModule : standardFieldModules) {
                List<StandardField> standardFields = standardFieldModule.getFields();
                // 1.填充主题域名称部分
                Row groupRow = ExcelCommonUtils.touchRow(standardFieldSheet, startRow);
                Cell groupCell = ExcelCommonUtils.touchCell(groupRow, 0, defaultCellStyle);
                groupCell.setCellValue(standardFieldModule.getDefKey() + "-" + standardFieldModule.getDefName());
                if (standardFields == null || standardFields.isEmpty()) {
                    startRow++;  //如果没有值，也要把空行留出来
                    continue;
                }

                // 2.填充字段定义部分
                for (StandardField standardField : standardFields) {
                    Row fieldRow = ExcelCommonUtils.touchRow(standardFieldSheet, startRow++);

                    ExcelCommonUtils.touchCell(fieldRow, 1, defaultCellStyle)
                            .setCellValue(standardField.getDefKey());
                    ExcelCommonUtils.touchCell(fieldRow, 2, defaultCellStyle)
                            .setCellValue(standardField.getDefName());
                    ExcelCommonUtils.touchCell(fieldRow, 3, defaultCellStyle)
                            .setCellValue(standardField.getType());
                    if (standardField.getLen() != null) {
                        ExcelCommonUtils.touchCell(fieldRow, 4, defaultCellStyle)
                                .setCellValue(standardField.getLen());
                    }
                    if (standardField.getScale() != null) {
                        ExcelCommonUtils.touchCell(fieldRow, 5, defaultCellStyle)
                                .setCellValue(standardField.getScale());
                    }
                    ExcelCommonUtils.touchCell(fieldRow, 6, defaultCellStyle)
                            .setCellValue(standardField.getDefaultValue());
                    ExcelCommonUtils.touchCell(fieldRow, 7, defaultCellStyle)
                            .setCellValue(standardField.getComment());

                }
            }
            workbook.setActiveSheet(0);
            workbook.write(fos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOKit.close(byteIn);
            IOKit.close(orginIn);
            IOKit.close(fos);
        }
    }
}
