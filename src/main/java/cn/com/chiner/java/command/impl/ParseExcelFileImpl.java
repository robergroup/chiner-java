/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.java.command.impl;

import cn.com.chiner.java.command.Command;
import cn.com.chiner.java.command.ExecResult;
import cn.com.chiner.java.command.impl.excel.EntityAndDictParser;
import cn.com.chiner.java.command.impl.excel.ExcelCommonUtils;
import cn.com.chiner.java.model.*;
import cn.com.chiner.java.model.Module;
import cn.fisok.raw.kit.JSONKit;
import cn.fisok.raw.kit.StringKit;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2022/10/12
 * @desc : 解析EXCEL文件
 */
public class ParseExcelFileImpl implements Command<ExecResult> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public ExecResult exec(Map<String, String> params) {

        String excelFile = params.get("excelFile");
        File inFile = new File(excelFile);

        ExecResult ret = new ExecResult();
        try {
            Workbook workBook = ExcelCommonUtils.getWorkbook(inFile);
            EntityAndDictParser parser = new EntityAndDictParser();
            parser.parse(workBook);
            ret.setBody(new LinkedHashMap<String, Object>() {{
                put("name", "EXCEL导入");
                put("describe", "EXCEL导入-导入数据表以及字典");
                if (parser.isSimpleMode()) {      //简单模式
                    put("entities", parser.getModule().getEntities());
                    put("dicts", parser.getModule().getDicts());
                } else {                          //主题分组模式
                    List<ViewGroup> viewGroups = new ArrayList<>();
                    List<Module> modules = parser.getModules();
                    List<TableEntity> entities = new ArrayList<>();
                    List<Dict> dicts = new ArrayList<>();

                    modules.forEach(module -> {
                        if (module.getEntities() != null) {
                            entities.addAll(module.getEntities());
                        }
                        if (module.getDicts() != null) {
                            dicts.addAll(module.getDicts());
                        }

                        ViewGroup viewGroup = new ViewGroup();
                        viewGroup.setId(StringKit.uuid("-").toUpperCase());
                        viewGroup.setDefKey(module.getDefKey());
                        viewGroup.setDefName(module.getDefName());
                        List<String> refEntities = new ArrayList<>();
                        List<String> refDicts = new ArrayList<>();
                        viewGroup.setRefEntities(refEntities);
                        viewGroup.setRefDicts(refDicts);
                        viewGroups.add(viewGroup);

                        if (module.getEntities() != null) {
                            module.getEntities().forEach(entity -> {
                                refEntities.add(entity.getId());
                            });
                        }
                        if (module.getDicts() != null) {
                            module.getDicts().forEach(dict -> {
                                refDicts.add(dict.getId());
                            });
                        }

                    });

                    put("entities", entities);
                    put("dicts", dicts);
                    put("viewGroups", viewGroups);

                }

            }});
            ret.setStatus(ExecResult.SUCCESS);
            System.out.println(JSONKit.toJsonString(ret, true));
        } catch (Exception e) {
            String message = e.getMessage();
            if (StringKit.isBlank(message)) {
                message = e.toString();
            }
            ret.setBody(message);
            ret.setStatus(ExecResult.FAILED);
            logger.error("", e);
        }

        return ret;
    }

}
