package cn.com.chiner.java.command.impl.excel;

import cn.com.chiner.java.model.Dict;
import cn.com.chiner.java.model.DictItem;
import cn.com.chiner.java.model.Module;
import cn.com.chiner.java.model.TableEntity;
import cn.fisok.raw.kit.StringKit;
import cn.fisok.raw.lang.ValueObject;
import org.apache.poi.ss.usermodel.*;

import java.util.ArrayList;
import java.util.List;

public class EntityAndDictParser {
    private boolean simpleMode = false;
    private Module module = null;
    private List<Module> modules = null;

    public boolean isSimpleMode() {
        return simpleMode;
    }

    public Module getModule() {
        return module;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void parse(Workbook workbook){
        Sheet sheet = workbook.getSheetAt(0);   //读取目录sheet
        int lastRowNum = sheet.getLastRowNum();
        if(lastRowNum<=2){
            return;
        }
        String headerCellText1 = ExcelCommonUtils.getCellValue(sheet.getRow(1).getCell(0)).strValue("");
        String headerCellText2 = ExcelCommonUtils.getCellValue(sheet.getRow(1).getCell(1)).strValue("");

        //主题模式：第一行的第一列值“主题域”或者第二列值为“#”
        if(headerCellText1.contains("主题域") || headerCellText2.contains("#")){
            simpleMode = false;
            modules = new ArrayList<>();
            parseWithGroupModel(workbook,sheet,modules);            //主题分组模式
        }else{
            simpleMode = true;
            module = parseWithSimpleModel(workbook, sheet);         //简单模式
        }
    }

    /**
     * 解析分组模式下的表以及字典
     * @param workbook 工作啵
     * @param catalogSheet 目录页
     * @param modules 模块列表容器
     */
    private void parseWithGroupModel(Workbook workbook,Sheet catalogSheet, List<Module> modules){
        Module curModule = null;
        for(int i=2;i<=catalogSheet.getLastRowNum();i++){
            Row row = catalogSheet.getRow(i);
            if(row == null){
                continue;
            }
            Cell moduleCell = row.getCell(0);
            Cell entityDefKeyCell = row.getCell(2);
            Cell entityDefNameCell = row.getCell(3);
            Cell entityCommentCell = row.getCell(4);

            String moduleText = "";
            if(moduleCell != null){
                moduleText = ValueObject.valueOf(moduleCell.getStringCellValue()).strValue("").trim();
            }
            if(StringKit.isNotEmpty(moduleText)){
                curModule = new Module();
                fillModuleKeyAndName(curModule,moduleText);
                curModule.setEntities(new ArrayList<>());
                modules.add(curModule);
            }
            if(curModule == null){
                curModule = new Module();
                fillModuleKeyAndName(curModule,"UNTOPIC-未分组的");
                curModule.setEntities(new ArrayList<>());
                modules.add(curModule);
            }
            if (!ExcelCommonUtils.cellIsNotBlank(entityDefKeyCell)){
                continue;
            }
            TableEntity entity = new TableEntity();
            curModule.getEntities().add(entity);
            entity.setId(StringKit.uuid("-").toUpperCase());
            entity.setDefKey(ValueObject.valueOf(ExcelCommonUtils.getCellValue(entityDefKeyCell)).strValue("").trim());
            entity.setDefName(ValueObject.valueOf(ExcelCommonUtils.getCellValue(entityDefNameCell)).strValue("").trim());
            entity.setComment(ValueObject.valueOf(ExcelCommonUtils.getCellValue(entityCommentCell)).strValue("").trim());


            //读取sheet页中的字段内容
            //如果设置了超链接，则通过超链接读取sheet名
            String sheetName = entity.getDefKey();
            Hyperlink hyperlink = row.getCell(1).getHyperlink();
            if(hyperlink != null){
                String linkAddr = hyperlink.getAddress();
                sheetName = ExcelCommonUtils.parseTableNameFromLinkAddress(linkAddr);
                if(StringKit.isEmpty(sheetName)){
                    sheetName = entity.getDefKey();
                }
            }
            ExcelCommonUtils.parseTableEntity(4,0,workbook,sheetName,entity);
            entity.fillFieldsCalcValue();
        }
        //把字典中的内容合并至模块中
        List<Module> dictModules = parseDictInTopicMode(workbook);
        for(Module module:modules){
            for(int k=0;k<dictModules.size();k++){
                Module dictModule = dictModules.get(k);
                if(module.getDefKey().equals(dictModule.getDefKey())
                        && module.getDefName().equals(dictModule.getDefName())){
                    module.setDicts(dictModule.getDicts());
                    dictModules.remove(k);
                    break;
                }
            }
        }
        //没有合并完的，说明这些模块在表的模块中不存在，需要加总进去
        modules.addAll(dictModules);
    }

    private void fillModuleKeyAndName(Module module,String moduleText){
        if(StringKit.isNotEmpty(moduleText)){
            moduleText = moduleText.replaceAll("\\s+","");
            int linePos = moduleText.indexOf("-");
            String key = linePos>1?moduleText.substring(0,linePos):moduleText;
            String name = linePos>1?moduleText.substring(linePos+1):moduleText;
            module.setDefKey(key);
            module.setDefName(name);
        }

    }

    /**
     * 解析分组模式下的表以及字典
     * @param workbook 工作啵
     * @param catalogSheet 目录页
     * @return
     */
    private Module parseWithSimpleModel(Workbook workbook,Sheet catalogSheet){
        Module module = new Module();   //先借个module容器放一放
        module.setDefKey("_SIMPLE_");
        module.setDefKey("_简单容器_");
        module.setEntities(new ArrayList<>());

        for(int i=2;i<=catalogSheet.getLastRowNum();i++){
            Row row = catalogSheet.getRow(i);
            if(row == null){
                continue;
            }
            Cell entityDefKeyCell = row.getCell(1);
            Cell entityDefNameCell = row.getCell(2);
            Cell entityCommentCell = row.getCell(3);

            if (!ExcelCommonUtils.cellIsNotBlank(entityDefKeyCell)){
                continue;
            }
            TableEntity entity = new TableEntity();
            module.getEntities().add(entity);
            entity.setId(StringKit.uuid("-").toUpperCase());
            entity.setDefKey(ValueObject.valueOf(entityDefKeyCell.getStringCellValue()).strValue("").trim());
            entity.setDefName(ValueObject.valueOf(ExcelCommonUtils.getCellValue(entityDefNameCell)).strValue("").trim());
            entity.setComment(ValueObject.valueOf(ExcelCommonUtils.getCellValue(entityCommentCell)).strValue("").trim());

            //读取sheet页中的字段内容
            //如果设置了超链接，则通过超链接读取sheet名
            String sheetName = entity.getDefKey();
            Hyperlink hyperlink = entityDefKeyCell.getHyperlink();
            if(hyperlink != null){
                String linkAddr = hyperlink.getAddress();
                sheetName = ExcelCommonUtils.parseTableNameFromLinkAddress(linkAddr);
                if(StringKit.isEmpty(sheetName)){
                    sheetName = entity.getDefKey();
                }
            }
            ExcelCommonUtils.parseTableEntity(4,0,workbook,sheetName,entity);
            entity.fillFieldsCalcValue();
        }

        //把字典中的内容合并至模块中
        List<Dict> dicts = parseDictInSimpleMode(workbook);
        module.setDicts(dicts);
        return  module;
    }

    /**
     * 把字典部分独立解析为一个Module
     * @param workbook
     * @return
     */
    private List<Module> parseDictInTopicMode(Workbook workbook){
        List<Module> modules = new ArrayList<>();
        Sheet sheet = workbook.getSheetAt(1);
        Dict curDict = null;
        Module curModule = null;
        for(int i=2;i<sheet.getLastRowNum();i++){
            Row row = sheet.getRow(i);
            if(row == null){
                continue;
            }
            Cell moduleCell = row.getCell(0);
            Cell dictDefKeyCell = row.getCell(1);
            Cell dictDefNameCell = row.getCell(2);
            Cell itemDefKeyCell = row.getCell(4);
            Cell itemDefNameCell = row.getCell(5);
            Cell itemSortCell = row.getCell(6);
            Cell itemParentKeyCell = row.getCell(7);
            Cell itemIntroCell = row.getCell(8);

            //处理模块层
            String moduleText = "";
            if(moduleCell != null){
                moduleText = ValueObject.valueOf(moduleCell.getStringCellValue()).strValue("").trim();
            }

            if(StringKit.isNotEmpty(moduleText)){
                curModule = new Module();
                fillModuleKeyAndName(curModule,moduleText);
                curModule.setDicts(new ArrayList<>());
                modules.add(curModule);
            }
            if(curModule == null){
                curModule = new Module();
                fillModuleKeyAndName(curModule,"UNTOPIC-未分组的");
                curModule.setDicts(new ArrayList<>());
                modules.add(curModule);
            }

            //处理字典对象层
            String defKey = null;
            String defName = null;
            if(dictDefKeyCell != null){
                defKey = ValueObject.valueOf(dictDefKeyCell.getStringCellValue()).strValue("").trim();
            }
            if(dictDefNameCell != null){
                defName = ValueObject.valueOf(dictDefNameCell.getStringCellValue()).strValue("").trim();
            }

            if(StringKit.isNotEmpty(defKey)){
                curDict = new Dict();
                curDict.setId(StringKit.uuid("-").toUpperCase());
                curDict.setDefKey(defKey);
                curDict.setDefName(defName);
                curDict.setItems(new ArrayList<>());
                curModule.getDicts().add(curDict);
            }

            //处理条目层
            if(itemDefKeyCell == null){
                continue;
            }
            String itemKey = ValueObject.valueOf(ExcelCommonUtils.getCellValue(itemDefKeyCell)).strValue("").trim();
            if(StringKit.isNotEmpty(itemKey)){
                DictItem item  = new DictItem();
                item.setDefKey(itemKey);
                item.setDefName(ValueObject.valueOf(ExcelCommonUtils.getCellValue(itemDefNameCell)).strValue("").trim());
                item.setSort(ValueObject.valueOf(ExcelCommonUtils.getCellValue(itemSortCell)).strValue("").trim());
                item.setParentKey(ValueObject.valueOf(ExcelCommonUtils.getCellValue(itemParentKeyCell)).strValue("").trim());
                item.setIntro(ValueObject.valueOf(ExcelCommonUtils.getCellValue(itemIntroCell)).strValue("").trim());
                if(curDict == null){
                    continue;
                }
                curDict.getItems().add(item);
            }
        }
        return modules;
    }


    /**
     * 简单模式解析字典
     * @param workbook
     * @return
     */
    private List<Dict> parseDictInSimpleMode(Workbook workbook){
        List<Dict> dicts = new ArrayList<>();

        Sheet sheet = workbook.getSheetAt(1);

        Dict curDict = null;
        for(int i=0;i<sheet.getLastRowNum();i++){
            Row row = sheet.getRow(i);
            Cell dictDefKeyCell = row.getCell(0);
            Cell dictDefNameCell = row.getCell(1);
            Cell itemDefKeyCell = row.getCell(3);
            Cell itemDefNameCell = row.getCell(4);
            Cell itemSortCell = row.getCell(5);
            Cell itemParentKeyCell = row.getCell(6);
            Cell itemIntroCell = row.getCell(7);

            //处理字典对象层
            String defKey = ExcelCommonUtils.getCellValue(dictDefKeyCell).strValue("").trim();
            String defName = ExcelCommonUtils.getCellValue(dictDefNameCell).strValue("").trim();
            if(StringKit.isNotEmpty(defKey)){
                curDict = new Dict();
                curDict.setId(StringKit.uuid("-").toUpperCase());
                curDict.setDefKey(defKey);
                curDict.setDefName(defName);
                curDict.setItems(new ArrayList<>());
                dicts.add(curDict);
            }

            //处理条目层
            String itemKey = ValueObject.valueOf(ExcelCommonUtils.getCellValue(itemDefKeyCell)).strValue("").trim();
            if(StringKit.isNotEmpty(itemKey)){
                DictItem item  = new DictItem();
                item.setDefKey(itemKey);
                item.setDefName(ExcelCommonUtils.getCellValue(itemDefNameCell).strValue("").trim());
                item.setSort(ExcelCommonUtils.getCellValue(itemSortCell).strValue("").trim());
                item.setParentKey(ExcelCommonUtils.getCellValue(itemParentKeyCell).strValue("").trim());
                item.setIntro(ExcelCommonUtils.getCellValue(itemIntroCell).strValue("").trim());
                curDict.getItems().add(item);
            }
        }
        return dicts;
    }
}
