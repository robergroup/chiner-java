/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.java.dialect.impl;

import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import cn.fisok.raw.kit.JdbcKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2021/6/19
 * @desc : 达梦数据库方言
 */
public class DBDialectDB2ByQuery extends DBDialectMetaByQuery {
    private static Logger logger = LoggerFactory.getLogger(DBDialectDB2ByQuery.class);

    @Override
    protected String getQueryTablesSQL() {
        return "select name as tbl_name,\n" +
                "       remarks as tbl_comment,\n" +
                "       creator as db_name\n" +
                "from sysibm.systables\n" +
                "where upper(creator)=upper(?)\n" +
                "order by ctime asc";
    }

    @Override
    protected String getQueryTableColumnsSQL() {
        return "select\n" +
                "    col.tbname as tbl_name,\n" +
                "    '' as tbl_comment,\n" +
                "    colno as col_index,\n" +
                "    col.name as col_name,\n" +
                "    remarks as col_comment,\n" +
                "    col.typename as data_type ,\n" +
                "    col.length as data_length,\n" +
                "    col.scale as num_scale,\n" +
                "    col.keyseq as is_primary_key,\n" +
                "    col.nulls as is_nullable,\n" +
                "    col.default as default_value\n" +
                "from sysibm.syscolumns col\n" +
                "where upper(tbcreator)=upper(?)\n" +
                "  and tbname =?\n" +
                "order by colno asc";
    }

    @Override
    public List<TableEntity> getAllTables(Connection conn, String schema) throws SQLException {
        if(schema == null){
            return super.getAllTables(conn, conn.getSchema());
        }
        return super.getAllTables(conn, schema);
    }

    @Override
    public TableEntity createTableEntity(Connection conn, DatabaseMetaData meta, String tableName, String schema) throws SQLException {
        String sql = getQueryTableColumnsSQL();
        logger.debug(sql);

        PreparedStatement pstmt = conn.prepareStatement(sql);
        if(schema == null){
            pstmt.setString(1, conn.getSchema());
        }else{
            pstmt.setString(1, schema);
        }
        pstmt.setString(2, tableName);


        TableEntity tableEntity = new TableEntity();
        tableEntity.setDefKey(tableName);

        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            tableEntity.setDefKey(rs.getString("tbl_name"));
            tableEntity.setComment(rs.getString("tbl_comment"));
            ColumnField field = new ColumnField();
            tableEntity.getFields().add(field);
            field.setDefKey(rs.getString("col_name"));
            field.setDefName(rs.getString("col_comment"));

            String dataType = rs.getString("data_type");
            Integer dataLength = rs.getInt("data_length");
            Integer numScale = rs.getInt("num_scale");
            if(withoutLenDataTypeName(dataType))
            {
                dataLength = null;
                numScale = null;
            }
            String isNullable = rs.getString("is_nullable");//  Y|N
            String isPrimaryKey = rs.getString("is_primary_key");//
            String defaultValue = rs.getString("default_value");//

            //数据类型以及长度
            field.setType(dataType);
            if (numScale != null && numScale > 0) {
                field.setLen(dataLength);
                if (numScale != null && numScale > 0) {
                    field.setScale(numScale);
                }
            } else if (dataLength != null && dataLength > 0) {
                field.setLen(dataLength);
            }
            field.setNotNull("N".equals(isNullable));
            field.setPrimaryKey("1".equals(isPrimaryKey));
            if (defaultValue != null
                    && !defaultValue.startsWith("'")
                    && isStringDataType(dataType) ) {
                defaultValue = "'" + defaultValue + "'";
            }
            field.setDefaultValue(defaultValue);
        }

        JdbcKit.close(pstmt);
        JdbcKit.close(rs);

        return tableEntity;
    }
}
