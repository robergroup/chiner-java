package cn.com.chiner.java.model;

import java.util.List;

/**
 * @author 陈家宁
 * @date 2024/4/22
 * @desc
 */
public class Profile {

    List<ColumnHeader> headers;

    public List<ColumnHeader> getHeaders() {
        return headers;
    }

    public void setHeaders(List<ColumnHeader> headers) {
        this.headers = headers;
    }
}
