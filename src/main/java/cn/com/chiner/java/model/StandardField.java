package cn.com.chiner.java.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;


@JsonPropertyOrder({
        "defKey",
        "defName",
        "type",
        "len",
        "scale",
        "defaultValue",
        "comment",
})
/**
 * 标准字段库，字段定义
 */
public class StandardField implements Serializable, Cloneable {
    private String defKey;          //字段代码
    private String defName;         //字段名称
    private String comment = "";    //备注
    private String type = "";       //字段类型
    private Integer len = null;     //字段长度
    private Integer scale = null;   //精度
    private String defaultValue = "";   //默认值

    public String getDefKey() {
        return defKey;
    }

    public void setDefKey(String defKey) {
        this.defKey = defKey;
    }

    public String getDefName() {
        return defName;
    }

    public void setDefName(String defName) {
        this.defName = defName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLen() {
        return len;
    }

    public void setLen(Integer len) {
        this.len = len;
    }

    public Integer getScale() {
        return scale;
    }

    public void setScale(Integer scale) {
        this.scale = scale;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

}

