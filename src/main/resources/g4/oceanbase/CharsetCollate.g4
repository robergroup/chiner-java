grammar CharsetCollate;
import Common;

charset:
    default_charset
    | column_charset
    ;

default_charset:
    ('DEFAULT' | 'default')? ('CHARSET'|'charset'|('CHARACTER' 'SET')|('character' 'set')) '=' ('UTF8' | 'UTF8MB4' | 'BINARY' | 'utf8' | 'utf8mb4' | 'binary')
    ;

column_charset:
    ('CHARSET'|'charset'|('CHARACTER' 'SET')|('character' 'set')) ('UTF8' | 'UTF8MB4' | 'BINARY' | 'utf8' | 'utf8mb4' | 'binary')
    ;

collate:
    default_collate
    | column_collate
    ;

default_collate:
    ('DEFAULT' | 'default')? ('COLLATE'|'collate') '='? ('UTF8MB4_GENERAL_CI'|'utf8mb4_general_ci' | 'UTF8MB4_BIN'|'utf8mb4_bin' | 'BINARY'|'binary')
    ;

column_collate:
    ('COLLATE'|'collate') ('UTF8MB4_GENERAL_CI'|'utf8mb4_general_ci' | 'UTF8MB4_BIN'|'utf8mb4_bin' | 'BINARY'|'binary')
    ;

