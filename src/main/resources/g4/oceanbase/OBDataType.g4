grammar OBDataType;
import Common;
//OceanBase数据库的数据类型，version:4.2.0

data_type:
    TINY|BOOL|BOOLEAN|SMALLINT|MEDIUMINT|INTEGER|BIGINT|DECIMAL|NUMBER|NUMERIC|FLOAT|DOUBLE|DOUBLE_PRICISION|BIT|
    DATE|DATETIME|TIMESTAMP|TIME|YEAR|
    CHAR|VARCHAR|VARCHAR2|CHARACTER_VARYING|BINARY|VARBINARY|STRING|
    TINYBLOB|BLOB|MEDIUMBLOB|LONGBBLOB|TINYTEXT|TEXT|MEDIUMTEXT|LONGTEXT|
    ENUM|SET|JSON|
    GEOMETRY|POINT|LINESTRING|POLYGON|MULTILINESTRING|MULTIPOINT|MULTIPOLYGON|GEOMETRYCOLLECTION
    ;

UNSIGNED:'UNSIGNED'|'unsigned';
ZEROFILL:'ZEROFILL'|'zerofill';
NATIONAL:'NATIONAL'|'national';
CHARACTER_SET:('CHARACTER'|'character') ('SET'|'set') ;//TODO:增加character的类型
COLLATE_SET:('COLLATE'|'collate') ; //TODO:增加collate类型

/**
  * 整数类型：BOOL/BOOLEAN/TINYINT/SMALLINT/MEDIUMINT/INT/INTEGER/BIGINT
  * 定点类型：DECIMAL/NUMERIC
  * 浮点类型：FLOAT/DOUBLE
  * bit-value类型：BIT
  */
TINY:('TINYINT'|'tinyint'|'TINYINT('INT')'|'tinyint('INT')') UNSIGNED? ZEROFILL?;
BOOL:('BOOL'|'bool'|'BOOL('INT')'|'bool('INT')') UNSIGNED? ZEROFILL?;
BOOLEAN:('BOOLEAN'|'boolean'|'BOOLEAN('INT')'|'boolean('INT')') UNSIGNED? ZEROFILL?;
SMALLINT:('SMALLINT'|'smallint'|'SMALLINT('INT')'|'smallint('INT')') UNSIGNED? ZEROFILL?;
MEDIUMINT:('MEDIUMINT'|'mediumint'|'MEDIUMINT('INT')'|'mediumint('INT')') UNSIGNED? ZEROFILL?;
INTEGER //INT放到INTEGER里，避免与Common中的INT冲突
    :('INTEGER'|'integer'|'INTEGER('INT')'|'integer('INT')') UNSIGNED? ZEROFILL?
    |('INT'|'int'|'INT('INT')'|'int('INT')') UNSIGNED? ZEROFILL?;
BIGINT:('BIGINT'|'bigint'|'BIGINT('INT')'|'bigint('INT')') UNSIGNED? ZEROFILL?;
DECIMAL:('DECIMAL'|'decimal'|'DECIMAL('INT')'|'decimal('INT')'|'DECIMAL('INT','INT')'|'decimal('INT','INT')') UNSIGNED? ZEROFILL?;
NUMERIC:('NUMERIC'|'numeric'|'NUMERIC('INT')'|'numeric('INT')'|'NUMERIC('INT','INT')'|'numeric('INT','INT')') UNSIGNED? ZEROFILL?;
FLOAT:('FLOAT'|'float'|'FLOAT('INT')'|'float('INT')'|'FLOAT('INT','INT')'|'float('INT','INT')') UNSIGNED? ZEROFILL?;
DOUBLE:('DOUBLE'|'double'|'DOUBLE('INT')'|'double('INT')'|'DOUBLE('INT','INT')'|'double('INT','INT')') UNSIGNED? ZEROFILL?;
DOUBLE_PRICISION:(('DOUBLE' 'PRICISION'|'double' 'pricision')|('DOUBLE' 'PRICISION('INT')'|'double' 'pricision('INT')')|'DOUBLE' 'PRICISION('INT','INT')'|'double' 'pricision('INT','INT')') UNSIGNED? ZEROFILL?;
BIT:'BIT'|'bit'|'BIT('INT')'|'bit('INT')';
NUMBER:('NUMBER'|'number'|'NUMBER('INT')'|'number('INT')'|'NUMBER('INT','INT')'|'number('INT','INT')') UNSIGNED? ZEROFILL?;

/**
  * 日期时间类型：DATE/TIME/DATETIME/TIMESTAMP/YEAR
  */
DATE:'DATE'|'date'|'Date';
DATETIME:'DATETIME'|'datetime'|'DATETIME('INT')'|'datetime('INT')';
TIMESTAMP:'TIMESTAMP'|'timestamp'|'TIMESTAMP('INT')'|'timestamp('INT')';
TIME:'TIME'|'time'|'TIME('INT')'|'time('INT')';
YEAR:'YEAR'|'year'|'YEAR(4)'|'year(4)';

/**
  * 字符类型：CHAR/VARCHAR/BINARY/VARBINARY
  */
CHAR:NATIONAL? ('CHAR'|'char'|'CHAR('INT')'|'char('INT')') CHARACTER_SET? COLLATE_SET?;
VARCHAR:NATIONAL? ('VARCHAR('INT')'|'varchar('INT')'|'VARCHAR'|'varchar') CHARACTER_SET? COLLATE_SET?;
VARCHAR2:NATIONAL? ('VARCHAR2('INT')'|'varchar2('INT')'|'VARCHAR2'|'varchar2') CHARACTER_SET? COLLATE_SET?;
CHARACTER_VARYING:NATIONAL? ('CHARACTER'|'character') ('VARYING('INT')'|'varying('INT')') CHARACTER_SET? COLLATE_SET?;
BINARY:'BINARY'|'binary'|'BINARY('INT')'|'binary('INT')';
VARBINARY:'VARBINARY('INT')'|'varbinary('INT')';
STRING:'STRING'|'string';

/**
  * 大对象和文本类型：TINYBLOB/BOLB/MEDIUMBLOB/LONGBLOB/TINYTEXT/TEXT/MEDIUMTEXT/LONGTEXT
  */
TINYBLOB:'TINYBLOB'|'tinyblob';
BLOB:'BLOB'|'blob'|'BLOB('INT')'|'blob('INT')';
MEDIUMBLOB:'MEDIUMBLOB'|'mediumblob';
LONGBBLOB:'LONGBLOB'|'longblob';
TINYTEXT:('TINYTEXT'|'tinytext') CHARACTER_SET? COLLATE_SET?;
TEXT:'TEXT'|'text'|'TEXT('INT')'|'text('INT')' CHARACTER_SET? COLLATE_SET?;
MEDIUMTEXT:('MEDIUMTEXT'|'mediumtext') CHARACTER_SET? COLLATE_SET?;
LONGTEXT:('LONGTEXT'|'longtext') CHARACTER_SET? COLLATE_SET?;

/**
  * 枚举类型：ENUM
  */
ENUM:('ENUM('NAME(','NAME)+')'|'enum('NAME(','NAME)+')') CHARACTER_SET? COLLATE_SET?;

/**
  * 集合类型：SET
  */
SET:('ENUM('NAME(','NAME)+')'|'enum('NAME(','NAME)+')') CHARACTER_SET? COLLATE_SET?;

/**
  * json类型：JSON
  */
JSON:'JSON'|'json';

/**
  * 空间类型：GEOMETRY/POINT/LINESTRING/POLYGON/MULTIPOINT/MULTILINESTRING/MULTIPOLYGON/GEOMETRYCOLLECTION
  */
GEOMETRY:('GEOMETRY'|'geometry')|('GEOMETRY'|'geometry') 'NOT' 'NULL' 'SRID' INT;
POINT:('POINT'|'point')|('POINT'|'point') 'NOT' 'NULL' 'SRID' INT;
LINESTRING:('LINESTRING'|'linestring')|('LINESTRING'|'linestring') 'NOT' 'NULL' 'SRID' INT;
POLYGON:('POLYGON'|'polygon')|('POLYGON'|'polygon') 'NOT' 'NULL' 'SRID' INT;
MULTIPOINT:('MULTIPOINT'|'multipoint')|('MULTIPOINT'|'multipoint') 'NOT' 'NULL' 'SRID' INT;
MULTILINESTRING:('MULTILINESTRING'|'multilinestring')|('MULTILINESTRING'|'multilinestring') 'NOT' 'NULL' 'SRID' INT;
MULTIPOLYGON:('MULTIPOLYGON'|'multipolygon')|('MULTIPOLYGON'|'multipolygon') 'NOT' 'NULL' 'SRID' INT;
GEOMETRYCOLLECTION:('GEOMETRYCOLLECTION'|'gepmetrycollection')|('GEOMETRYCOLLECTION'|'gepmetrycollection') 'NOT' 'NULL' 'SRID' INT;
