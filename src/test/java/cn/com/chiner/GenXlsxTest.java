/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner;

import cn.com.chiner.java.Application;
import org.junit.Test;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2024/1/11
 * @desc :
 */
public class GenXlsxTest {
    @Test
    public void genDocxGroupsTest(){
        String[] args =  new String[]{
                "GenExcelImpl",            //执行什么命令
//                "dataFile=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/resources/pdma/大学综合管理系统.pdma.json",  //输入的PDMan文件
                "dataFile=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/resources/pdma/test.pdma.json",  //输入的PDMan文件
                "outFile=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/out/result-by-group.xlsx",  //输入的PDMan文件
                "out=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/out/gen-xlsx-group-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }

    @Test
    public void genXlsxTest(){

        String[] args =  new String[]{
                "GenExcelImpl",            //执行什么命令
                "dataFile=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/resources/pdma/教学管理系统-标准模板.pdma.json",  //输入的PDMan文件
                "outFile=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/out/result-by-simple.xlsx",  //输入的PDMan文件
                "out=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/out/gen-xlsx-simple-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }
}
