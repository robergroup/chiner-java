package cn.com.chiner;

public class NumberTest {
    public static void main(String[] args){
        String[] strArr = new String[]{
                "123456",
                "123-456",
                "123456-",
                "-123456",
                "--123456",
                "---123456",
                "123-4-56",
                "123--456"
        };
        for(String str:strArr){
            int linePos = str.indexOf("-");
            String moduleId = linePos>1?str.substring(0,linePos):str;
            String moduleName = linePos>1?str.substring(linePos+1):str;

            System.out.println(str+"=>"+moduleId+"["+moduleName+"]");
        }
    }


}
