/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner;

import cn.com.chiner.java.Application;
import org.junit.Test;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2021/6/12
 * @desc :
 */
public class PDMParseTest {



    @Test
    public void parsePDMFileTest(){
        String[] args =  new String[]{
                "ParsePDMFile",            //执行什么命令
//                "pdmFile=/Users/asher/workspace/ws-vekai/chiner-java/src/test/resources/pdm/供应商管理.pdm",  //输入的PDMan文件
//                "pdmFile=/Users/asher/workspace/ws-vekai/chiner-java/src/test/resources/pdm/JEKI-WIKI文章模块.pdm",  //输入的PDMan文件
//                "pdmFile=/Users/asher/workspace/ws-vekai/chiner-java/src/test/resources/pdm/数据字典.PDM",  //输入的PDMan文件
//                "pdmFile=/Users/asher/workspace/ws-vekai/chiner-java/src/test/resources/pdm/知识管理.pdm",  //输入的PDMan文件
                "pdmFile=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/resources/pdm/知识管理.pdm",  //输入的PDMan文件
                "out=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/resources/out/import-pdm-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }

}
