package cn.com.chiner.antlr4;

import cn.com.chiner.java.Application;
import org.junit.Test;

public class DDLParse {
    @Test
    public void mysqlTest(){
        String[] args =  new String[]{
                "DDLParseImpl",                      //执行什么命令
                //测试，常用drop可以，使用pdmaas导出的ddl实现的drop不行
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/sqlserver-ddl.sql",
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/pdmaas-ddl-mysql.sql",
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/pdmaas-ddl-oracle.sql",
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/pdmaas-ddl-sqlServer.sql",
                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/issue0.sql",
                "out=/Users/wantyx/java/chiner-java/src/test/resources/sql/parse-ddl-"+System.nanoTime()+".json",
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/pdmaas-ddl-sqlServer.sql",
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/一表通-改前.sql",
//                "ddlFile=/Users/wantyx/java/chiner-java/src/test/resources/sql/mysql-export.sql",
//                "out=/Users/yangsong/Workspace/ws-dms/chiner-java/src/test/out/parse-ddl-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }
}
