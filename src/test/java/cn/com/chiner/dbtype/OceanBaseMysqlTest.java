package cn.com.chiner.dbtype;

import cn.com.chiner.java.Application;
import org.junit.Test;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/10/21
 * 实现功能 :
 */
public class OceanBaseMysqlTest {
    @Test
    public void pingDriverLoadTest(){
        String[] args =  new String[]{
                "PingLoadDriverClass",                      //执行什么命令
                "driver_class_name=com.mysql.cj.jdbc.Driver",
                "url=jdbc:mysql://xxxxx-mi.oceanbase.aliyuncs.com:3306/test?user=wantyx&password=Tyx19971003",
                "username=xxxxx",
                "password=xxxx",
                "out=/Users/wantyx/java/chiner-java/src/test/resources/out/pdc-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }

    @Test
    public void getTableDDLTest(){
        String[] args =  new String[]{
                "DBReverseGetTableDDL",            //执行什么命令
                "driver_class_name=com.ibm.db2.jcc.DB2Driver",
                "url=jdbc:mysql://xxxxx.oceanbase.aliyuncs.com:3306/test?user=wantyx&password=Tyx19971003",
                "username=xxxx",
                "password=xxxx",
                "tables=core_code_generator",
                "out=/Users/wantyx/java/chiner-java/src/test/resources/out/pdc-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }
}
